<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js ie6 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js ie7 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js ie8 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Оригинальные подарки для мужчин и женщин — Dream Moments</title>
		<meta name="description" content="Интернет-магазин оригинальных подарков Dream Moments. У нас вы можете купить необычные подарки для дорогих вам людей.">
	<meta name="keywords" content="подарки, купить, оригинальные, для мужчин, для женщин, праздничные, Казахстан, Алма-Ата">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link href="/tpl/default/css/style.css" rel="stylesheet">
</head>
<?php
	$uc_classes = array('white', 'purple');
	$uc_logo_classes = array('logo-1', 'logo-2', 'logo-3');
?>
<body class="<?=$uc_classes[mt_rand(0,1)]?>">
	<div class="container">
		<div class="logo-wrap">
			<h1 class="logo <?=$uc_logo_classes[mt_rand(0,2)]?>"><span>Dream Moments — магазин подарков</span></h1>
			<p><?php echo Yii::app()->maintenanceMode->message; ?></p>
		</div>
		<!-- /.logo-wrap -->
	</div>
	<!-- /.container -->
</body>
</html>