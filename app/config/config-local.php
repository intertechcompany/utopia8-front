<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$protectedPath = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'app');
$runtimePath   = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'runtime');
$sessionPath   = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'tmp');
$assetsPath    = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'www' . DS . 'assets');

return array(
	'basePath'=>$protectedPath,
	'id'=>'utopia8',
	'name'=>'Utopia8',
	'charset'=>'utf-8',
	'defaultController'=>'site',
	
	'language' => 'uk',
	'sourceLanguage'=>'en',
	'timeZone'=>'Europe/Kiev',
	
	'runtimePath' => $runtimePath,

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.Forms.*',
		'application.components.*',
	),

	// application components
	'components'=>array(
		'assetManager'=>array(
			'basePath' => $assetsPath,
			'baseUrl' => 'http://utopia8.loc/assets',
		),
		'format'=>array(
			'class'=>'CLocalizedFormatter',
		),
		'user'=>array(
			'class'=>'application.components.WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'autoRenewCookie'=>true,
			'identityCookie'=>array(
				'path' => '/',
				'domain' => 'utopia8.loc',
				'secure' => false,
				'httpOnly' => true,
			),
			'loginUrl'=>array('site/login'),
		),
		'urlManager'=>array(
			'class'=>'application.components.UrlManager',
			'actAddressedDoubleUrl'=>'404',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>true,
			'useStrictParsing'=>true,
			'routeVar'=>'_r',
			'rules'=>array(
				''=>'site/index',
				'ajax/<_a:\w+>'=>'ajax/<_a>',
				'api/<call:(updateNpData)>'=>'api/call',
				'<_a:(cart|checkout|sale|news|search)>'=>'site/<_a>',
				'catalog/<alias:[a-z0-9\-]+>'=>'site/category',
				'special/<alias:[a-z0-9\-]+>'=>'site/tag',
				'product/<alias:[a-z0-9\-]+>'=>'site/product',
				'news/<alias:[a-z0-9\-]+>'=>'site/newsarticle',
				'thank-you'=>'site/thankyou',
                'payment/api'=>'payment/api',
				'payment/result'=>'payment/result',
				// 'sitemap'=>'site/sitemap',
				'sitemap.xml'=>'site/sitemapXml',
				'<alias:[a-z0-9\-]+>'=>'site/page',
			),
		),
		'db'=>array(
			'connectionString'=>'mysql:host=localhost;dbname=utopia8',
			'emulatePrepare'=>true,
			'username'=>'root',
			'password'=>'',
			'charset'=>'utf8mb4',
			'autoConnect'=>false,
			// включаем профайлер
			'enableProfiling'=>true,
			// показываем значения параметров
			'enableParamLogging' => true,
		),
		'session'=>array(
			//'class'=>'CCacheHttpSession',
			'class'=>'CHttpSession',
			'autoStart'=>true,
			'cookieMode'=>'only',
			'sessionName'=>'sessID',
			'savePath'=>$sessionPath,
			'gCProbability'=>1,
			'timeout'=>7200,
			'cookieParams' => array(
				'path' => '/',
				'domain' => 'utopia8.loc',
				'secure' => false,
				'httpOnly' => true,
			),
		),
		'sphinx'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;port=9306;',
			'emulatePrepare' => true,
			'charset' => 'utf8',
			'autoConnect' => false,
			'username' => '',
			'password' => '',
			'enableProfiling' => true,
			'enableParamLogging' => true,
			'class' => 'CDbConnection',
		),
		'widgetFactory'=>array(
			'widgets'=>array(
				'LinkPager'=>array(
					'cssFile'                    => false,
					'header'                     => '',
					'firstPageCssClass'          => 'pagination__arrow',
					'lastPageCssClass'           => 'pagination__arrow',
					'previousPageCssClass'       => 'pagination__arrow pagination__arrow--prev',
					'nextPageCssClass'           => 'pagination__arrow pagination__arrow--next',
					'internalPageCssClass'       => 'pagination__number',
					'internalHiddenPageCssClass' => 'pagination__number',
					'prevPageLabel'              => '<span>Назад</span><i>←</i>',
					'nextPageLabel'              => '<i>→</i><span>Дальше</span>',
					// 'firstPageLabel'          => '<<',
					// 'lastPageLabel'           => '>>',
				),
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info',
					'except'=>'exception.CHttpException.*',
				),
				// uncomment the following to show log messages on web pages
				/* array(
					'class'=>'CWebLogRoute',
					//'categories' => 'application',
					'levels'=>'error, warning, trace, profile, info',
				),
				array(
					// направляем результаты профайлинга в ProfileLogRoute (отображается внизу страницы)
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
				), */
			),
		),
		'cache'=>array(
			'class'=>'CDummyCache',
		),
	),
	
	'onBeginRequest' => array('BeginRequest', 'onStartSite'),
	// 'onEndRequest' => array('BeginRequest', 'onStopSite'),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'dev' => true,
		'secret' => 'bg7,{Fk',
		'currencies' => [
			'usd' => [
				'code' => 'usd',
				'name' => 'USD',
				'suffix' => '',
				'prefix' => '$',
			],
			'eur' => [
				'code' => 'eur',
				'name' => 'EUR',
				'suffix' => '€',
				'prefix' => '',
			],
			'uah' => [
				'code' => 'uah',
				'name' => 'Грн.',
				'suffix' => ' грн.',
				'prefix' => '',
			],
		],
		'site' => 'http://utopia8.loc/',
		'langs' => array(
            'uk' => [
				'name' => 'Українська',
				'short_name' => 'UA',
				'url' => '',
			],
            'ru' => [
				'name' => 'Русский',
				'short_name' => 'RU',
				'url' => 'ru',
			],
            'en' => [
				'name' => 'English',
				'short_name' => 'EN',
				'url' => 'en',
			],
        ),
		'currency' => 'uah',
		'translations' => array(),
		'has_sale' => false,
		'mailer' => array(
			'from_email' => 'support@utopia8.ua',
			'from_name' => 'Black',
			'sendgrid' => array(
				'api_key' => '*****',
			),
			'mailgun' => array(
				'domain' => 'mailer.utopia8.loc',
				'api_key' => 'key-*****',
			),
			'esputnik' => array(
				'login' => '***@utopia8.ua',
				'password' => '***',
			),
		),
		'wfp' => array(
			'account' => 'test_merch_n1',
			'secret' => 'flk3409refn54t54t*FNJRET',
		),
		'np' => array(
			'api_key' => 'bd96f81fed28881ed86dc9ec9b773316', // 602aadd7a0592c8253b9e923d3f16ab7
		),
		'retail_crm' => array(
			'api_key' => 'Bj2DWQ4YB4tEVy112kxUdNREXTyjROds',
		),
	),
);