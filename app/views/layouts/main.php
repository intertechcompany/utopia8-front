<?php /* @var $this Controller */ ?>
<?php
    if (in_array($this->route, ['site/index', 'site/category', 'site/product'])) {
        $meta_title = $this->pageTitle;
    } else {
        $meta_title = $this->pageTitle . ' | ' . Yii::app()->name;
    }

    $categories = Yii::app()->params->categories_tree;

    $meta_description = $this->pageDescription ?: '';

    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

    $this->ogTitle = !empty($this->ogTitle) ? $this->ogTitle : $this->pageTitle;
    $this->ogDescription = !empty($this->ogDescription) ? $this->ogDescription : $meta_description;
    $this->ogUrl = Yii::app()->getRequest()->getBaseUrl(true) . '/' . Yii::app()->getRequest()->getPathInfo();

    if (empty($this->ogImage)) {
        $this->ogImage = Yii::app()->assetManager->getBaseUrl() . '/og2.png';
        $this->ogImageWidth = 1200;
        $this->ogImageHeight = 630;
    }

    $cart_total = Cart::model()->getTotal();
?><!DOCTYPE html>
<html>
<head>
    <title><?=CHtml::encode($meta_title)?></title>
    <meta name="description" content="<?=CHtml::encode($meta_description)?>">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="<?=$staticUrl?>/css/style.min.css">
    <script>var Modernizr={},userAgent=window.navigator.userAgent.toLowerCase();document.documentElement.className="js",window.opera&&window.opera.buildNumber?(document.documentElement.className+=" opera",Modernizr.opera=!0):Modernizr.opera=!1,/webkit/.test(userAgent)?(document.documentElement.className+=" webkit",Modernizr.webkit=!0):Modernizr.webkit=!1,-1<userAgent.indexOf("firefox")?(document.documentElement.className+=" firefox",Modernizr.firefox=!0):Modernizr.firefox=!1,0<userAgent.indexOf("msie ")||navigator.userAgent.match(/Trident.*rv\:11\./)?(Modernizr.msie=!0,document.documentElement.className+=" msie"):Modernizr.msie=!1,navigator.userAgent.match(/(iPad|iPhone|iPod|Android|Windows Phone|iemobile)/i)?(Modernizr.is_mobile=!0,document.documentElement.className+=" is-mobile",navigator.userAgent.match(/(CriOS|Android)/)&&(Modernizr.is_mobile_chrome=!0,document.documentElement.className+=" is-mobile-chrome")):(Modernizr.is_desktop=!0,document.documentElement.className+=" is-desktop"),navigator.userAgent.match(/(Mac|iPhone|iPod|iPad)/i)&&(Modernizr.is_mac=!0,document.documentElement.className+=" is-mac");</script>
    <?php /* <script src="<?=$staticUrl?>/js/modernizr.min.js"></script> */ ?>

    <?php if (!empty($this->canonicalUrl)) { ?><link rel="canonical" href="<?=$this->canonicalUrl?>"><?php } ?>
    <?php if (!empty($this->noIndex)) { ?><meta name="robots" content="noindex,nofollow"><?php } ?>

    <?php if (!Yii::app()->errorHandler->error) { ?>
    <?php
        $current_url_path = Yii::app()->getRequest()->getPathInfo();
        $query_string = Yii::app()->getRequest()->getQueryString();

        if (!empty($query_string)) {
            $current_url_path .= '?' . $query_string;
        }
        
        $base_url = Yii::app()->getBaseUrl(true);
    ?>
    <?php foreach (Yii::app()->params->langs as $lang_code => $lang) { ?>
    <?php
        // if ($lang_code == Yii::app()->language) {
        //     continue;
        // }

        if (empty($lang['url'])) {
            $lang_url = $base_url . '/' . $current_url_path;
        } else {
            $lang_url = $base_url . '/' . $lang['url'] . '/' . $current_url_path;
        }
    ?>
    <link rel="alternate" hreflang="<?=$lang_code?>" href="<?=$lang_url?>">
    <?php } ?>
    <?php } ?>

    <?php /* <link rel="shortcut icon" href="/favicon.ico" sizes="16x16 32x32" type="image/x-icon"> */ ?>
    <link rel="icon" href="/favicon-u8.svg">
    <link rel="apple-touch-icon" href="/apple-u8.png">

    <meta property="og:url" content="<?=CHtml::encode($this->ogUrl)?>">
    <meta property="og:title" content="<?=CHtml::encode($this->ogTitle)?>">
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?=CHtml::encode($this->ogDescription)?>">
    <?php if (!empty($this->ogImage)) { ?>
    <meta property="og:image" content="<?=CHtml::encode($this->ogImage)?>">
    <meta property="og:image:width" content="<?=CHtml::encode($this->ogImageWidth)?>">
    <meta property="og:image:height" content="<?=CHtml::encode($this->ogImageHeight)?>">
    <?php } ?>

    <?php if (!empty(Yii::app()->params->highlight)) { ?>
    <style>
        .__highlight {
            display: inline-block;
            position: absolute;
            height: 16px;
            padding: 1px 5px;
            color: #000;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 14px;
        }

        .__highlight input {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 16px;
            border: 0;
            background-color: yellow;
            padding: 0 5px;
        }
    </style>
    <?php } ?>

    <?php if (!Yii::app()->params->dev) { ?>
    <script>
        dataLayer = [];

        <?php if (!empty($this->dataLayer)) { ?>
        dataLayer.push(<?=json_encode($this->dataLayer)?>);
        <?php } ?>
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KQBNPGK');</script>
    <!-- End Google Tag Manager -->
    <?php } ?>
</head>
<body>
    <?php if (!Yii::app()->params->dev) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQBNPGK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php } ?>

    <?php $this->widget('application.components.Header.Header'); ?>

    <div class="container<?php if ($this->route != 'site/index') { ?> container--content<?php } ?>">
        <?php echo $content; ?>

        <?php $this->widget('application.components.Footer.Footer'); ?>
    </div>

    <?php if ($this->route != 'site/cart' && $this->route != 'site/checkout') { ?>
    <?php $this->widget('application.components.CartSide.CartSide'); ?>
    <?php } ?>

    <?php if (!isset(Yii::app()->request->cookies['gdpr'])) { ?>
    <div class="cookies">
        <div class="cookies__title"><?=Lang::t('cookies.tip.title')?></div>
        <div class="cookies__text">
            <?=Lang::t('cookies.tip.info')?>
        </div>
        <button type="button" class="cookies__btn"><?=Lang::t('cookies.btn.agree')?></button>
    </div>
    <?php } ?>

    <script src="<?=$staticUrl?>/js/app.min.js"></script>

    <?php if (!Yii::app()->params->dev) { ?>
    <?=Yii::app()->params->settings['ga']?>
    <?php } ?>

    <?php if (!empty(Yii::app()->params->highlight)) { ?>
    <script>
        $(function() {
            $('.__highlight').on('click', 'input', function(e) {
                e.preventDefault();
                this.setSelectionRange(0, this.value.length);
            });
        });
    </script>
    <?php } ?>
</body>
</html>