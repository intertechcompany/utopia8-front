<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
      body {width: 600px;margin: 0 auto;}
      table {border-collapse: collapse;}
      table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
      img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">
      body, p, div {
        font-family: arial;
        font-size: 14px;
      }
      body {
        color: #000000;
      }
      body a {
        color: #000000;
        text-decoration: none;
      }
      p { margin: 0; padding: 0; }
      table.wrapper {
        width:100% !important;
        table-layout: fixed;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
        -moz-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      img.max-width {
        max-width: 100% !important;
      }
      .column.of-2 {
        width: 50%;
      }
      .column.of-3 {
        width: 33.333%;
      }
      .column.of-4 {
        width: 25%;
      }
      @media screen and (max-width:480px) {
        .preheader .rightColumnContent,
        .footer .rightColumnContent {
            text-align: left !important;
        }
        .preheader .rightColumnContent div,
        .preheader .rightColumnContent span,
        .footer .rightColumnContent div,
        .footer .rightColumnContent span {
          text-align: left !important;
        }
        .preheader .rightColumnContent,
        .preheader .leftColumnContent {
          font-size: 80% !important;
          padding: 5px 0;
        }
        table.wrapper-mobile {
          width: 100% !important;
          table-layout: fixed;
        }
        img.max-width {
          height: auto !important;
          max-width: 480px !important;
        }
        a.bulletproof-button {
          display: block !important;
          width: auto !important;
          font-size: 80%;
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
        .columns {
          width: 100% !important;
        }
        .column {
          display: block !important;
          width: 100% !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
          margin-left: 0 !important;
          margin-right: 0 !important;
        }
      }
    </style>
    <!--user entered Head Start-->
    
     <!--End Head user entered-->
  </head>
  <body>
    <center class="wrapper" data-link-color="#000000" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
      <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
          <tr>
            <td valign="top" bgcolor="#ffffff" width="100%">
              <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td width="100%">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td>
                          <!--[if mso]>
                          <center>
                          <table><tr><td width="600">
                          <![endif]-->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                            <tr>
                              <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                
    <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
           style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
      <tr>
        <td role="module-content">
          <p>
            <?php if ($order['status'] == 'paid') { ?>
            <?=Lang::t('email.body.paid')?>
            <?php } else { ?>
            <?=Lang::t('email.body.thankYou', ['{first_name}' => $order['first_name']])?>
            <?php } ?>
          </p>
        </td>
      </tr>
    </table>
  
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="font-size:6px;line-height:10px;padding:8px 0px 14px 0px; border-bottom: 1px solid #000" valign="top" align="left">
          <a href="<?=$this->createAbsoluteUrl('site/index')?>"><img class="max-width" border="0" style="color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;height:auto !important;" src="https://new.fresh.black/assets/email_logo.png" alt="" width="100" height="37"></a>
        </td>
      </tr>
    </table>
  
    <table class="module"
           role="module"
           data-type="spacer"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 20px 0px;"
            role="module-content"
            bgcolor="">
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 18px 0px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
            <div style="text-align: center;"><span style="font-size:20px;"><strong><span id="docs-internal-guid-6bbae270-7fff-1ab5-7f5a-ac9db79cc823" style="font-weight:normal;"><span style="font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=Lang::t('email.body.orderNum', ['{order_id}' => $order['order_id']])?></span></span></strong></span></div>

        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:18px 0px 8px 0px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
            <div>
<?php if ($order['status'] == 'paid') { ?>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span id="docs-internal-guid-26700e3b-7fff-aa1b-e688-e745c64b401f" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><b><?=Lang::t('email.body.paid')?></b></span></span></p>
&nbsp;
<?php } else { ?>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span id="docs-internal-guid-26700e3b-7fff-aa1b-e688-e745c64b401f" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=Lang::t('email.body.thankYou', ['{first_name}' => $order['first_name']])?></span></span></p>
&nbsp;
<?php } ?>

<!-- <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span id="docs-internal-guid-26700e3b-7fff-aa1b-e688-e745c64b401f" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=Lang::t('email.body.hello', ['{first_name}' => $order['first_name']])?></span></span></p> -->

<div>&nbsp;</div>
</div>

        </td>
      </tr>
    </table>
    <table class="module" role="module" data-type="code" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td height="100%" valign="top">
          <div style="padding: 30px 0 0 0;"><span id="docs-internal-guid-0ff87aba-7fff-c2d6-800c-f4e68d4e4ffe" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=Lang::t('email.body.products')?></span></span></div>

<div>&nbsp;</div>

<div>
<table align="left" border="0" cellpadding="0" cellspacing="0" style="width:600px;">
	<tbody>
    <?php $assetsUrl = Yii::app()->assetManager->getBaseUrl(); ?>
    <?php foreach ($order_products as $order_product) { ?>
    <?php
      if (!empty($order_product['product_photo'])) {
        $product_photo = json_decode($order_product['product_photo'], true);
        $product_photo = $assetsUrl . '/product/' . $order_product['product_id'] . '/' . $product_photo['path']['catalog_alt']['1x'];
      } else {
        $product_photo = '';
      }
      
      if (!empty($order_product['product_alias'])) {
        $product_link = $this->createAbsoluteUrl('site/product', array('alias' => $order_product['product_alias']));
      } else {
        $product_link = $this->createAbsoluteUrl('site/index');
      }
    ?>
		<tr>
      <td style="padding: 10px 5px"><span id="docs-internal-guid-f0c1bbe8-7fff-73e8-ed9d-48453b62b43a" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?php if (!empty($product_photo)) { ?><a href="<?=$product_link?>"><img src="<?=$product_photo?>" alt="" style="width:80px;height:auto;"></a><?php } else { ?>&nbsp;<?php } ?></span></span></td>
			<td style="padding: 10px 5px"><span id="docs-internal-guid-e7c615d8-7fff-a989-f628-641f779440dc" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><a href="<?=$product_link?>" style="color:#000;text-decoration:underline"><?=$order_product['title']?></a></span></span></td>
			<td style="padding: 10px 5px"><span id="docs-internal-guid-b6f19b9d-7fff-bfb9-d352-783ddccdfe91" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=$order_product['variant_title']?></span></span></td>
			<td style="padding: 10px 5px"><span id="docs-internal-guid-7a4a7c24-7fff-e556-e2ed-38d7d997dc4d" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=$order_product['quantity']?></span></span></td>
			<td style="padding: 10px 5px"><span id="docs-internal-guid-2f1b7e2b-7fff-1708-6141-5caa36d89f8e" style="font-weight:normal;"><span style="font-size: 11pt; font-family: Arial; color: rgb(33, 33, 33); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"><?=$order_product['price']?> <?=Lang::t('layout.tip.uah')?></span></span></td>
		</tr>
    <?php } ?>
	</tbody>
</table>
</div>
        </td>
      </tr>
    </table>
    <table class="module"
           role="module"
           data-type="divider"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 0px 0px;"
            role="module-content"
            height="100%"
            valign="top"
            bgcolor="">
          <table border="0"
                 cellpadding="0"
                 cellspacing="0"
                 align="center"
                 width="100%"
                 height="1px"
                 style="line-height:1px; font-size:1px;">
            <tr>
              <td
                style="padding: 0px 0px 1px 0px;"
                bgcolor="#000000"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:10px 0px 18px 0px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
            <div><b><?=Lang::t('email.body.amount')?> <?=$order['price']?> <?=Lang::t('layout.tip.uah')?></b>
            <?php /* <br />
            <?=Lang::t('payment.emailBody.delivery')?> <?=($order['currency_code'] == 'UAH') ? $order['delivery_price'] . $order['currency_text']  : $order['currency_text'] . $order['delivery_price']?>
            <br />
            <strong><?=Lang::t('payment.emailBody.total')?> <?=($order['currency_code'] == 'UAH') ? ($order['price'] + $order['delivery_price']) . $order['currency_text']  : $order['currency_text'] . ($order['price'] + $order['delivery_price'])?>
            </strong> */ ?></div>

<div>&nbsp;</div>

        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:6px 0px 24px 0px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
            <div>
                <strong><?=Lang::t('email.body.orderClientInfo')?></strong><br>
                <?=$order['full_name']?><br>
                <?=$order['email']?><br>
                <?=$order['phone']?>
            </div>

            <div>&nbsp;</div>

            <div>
                <strong><?=Lang::t('email.body.orderDeliveryInfo')?></strong><br>
                <?php if ($order['delivery'] == 1) { ?>
                <?=Lang::t('email.body.orderDeliveryPickup')?>
                <?php } if ($order['delivery'] == 2) { ?>
                <?=Lang::t('email.body.orderDeliveryCourier')?><br>
                <?=Lang::t('email.body.orderDeliveryAddress')?> <?=$order['address']?>
                <?php } if ($order['delivery'] == 3) { ?>
                <?=Lang::t('email.body.orderDeliveryNp')?><br>
                <?=Lang::t('email.body.orderDeliveryCity')?> <?=$order['np_city']?><br>
                <?=Lang::t('email.body.orderDeliveryDepartment')?> <?=$order['np_department']?>
                <?php } ?>
            </div>

            <div>&nbsp;</div>

            <div><strong><?=Lang::t('email.body.orderPaymentInfo')?></strong><br>
            <?php if ($order['payment'] == 1) { ?>
            <?=Lang::t('email.body.orderPaymentCash')?>
            <?php } if ($order['payment'] == 2) { ?>
            <?=Lang::t('email.body.orderPaymentCard')?>
            <?php } ?>
            </div>

            <div>&nbsp;</div>
        </td>
      </tr>
    </table>
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:18px 0px 18px 0px;border-top:1px solid #000;line-height:22px;text-align:left;"
            height="100%"
            valign="top"
            align="left"
            bgcolor="">
            <?php if (!empty(Yii::app()->params->settings['phone'])) { ?>
            <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>" style="color:#000;text-decoration:none"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a>
            <?php } ?>
            <span>&nbsp;</span>
            <?php if (!empty(Yii::app()->params->settings['mail'])) { ?>
            <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>" style="color:#000;text-decoration:none"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a>
            <?php } ?>
            <span>&nbsp;</span>
            <?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
            <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" style="color:#000;text-decoration:none">Facebook</a>
            <?php } ?>
            <span>&nbsp;</span>
            <?php if (!empty(Yii::app()->params->settings['instagram'])) { ?>
            <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" style="color:#000;text-decoration:none">Instagram</a>
            <?php } ?>
        </td>
      </tr>
    </table>
  
                              </td>
                            </tr>
                          </table>
                          <!--[if mso]>
                          </td></tr></table>
                          </center>
                          <![endif]-->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </center>
  </body>
</html>