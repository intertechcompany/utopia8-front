<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$categories = Yii::app()->params->categories;
?>
<html>
	<head>
		<title><?=Lang::t('pdf.title.orderConfirmation')?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body {
				color: #43484d;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}

			table {
				border-collapse: collapse;
				border-spacing: 0;
			}

			td,
			th {
				padding: 0;
			}

			.invoice-title-sep {
				border-top: 2px solid #43484d;
				padding-top: 10px;
				margin: 20px 0 30px;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding: 10px;
				font-size: 12px;
				vertical-align: middle;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding-left: 7px;
				padding-right: 7px;
			}

			.product {
				margin-top: 10px;
				margin-bottom: 10px;
			}

			.product th {
				border-bottom: 2px solid #43484d;
			}

			.product td {
				border-bottom: 1px solid #43484d;
			}

			.product a {
				color: #43484d;
				text-decoration: underline;
			}

			.product-bottom td {
				font-size: 14px;
			}

			.confirmation-details table td {
				border: 1px solid #000;
				padding: 5px 7px;
			}
		</style>
	</head>
	<body>
		<div class="invoice-head">
			<table style="width: 100%;" autosize="1">
				<tr>
					<td><img src="<?=$assetsUrl . '/inorex_oy.svg'?>" width="200" height="46" alt="Inorex"></td>
					<td style="font-size: 20px; text-align: right; vertical-align: bottom;">
						<b><?=Lang::t('pdf.title.orderConfirmation')?></b>
					</td>
				</tr>
			</table>
		</div>
		
		<div class="invoice-title-sep">&nbsp;</div>

		<div class="invoice-head">
			<?=Lang::t('pdf.html.orderConfirmationHead')?>
		</div>
		
		<div class="invoice-details">
			<p><b><?=Lang::t('pdf.tip.orderInformation')?></b></p>

			<table class="confirmation-details" style="width: 70%;" autosize="1">
				<tbody>
					<?php /*
					<tr>
						<td width="28%"><?=Lang::t('pdf.tip.invoiceNum')?></td>
						<td width="72%">
							<table style="width: 40%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=$order['order_id']?></td>
								</tr>
							</table>
						</td>
					</tr>
					 */ ?>

					<tr>
						<td width="28%"><?=Lang::t('pdf.tip.customerInfo')?></td>
						<td width="72%">
							<table style="width: 60%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['first_name'] . ' ' . $order['personal']['last_name'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<?php if (!empty($order['personal']['address_2'])) { ?>
					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['address_2'])?></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php } ?>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['address'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="40%" style="border-right: 0"><?=CHtml::encode($order['personal']['zip'])?></td>
									<td width="60%"><?=CHtml::encode($order['personal']['city'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 40%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0; border-top: 0"><?=CHtml::encode($order['personal']['phone'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 80%;" autosize="1">
								<tr>
									<td width="100%"><?=CHtml::encode($order['personal']['email'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<?php if (!empty($order['personal']['delivery_to_address'])) { ?>
					<tr>
						<td><?=Lang::t('pdf.tip.deliveryInfo')?></td>
						<td>
							<table style="width: 60%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['delivery_first_name'] . ' ' . $order['personal']['delivery_last_name'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<?php if (!empty($order['personal']['delivery_address_2'])) { ?>
					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['delivery_address_2'])?></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php } ?>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0"><?=CHtml::encode($order['personal']['delivery_address'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 100%;" autosize="1">
								<tr>
									<td width="40%" style="border-right: 0"><?=CHtml::encode($order['personal']['delivery_zip'])?></td>
									<td width="60%"><?=CHtml::encode($order['personal']['delivery_city'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 40%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0; border-top: 0"><?=CHtml::encode($order['personal']['delivery_phone'])?></td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>
							<table style="width: 80%;" autosize="1">
								<tr>
									<td width="100%"><?=CHtml::encode($order['personal']['delivery_email'])?></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php } ?>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td><?=Lang::t('pdf.tip.deliveryMethod')?></td>
						<td>
							<table style="width: 80%;" autosize="1">
								<tr>
									<td width="100%" style="border-bottom: 0">
										<?php if ($order['delivery'] == 1) { ?>
										<?=Lang::t('checkout.label.shipping')?>
										<?php } elseif ($order['delivery'] == 2) { ?>
										<?=Lang::t('checkout.label.shippingConstruction')?>
										<?php } elseif ($order['delivery'] == 3) { ?>
										<?=Lang::t('checkout.label.shippingPost')?>
										<?php } ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td><?=Lang::t('pdf.tip.paymentMethod')?></td>
						<td>
							<table style="width: 80%;" autosize="1">
								<tr>
									<td width="100%">
										<?php if ($order['payment'] == 1) { ?>
										<?=Lang::t('checkout.label.bank')?>
										<?php } elseif ($order['payment'] == 2) { ?>
										<?=Lang::t('checkout.label.bankInvoice')?>
										<?php } ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="invoice-date" style="padding: 20px 0">
			<p><?=Lang::t('pdf.tip.orderDate')?> <?=date('d.m.Y H:i')?></p>
			<p><?=Lang::t('pdf.tip.orderConfirmationDate')?> <?=date('d.m.Y')?></p>
		</div>
		
		<div class="product">
			<table style="width: 100%;" autosize="1">
				<thead>
					<tr>
						<th style="width: 16%">&nbsp;</th>
						<th style="width: 32%; text-align: left"><?=Lang::t('pdf.tip.thProduct')?></th>
						<th style="width: 10%"><?=rtrim(Lang::t('accountOrder.tip.productCode'), ': ');?></th>
						<th style="width: 12%"><?=Lang::t('pdf.tip.thDelivery')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thQuantity')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thTotal')?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($cart as $cart_item) { ?>
					<?php
						$product = $cart_item['data'];
						$variant = $cart_item['variant'];
						$options = $cart_item['options'];
						
						$product_id = $product['product_id'];
						$variant_id = !empty($variant) ? $variant['variant_id'] : 0;
						$product_url = $this->createAbsoluteUrl('site/product', array('alias' => $product['product_alias']));

						$is_custom_product = (preg_match('#^C_[0-9abcdef]{13}#ui', $product_id)) ? true : false;

						if (!empty($variant['photo_path'])) {
							$product_image = json_decode($variant['photo_path'], true);
							$product_image_size = json_decode($variant['photo_size'], true);
							$product_image_size = $product_image_size['catalog'];
							$product_image = $assetsUrl . '/product/' . $product['product_id'] . '/variant/' . $variant['variant_id'] . '/' . $product_image['catalog'];
							
							$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 70px; max-height: 70px; width: auto; height: auto;"></a>';
						} elseif (!empty($product['product_photo'])) {
							$product_image = json_decode($product['product_photo'], true);
							$product_image_size = $product_image['size']['catalog'];
							$product_image = $assetsUrl . '/product/' . $product_id . '/' . $product_image['path']['catalog'];

							$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 70px; max-height: 70px; width: auto; height: auto;"></a>';
						} else {
							$product_image = '';
						}

						$product_options = '';

						if (!empty($product['properties'])) {
							$properties = array();

							foreach ($product['properties'] as $property) {
								$properties[] = CHtml::encode($property['property_title'] . ': ' . $property['value_title']);
							}
						
							$product_options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $properties) . '</span>';
						}

						$variant_options = '';

						if (!empty($variant)) {
							$product['product_price_type'] = $variant['variant_price_type'];
							$product['product_pack_size'] = $variant['variant_pack_size'];

							if (!empty($variant['values'])) {
								$variant_values = array();

								foreach ($variant['values'] as $value) {
									$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
								}
							
								$variant_options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $variant_values) . '</span>';
							}
						}

						$item_options = '';

						if (!empty($options)) {
							$options_values = array();

							foreach ($options as $option) {
								$options_values[] = CHtml::encode($option['property_title'] . ': ' . $option['value_title']);
							}
						
							$item_options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $options_values) . '</span>';
						}

						$product_sku = !empty($variant) ? $variant['variant_sku'] : $product['product_sku'];
						
						if ($is_custom_product) {
							$product_title = CHtml::encode($product['product_title']) . $variant_options . $item_options . $product_options;
						} else {
							$product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a>' . $variant_options . $item_options . $product_options;
						}

						$delivery = '';

						if (isset($categories[$product['category_id']])) {
							$category = $categories[$product['category_id']];

							if (!empty($category['category_delivery'])) {
								$delivery = CHtml::encode($category['category_delivery']);
							} elseif (!empty($category['parent_id'])) {
								$root_category_id = $category['parents'][0];

								if (!empty($categories[$root_category_id]['category_delivery'])) {
									$delivery = CHtml::encode($categories[$root_category_id]['category_delivery']);
								}
							}
						}

						$regular_price = !empty($variant) ? $variant['variant_price'] : $product['product_price'];
						$discount_price = Product::getDiscountPrice($regular_price, $product['category_id']);
						$discount_price['price'] = number_format((float) $discount_price['price'], 2, '.', '');
						$regular_price = number_format((float) $regular_price, 2, '.', '');

						$product['product_pack_size'] = (float) $product['product_pack_size'];
						$product['product_pack_size'] = empty($product['product_pack_size']) ? 1 : $product['product_pack_size'];

						$quantity = $cart_item['qty'] . ' ' . Lang::t('checkout.tip.pcs');

						if ($product['product_price_type'] == 'per_meter' && $product['product_pack_size'] == 1) {
							$quantity = $cart_item['qty'] . ' ' . Yii::t('app', 'meter|meters', $cart_item['qty']);
						} else {
							if ($product['product_price_type'] == 'package') {
								$quantity = number_format($product['product_pack_size'] * $cart_item['qty'], 2, '.', '') . ' m<sup>2</sup>';
							} elseif ($product['product_price_type'] == 'per_meter' && $product['product_pack_size'] > 1) {
								$quantity = number_format($product['product_pack_size'] * $cart_item['qty'], 2, '.', '') . ' m';
							}
						}
					?>
					<tr>
						<td style="text-align: center;"><?=$product_image?></td>
						<td style="text-align: left;"><?=$product_title?></td>
						<td style="text-align: center;"><?=$product_sku?></td>
						<td style="text-align: center;"><?=$delivery?></td>
						<td style="text-align: center;"><?=$quantity?></td>
						<td style="text-align: center;"><?=number_format($cart_item['qty'] * $cart_item['price'], 2, '.', '')?>€</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>

		<?php
			$vat = round($order['price'] * 0.24, 2);
			$total_with_vat = $order['price'];
			$total_without_vat = $order['price'] - $vat;
		?>
		<div class="product-bottom">
			<table style="width: 100%;" autosize="1">
				<tr>
					<td style="width: 90%; text-align: right; padding-bottom: 0"><?=Lang::t('pdf.tip.deliveryCost')?></td>
					<td style="width: 10%; text-align: center; padding-bottom: 0">&mdash;</td>
				</tr>
				<tr>
					<td style="width: 90%; text-align: right; padding-top: 5px; padding-bottom: 0"><b><?=Lang::t('pdf.tip.totalWithoutTax')?></b></td>
					<td style="width: 10%; text-align: center; padding-top: 5px; padding-bottom: 0"><?=number_format($total_without_vat, 2, '.', '')?>€</td>
				</tr>
				<tr>
					<td style="width: 90%; text-align: right; padding-top: 5px; padding-bottom: 0"><?=Lang::t('accountOrder.title.alv')?></td>
					<td style="width: 10%; text-align: center; padding-top: 5px; padding-bottom: 0"><?=number_format($vat, 2, '.', '')?>€</td>
				</tr>
				<tr>
					<td style="width: 90%; text-align: right; padding-top: 5px"><b><?=Lang::t('pdf.tip.totalWithTax')?></b></td>
					<td style="width: 10%; text-align: center; padding-top: 5px"><?=number_format($total_with_vat, 2, '.', '')?>€</td>
				</tr>
			</table>
		</div>

		<div>
			<?=Lang::t('pdf.html.confirmationBottom')?>
		</div>
	</body>
</html>