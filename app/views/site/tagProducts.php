<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="category category--catalog">
    <div class="category__head wrap">
        <div class="category__title">
            <?=!empty($tag['tag_h1']) ? CHtml::encode($tag['tag_h1']) : CHtml::encode($tag['tag_name'])?>
            <?php if ($pages->itemCount > 0) { ?>
            <small><span><?=$pages->itemCount?></span> <?=Lang::t('catalog.tip.pcs')?></small>
            <?php } ?>
        </div>

        <?php
            $sort_params = array(
                'popular' => array(
                    'title' => Lang::t('catalog.select.sortPopular'),
                    'url' => null, // default value
                ),
                'price' => array(
                    'title' => Lang::t('catalog.select.sortPrice'),
                    'url' => 'price',
                ),
                'newest' => array(
                    'title' => Lang::t('catalog.select.sortNewest'),
                    'url' => 'newest',
                ),
            );
        ?>
        <div class="category__sort">
            <?=Lang::t('catalog.tip.sort')?>
            <div class="category__sort-select">
                <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                <?php if ($sort_index == $sort) { ?>
                <div class="category__sort-value"><?=CHtml::encode($sort_param['title'])?></div>
                <?php break; } ?>
                <?php } ?>
                <select>
                    <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                    <?php 
                        $sort_url_params = ['alias' => $tag['tag_alias']];

                        if (!empty($sort_param['url'])) {
                            $sort_url_params = [
                                'alias' => $tag['tag_alias'], 
                                'sort' => $sort_param['url'],
                            ];
                        }

                        if (!empty($category)) {
                            $sort_url_params['cid'] = $category['category_id'];
                        }

                        $sort_url = $this->createUrl('tag', $sort_url_params);
                    ?>
                    <option value="<?=$sort_url?>"<?php if ($sort_index == $sort) { ?> selected<?php } ?>><?=CHtml::encode($sort_param['title'])?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <?php if (!empty($category)) { ?>
    <div class="category__catalog">
        <a href="#" class="category__mobile-filter"><i class="icon-inline icon-filter"></i><?=Lang::t('catalog.tip.filters')?></a>
        <div class="category__side">
            <form id="filter" class="category-filters" action="<?=$this->createUrl('tag', ['alias' => $tag['tag_alias']])?>" method="get">
                <div class="category-filter">
                    <div class="category-filter__head"><?= empty($categories[$category['category_id']]['sub']) && isset($categories[$category['parent_id']]) ? $categories[$category['parent_id']]['category_name'] : $categories[$category['category_id']]['category_name'] ?></div>
                    <div class="category-filter__list">
                        <?php foreach ($subcategories as $subcategory) { ?>
                        <div class="category-filter__item">
                            <a href="<?=$this->createUrl('category', ['alias' => $subcategory['category_alias']])?>" class="category-filter__link">
                                <span class="category-filter__title"><?=CHtml::encode($subcategory['category_name'])?></span>
                                <span class="category-filter__counter"><?= $subcategory['total']['total'] ? $subcategory['total']['total'] : '-' ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if (!empty($tags)) { ?>
                <div class="category-filter">
                    <div class="category-filter__head"><?=Lang::t('catalog.tip.tags')?></div>
                    <div class="category-filter__list">
                        <?php foreach ($tags as $tag_item) { ?>
                        <div class="category-filter__item">
                            <a href="<?=$this->createUrl('tag', ['alias' => $tag_item['tag_alias'], 'cid' => $category['category_id']])?>" class="category-filter__link<?php if ($tag['tag_id'] == $tag_item['tag_id']) { ?> category-filter__link--active<?php } ?>">
                                <span class="category-filter__title">
                                    <?php if (!empty($tag_item['tag_image'])) { $tag_item_image = json_decode($tag_item['tag_image'], true); ?>
                                    <img src="<?=$assetsUrl?>/tag/<?=$tag_item_image['file']?>" alt="" style="width: 20px; height: auto; margin-top: -2px; margin-right: 4px">
                                    <?php } ?>
                                    <?=CHtml::encode($tag_item['tag_name'])?>
                                </span>
                                <span class="category-filter__counter"><?= $tag_item['total'] ? $tag_item['total'] : '-' ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
		    </form>
        </div>
        <div class="category__products">
            <?php if (!empty($products)) { ?>
            <?php $this->renderPartial('productsList', ['products' => $products]); ?>
            <?php } else { ?>
            <p style="padding: 15px"><?=Lang::t('catalog.tip.productsNotFound')?></p>
            <?php } ?>

            <?php $this->renderPartial('pagination', ['pages' => $pages]); ?>
        </div>
    </div>
    <?php } else { ?>
    <div class="category__products">
        <?php if (!empty($products)) { ?>
        <?php $this->renderPartial('productsList', ['products' => $products]); ?>
        <?php } else { ?>
        <p style="padding: 15px"><?=Lang::t('catalog.tip.productsNotFound')?></p>
        <?php } ?>

        <?php $this->renderPartial('pagination', ['pages' => $pages]); ?>
    </div>
    <?php } ?>
</main>