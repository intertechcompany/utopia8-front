<?php
	/* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="extra">
	<div class="extra__video">
		<div class="extra__video-wrap">
			<video autoplay muted loop playsinline>
				<source type="video/mp4" src="<?=$assetsUrl?>/no_results.mp4"></source>
			</video>
		</div>
	</div>
	<div class="extra__content">
		<?php if (!empty($errors)) { ?>
		<div class="extra__title"><?=$errors?></div>
		<?php } else { ?>
		<div class="extra__title"><?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('search.tip.nothingFound', ['{keyword}' => CHtml::encode($keyword)]))?></div>
		<?php } ?>
        <div class="extra__text">
			<?=Lang::t('search.tip.searchHint')?>
        </div>
	</div>
</main>
<?php if (!empty($products)) { ?>
<section class="category category--blue">
	<div class="category__head wrap">
		<div class="category__title"><?=Lang::t('layout.tip.newestProducts')?></div>
	</div>
	<div class="category__products">
		<?php $this->renderPartial('productsList', ['products' => $products]); ?>
	</div>
</section>
<?php } ?>