<?php
/* @var $this SiteController */
$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<div class="wrap">
	<div class="content-divider content-divider--top"></div>
	
	<div class="page">
		<h1 class="page__title"><?=CHtml::encode($this->pageTitle)?></h1>

		<div class="page__description">
			<ul class="list-unstyled">
				<li><a href="<?=$this->createUrl('site/newest')?>">Новинки</a></li>
				<?php if (!empty($categories)) { ?>
				<?php foreach ($categories as $category) { ?>
				<li><a href="<?=$this->createUrl('site/category', array('alias' => $category['category_alias']))?>"><?=CHtml::encode($category['category_name'])?></a></li>
				<?php } ?>
				<?php } ?>
				
				<li><a href="<?=$this->createUrl('site/brands')?>">Бренды</a></li>
				<?php if (!empty($brands)) { ?>
				<?php foreach ($brands as $brand) { ?>
				<?php if (!empty($brand['brand_no_index'])) { continue; } ?>
				<li><a href="<?=$this->createUrl('site/brand', array('alias' => $brand['brand_alias']))?>"><?=CHtml::encode($brand['brand_name'])?></a></li>
				<?php } ?>
				<?php } ?>

				<?php if (!empty($products)) { ?>
				<?php foreach ($products as $product) { ?>
				<?php if (!empty($product['product_no_index'])) { continue; } ?>
				<li><a href="<?=$this->createUrl('site/product', array('alias' => $product['product_alias']))?>"><?=CHtml::encode($product['product_title'])?></a></li>
				<?php } ?>
				<?php } ?>

				<?php if (!empty($pages)) { ?>
				<?php foreach ($pages as $page) { ?>
				<?php if (!empty($page['page_no_index'])) { continue; } ?>
				<li><a href="<?=$this->createUrl('site/page', array('alias' => $page['page_alias']))?>"><?=CHtml::encode($page['page_title'])?></a></li>
				<?php } ?>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>