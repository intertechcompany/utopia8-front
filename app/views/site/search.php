<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="category category--catalog">
    <div class="category__head wrap">
        <div class="category__title">
			<?=CHtml::encode($this->pageTitle)?> 
            <?php if ($pages->itemCount > 0) { ?>
            <small><span><?=$pages->itemCount?></span> <?=Lang::t('catalog.tip.pcs')?></small>
            <?php } ?>
        </div>

		<?php if ($pages->itemCount > 0) { ?>
        <?php
            $sort_params = array(
                'popular' => array(
                    'title' => Lang::t('catalog.select.sortRelevance'),
                    'url' => null, // default value
                ),
                'price' => array(
                    'title' => Lang::t('catalog.select.sortPrice'),
                    'url' => 'price',
                ),
                'newest' => array(
                    'title' => Lang::t('catalog.select.sortNewest'),
                    'url' => 'newest',
                ),
            );
        ?>
        <div class="category__sort">
            <?=Lang::t('catalog.tip.sort')?> 
            <div class="category__sort-select">
                <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                <?php if ($sort_index == $sort) { ?>
                <div class="category__sort-value"><?=CHtml::encode($sort_param['title'])?></div>
                <?php break; } ?>
                <?php } ?>
                <select>
                    <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                    <?php 
                        $sort_url_params = ['keyword' => $keyword];

                        if (!empty($sort_param['url'])) {
                            $sort_url_params = [
                                'keyword' => $keyword, 
                                'sort' => $sort_param['url'],
                            ];
                        }

                        $sort_url = $this->createUrl('search', $sort_url_params);
                    ?>
                    <option value="<?=$sort_url?>"<?php if ($sort_index == $sort) { ?> selected<?php } ?>><?=CHtml::encode($sort_param['title'])?></option>
                    <?php } ?>
                </select>
            </div>
		</div>
		<?php } ?>
    </div>
	<div class="category__products">
		<?php if (!empty($errors)) { ?>
		<p style="padding: 15px"><?=$errors?></p>
		<?php } else { ?>
		<?php if (!empty($products)) { ?>
		<?php $this->renderPartial('productsList', ['products' => $products]); ?>
		<?php } else { ?>
		<p style="padding: 15px">Товари не знайдено</p>
		<?php } ?>
		<?php } ?>

		<?php $this->renderPartial('pagination', ['pages' => $pages]); ?>
	</div>
</main>