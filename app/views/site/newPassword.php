<?php
/* @var $this SiteController */
?>
<div class="wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1><?=CHtml::encode($this->pageTitle)?></h1>
        <form id="reset-form" method="post" data-type="newpassword" novalidate>
            <?php if (!empty($result['errorCode'])) { ?>
                <div class="error-msg"><?=$result['errorCode']?></div>
            <?php } elseif (Yii::app()->user->hasFlash('reset_error')) { ?>
                <div class="error-msg"><?=Yii::app()->user->getFlash('reset_error')?></div>
            <?php } ?>
            <input type="hidden" name="newpassword[user_id]" value="<?=$result['uid']?>">
            <input type="hidden" name="newpassword[token]" value="<?=$result['token']?>">
            <div class="form-row clearfix">
                <div class="form-col form-col-100">
                    <label for="reset-password"><?=Lang::t('newPassword.label.newPassword')?></label>
                    <input id="reset-password" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?> type="password" name="newpassword[password]" value="">
                    <?php if (!empty($result['errorFields']['password'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password'])?></div><?php } ?>
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-col form-col-100">
                    <label for="reset-password_confirm"><?=Lang::t('newPassword.label.confirmNewPassword')?></label>
                    <input id="reset-password_confirm" <?php if (isset($result['errorFields']['password_confirm'])) { ?> class="error-field"<?php } ?> type="password" name="newpassword[password_confirm]" value="">
                    <?php if (!empty($result['errorFields']['password_confirm'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password_confirm'])?></div><?php } ?>
                    <a href="<?=$this->createUrl('site/login')?>"><?=Lang::t('newPassword.link.loginToAccount')?></a>
                </div>
            </div>
            <div class="form-row form-row-btn clearfix">
                <div class="form-col form-col-100">
                    <button class="btn"><?=Lang::t('newPassword.btn.changePassword')?></button>
                </div>
            </div>
        </form>
    </div>
</div>