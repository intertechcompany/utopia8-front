<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap account-edit">
    <div class="content-divider"></div>
    <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a>
        <span class="b-sep">|</span>
        <a href="<?=$this->createUrl('site/account')?>"><?=Lang::t('layout.link.account')?></a>
        <span class="b-sep">|</span>
        <?=CHtml::encode($this->pageTitle)?>
    </div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div id="account-personal" class="account-personal">
            <h1 class="account-title"><?=Lang::t('account.title.personalData')?></h1>

            <div class="account-personal-data">
                <form id="account-details" action="<?=$this->createUrl('site/account')?>" class="account-form hidden" method="post" novalidate>
                    <?php $this->renderPartial('accountPersonalForm', array('user' => $user)); ?>
                </form>
                <form id="account-password" action="<?=$this->createUrl('site/account')?>" class="account-form hidden" method="post" novalidate>
                    <table>
                        <tbody>
                            <tr>
                                <td><label for="af-password_current"><?=Lang::t('account.tip.labelCurrentPassword')?></label></td>
                                <td><input id="af-password_current" type="password" name="user[password_current]" value=""></td>
                            </tr>
                            <tr>
                                <td><label for="af-newpassword"><?=Lang::t('account.tip.labelNewPassword')?></label></td>
                                <td><input id="af-newpassword" type="password" name="user[password]" value=""></td>
                            </tr>
                            <tr>
                                <td><label for="af-newpassword_confirm"><?=Lang::t('account.tip.labelConfirmNewPassword')?></label></td>
                                <td><input id="af-newpassword_confirm" type="password" name="user[password_confirm]" value=""></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" name="action" value="password">
                                    <button class="btn"><?=Lang::t('account.btn.changePassword')?></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>