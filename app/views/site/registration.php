<?php
/* @var $this SiteController */
?>
<div class="wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1><?= CHtml::encode($this->pageTitle) ?></h1>
        <?php if (Yii::app()->user->hasFlash('registration')) { ?>
            <div class="success-msg"><?= Yii::app()->user->getFlash('registration') ?></div>
        <?php } else { ?>
            <form id="registration" action="<?= $this->createUrl('site/registration') ?>"
                  class="registration-form" method="post">
                <?php if (!empty($result['errorCode'])) { ?>
                    <div class="error-msg"><?= $result['errorCode'] ?></div>
                <?php } elseif (Yii::app()->user->hasFlash('registration_error')) { ?>
                    <div class="error-msg"><?= Yii::app()->user->getFlash('registration_error') ?></div>
                <?php } ?>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-email"><?= Lang::t('registration.label.email') ?></label>
                        <input id="registration-email" <?php if (isset($result['errorFields']['email'])) { ?> class="error-field"<?php } ?>
                               type="email" name="registration[email]" required
                               value="<?php if (!empty($data['email']) && empty($result['errorFields']['email'])) { ?><?=$data['email']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['email'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['email']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-phone"><?= Lang::t('registration.label.phone') ?></label>
                        <input id="registration-phone" <?php if (isset($result['errorFields']['phone'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[phone]" required
                               value="<?php if (!empty($data['phone']) && empty($result['errorFields']['phone'])) { ?><?=$data['phone']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['phone'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['phone']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-first_name"><?= Lang::t('registration.label.firstName') ?></label>
                        <input id="registration-first_name" <?php if (isset($result['errorFields']['first_name'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[first_name]" required
                               value="<?php if (!empty($data['first_name']) && empty($result['errorFields']['first_name'])) { ?><?=$data['first_name']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['first_name'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['first_name']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-last_name"><?= Lang::t('registration.label.lastName') ?></label>
                        <input id="registration-last_name" <?php if (isset($result['errorFields']['last_name'])) { ?> class="error-field"<?php } ?>
                               type="text" name="registration[last_name]" required
                               value="<?php if (!empty($data['last_name']) && empty($result['errorFields']['last_name'])) { ?><?=$data['last_name']?><?php } ?>">
                        <?php if (!empty($result['errorFields']['last_name'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['last_name']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-password"><?= Lang::t('registration.label.password') ?></label>
                        <input id="registration-password" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?>
                               type="password" name="registration[password]" required>
                        <?php if (!empty($result['errorFields']['password'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['password']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row clearfix">
                    <div class="form-col form-col-100">
                        <label for="registration-password_confirm"><?= Lang::t('registration.label.passwordConfirm') ?></label>
                        <input id="registration-password_confirm"
                        <?php if (isset($result['errorFields']['password_confirm'])) { ?> class="error-field"<?php } ?>
                               type="password" name="registration[password_confirm]" required>
                        <?php if (!empty($result['errorFields']['password_confirm'][0])) { ?>
                            <div class="error-msg"><?= implode('<br>',
                            $result['errorFields']['password_confirm']) ?></div><?php } ?>
                    </div>
                </div>
                <div class="form-row form-row-btn clearfix">
                    <div class="form-col form-col-100">
                        <button class="btn"><?= Lang::t('registration.btn.submit') ?></button>
                    </div>
                </div>
            </form>
        <?php } ?>
    </div>
</div>