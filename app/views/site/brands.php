<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<div class="wrap">
    <div class="content-divider content-divider--top"></div>

    <!-- <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>">Главная</a> / Бренды
    </div> -->

    <div class="brands-list">
        <h1 class="brands-list__title">Бренды</h1>

        <?php if ($brands) { ?>
        <?php
            // group brands by letter
            $brands_list = array();

            foreach ($brands as $brand) {
                $brand_letter = $brand['brand_name'][0];

                if (preg_match('#[^a-zа-я0-9]#ui', $brand_letter)) {
                    $brand_letter = '#';
                } elseif (preg_match('#[0-9]#ui', $brand_letter)) {
                    $brand_letter = '0-9';
                }

                $brands_list[$brand_letter][] = $brand;
            }
        ?>
        <div class="brands-list__groups">
            <?php foreach ($brands_list as $brand_letter => $brands) { ?>
            <div class="brands-list__group">
                <div class="brands-list__letter"><?=$brand_letter?></div>
                <ul class="list-unstyled">
                    <?php foreach ($brands as $brand) { ?>
                    <li><a href="<?=$this->createUrl('site/brand', array('alias' => $brand['brand_alias']))?>"><?=CHtml::encode($brand['brand_name'])?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <?php } else { ?>
        <p style="font-size: 14px">Бренды не найдены.</p>
        <?php } ?>
    </div>
</div>