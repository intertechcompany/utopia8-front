<div class="account-details-row">
	<div class="account-details-tip"><?=Lang::t('account.label.firstName')?></div>
	<div class="account-details-value"><?=CHtml::encode($user['user_first_name'])?></div>
</div>
<div class="account-details-row">
	<div class="account-details-tip"><?=Lang::t('account.label.lastName')?></div>
	<div class="account-details-value"><?=CHtml::encode($user['user_last_name'])?></div>
</div>
<div class="account-details-row">
	<div class="account-details-tip"><?=Lang::t('account.label.email')?></div>
	<div class="account-details-value"><?=CHtml::encode($user['user_email'])?></div>
</div>
<div class="account-details-row">
	<div class="account-details-tip"><?=Lang::t('account.label.phone')?></div>
	<div class="account-details-value"><?=CHtml::encode($user['user_phone'])?></div>
</div>
<?php //if (!empty($user['user_zip'])) { ?>
<!--<div class="account-details-row">-->
<!--	<div class="account-details-tip">--><?//=Lang::t('checkout.label.zip')?><!--</div>-->
<!--	<div class="account-details-value">--><?//=CHtml::encode($user['user_zip'])?><!--</div>-->
<!--</div>-->
<?php //} ?>
<?php //if (!empty($user['user_city'])) { ?>
<!--<div class="account-details-row">-->
<!--	<div class="account-details-tip">--><?//=Lang::t('checkout.label.city')?><!--</div>-->
<!--	<div class="account-details-value">--><?//=CHtml::encode($user['user_city'])?><!--</div>-->
<!--</div>-->
<?php //} ?>
<?php //if (!empty($user['user_address'])) { ?>
<!--<div class="account-details-row">-->
<!--	<div class="account-details-tip">--><?//=Lang::t('checkout.label.address1')?><!--</div>-->
<!--	<div class="account-details-value">--><?//=CHtml::encode($user['user_address'])?><!--</div>-->
<!--</div>-->
<?php //} ?>
<?php //if (!empty($user['user_address_2'])) { ?>
<!--<div class="account-details-row">-->
<!--	<div class="account-details-tip">--><?//=Lang::t('checkout.label.address2')?><!--</div>-->
<!--	<div class="account-details-value">--><?//=CHtml::encode($user['user_address_2'])?><!--</div>-->
<!--</div>-->
<?php //} ?>