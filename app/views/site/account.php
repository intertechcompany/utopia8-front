<?php
/* @var $this SiteController */
$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap account-menu">
    <div class="content-divider"></div>
    <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a>
        <span class="b-sep">|</span>
        <?=CHtml::encode($this->pageTitle)?>
    </div>
    <?php $this->renderPartial('accountMenu'); ?>
    <h1><?= CHtml::encode($this->pageTitle) ?></h1>
    <h2><?= Lang::t('layout.title.account') ?></h2>
    <ul>
        <li><a href="<?= $this->createUrl('site/accountedit') ?>"><?= Lang::t('layout.title.accountEdit') ?></a></li>
        <li><a href="<?= $this->createUrl('site/accountaddress') ?>"><?= Lang::t('layout.title.accountAddress') ?></a></li>
    </ul>
    <h2><?= Lang::t('layout.title.orders') ?></h2>
    <ul>
        <li><a href="<?= $this->createUrl('site/orders') ?>"><?= Lang::t('layout.title.ordersHistory') ?></a></li>
    </ul>
    <h2><?= Lang::t('layout.title.subscription') ?></h2>
    <ul>
        <li><a href="<?= $this->createUrl('site/accountsubscription') ?>"><?= Lang::t('layout.title.editSubscription') ?></a></li>
    </ul>
</div>