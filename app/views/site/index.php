<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
?>
<section class="banners-slider banners-slider--first"<?php if (!empty($banners_1[0]['banner_color'])) { ?> style="background-color: <?=$banners_1[0]['banner_color']?>"<?php } ?>>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($banners_1 as $banner) { ?>
            <div class="swiper-slide" data-color="<?=!empty($banner['banner_color']) ? $banner['banner_color'] : '#57697e'?>">
                <?php
                    if (strpos($banner['banner_url'], '/') === 0) {
                        $banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
                    }

                    if (!empty($banner['banner_logo'])) {
                        $banner_image = json_decode($banner['banner_logo'], true);
                        $banner_image = $assetsUrl . '/banner/' . $banner_image['file'];
                    } else {
                        $banner_image = '';
                    }
                ?>
                <a href="<?=$banner['banner_url']?>" class="banners-slider__item"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
                    <?php /* <picture>
                        <source type="image/webp" srcset="<?=$staticUrl?>/images/banner.webp, <?=$staticUrl?>/images/banner@2x.webp 2x">
                        <source srcset="<?=$staticUrl?>/images/banner.jpg, <?=$staticUrl?>/images/banner@2x.jpg 2x">
                        <img src="<?=$staticUrl?>/images/banner.jpg" alt="" width="679" height="420">
                    </picture> */ ?>
                    <?php if (!empty($banner_image)) { ?>
                    <picture>
                        <img src="<?=$banner_image?>" alt="" width="680" height="420">
                    </picture>
                    <?php } ?>
                    <span class="banners-slider__text">
                        <span><?=CHtml::encode($banner['banner_name'])?></span>
                    </span>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php if (count($banners_1) > 1) { ?>
        <div class="banners-slider__arrow banners-slider__arrow--prev swiper-button-disabled"></div>
        <div class="banners-slider__arrow banners-slider__arrow--next"></div>
        <?php } ?>
    </div>
</section>
<!-- /.banners-slider -->

<?php /* <div class="ticker" data-duration="12000"> */ ?>
<div class="ticker">
    <?=Lang::t('home.html.ticker')?>
</div>
<!-- /.ticker -->

<section class="banners">
    <?php foreach ($banners_2 as $banner) { ?>
    <div class="banners__item">
        <?php
            if (strpos($banner['banner_url'], '/') === 0) {
                $banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
            }

            if (!empty($banner['banner_logo'])) {
                $banner_image = json_decode($banner['banner_logo'], true);
                $banner_image = $assetsUrl . '/banner/' . $banner_image['file'];
            } else {
                $banner_image = '';
            }
        ?>
        <a href="<?=$banner['banner_url']?>" <?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
            <?php /* <picture>
                <source type="image/webp" srcset="<?=$staticUrl?>/images/banner.webp, <?=$staticUrl?>/images/banner@2x.webp 2x">
                <source srcset="<?=$staticUrl?>/images/banner.jpg, <?=$staticUrl?>/images/banner@2x.jpg 2x">
                <img src="<?=$staticUrl?>/images/banner.jpg" alt="" width="679" height="420">
            </picture> */ ?>
            <?php if (!empty($banner_image)) { ?>
            <picture>
                <img src="<?=$banner_image?>" alt="">
            </picture>
            <?php } ?>
            <span class="banners__text">
                <span><?=CHtml::encode($banner['banner_name'])?></span>
            </span>
        </a>
    </div>
    <?php } ?>
</section>
<!-- /.banners -->

<?php foreach ($banners_p_1 as $banner) { ?>
<section class="category category--brown">
    <div class="category__head wrap"<?php if (!empty($banner['banner_color'])) { ?> style="background-color: <?=$banner['banner_color']?>"<?php } ?>>
        <div class="category__title"><?=CHtml::encode($banner['banner_name'])?></div>
        <?php if (!empty($banner['banner_url']) && !empty($banner['banner_button'])) { ?>
        <div class="category__more-link"><a href="<?=CHtml::encode($banner['banner_url'])?>"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>><?=CHtml::encode($banner['banner_button'])?> ↗</a></div>
        <?php } ?>
    </div>
    <?php if (!empty($banner['products'])) { ?>
    <div class="category__products">
        <?php $this->renderPartial('productsList', ['products' => $banner['products']]); ?>
    </div>
    <?php } ?>
</section>
<!-- /.category -->
<?php } ?>

<section class="brand-section">
    <div class="brand-section__title"><?=Lang::t('home.tip.brandTitle')?></div>
    <div class="brand-section__info">
        <?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('home.tip.brandText'))?>
        <?php /* <div class="brand-section__more">
            <a href="#">Перейти в раздел ↗</a>
        </div> */ ?>
    </div>
</section>
<!-- /.brand-section -->

<section class="banners">
    <?php foreach ($banners_3 as $banner) { ?>
    <div class="banners__item">
        <?php
            if (strpos($banner['banner_url'], '/') === 0) {
                $banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
            }

            if (!empty($banner['banner_logo'])) {
                $banner_image = json_decode($banner['banner_logo'], true);
                $banner_image = $assetsUrl . '/banner/' . $banner_image['file'];
            } else {
                $banner_image = '';
            }
        ?>
        <a href="<?=$banner['banner_url']?>" <?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
            <?php /* <picture>
                <source type="image/webp" srcset="<?=$staticUrl?>/images/banner.webp, <?=$staticUrl?>/images/banner@2x.webp 2x">
                <source srcset="<?=$staticUrl?>/images/banner.jpg, <?=$staticUrl?>/images/banner@2x.jpg 2x">
                <img src="<?=$staticUrl?>/images/banner.jpg" alt="" width="679" height="420">
            </picture> */ ?>
            <?php if (!empty($banner_image)) { ?>
            <picture>
                <img src="<?=$banner_image?>" alt="">
            </picture>
            <?php } ?>
            <span class="banners__text">
                <span><?=CHtml::encode($banner['banner_name'])?></span>
            </span>
        </a>
    </div>
    <?php } ?>
</section>
<!-- /.banners -->

<?php foreach ($banners_p_2 as $banner) { ?>
<section class="category">
    <div class="category__head wrap"<?php if (!empty($banner['banner_color'])) { ?> style="background-color: <?=$banner['banner_color']?>"<?php } ?>>
        <div class="category__title"><?=CHtml::encode($banner['banner_name'])?></div>
        <?php if (!empty($banner['banner_url']) && !empty($banner['banner_button'])) { ?>
        <div class="category__more-link"><a href="<?=CHtml::encode($banner['banner_url'])?>"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>><?=CHtml::encode($banner['banner_button'])?> ↗</a></div>
        <?php } ?>
    </div>
    <?php if (!empty($banner['products'])) { ?>
    <div class="category__products">
        <?php $this->renderPartial('productsList', ['products' => $banner['products']]); ?>
    </div>
    <?php } ?>
</section>
<!-- /.category -->
<?php } ?>

<?php if ($faq['page_type'] == 'faq' && !empty($faq['page_content_faq_main'])) { ?>
<section class="faq-section">
    <?php $faq_content = json_decode($faq['page_content_faq_main'], true); ?>
    <ul class="faq-section__list list-unstyled">
        <?php foreach ($faq_content as $faq_index => $faq_item) { ?>
        <li>
            <a href="#" class="faq-section__head<?php if ($faq_index > 0) { ?> faq-section__head--collapsed<?php } ?>"><?=CHtml::encode($faq_item['title'])?><i></i></a>
            <div class="faq-section__content"><?=strip_tags($faq_item['text'], 'a')?></div>
        </li>
        <?php } ?>
    </ul>
    <?php /* str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('home.tip.faqText')) */ ?>
    <?php /* <ul class="faq-section__list list-unstyled">
        <li><a href="#">Чи можу я зберегти обрані товари, щоб замовити їх пізніше?</a></li>
        <li><a href="#">Чи можу я приміряти товар?</a></li>
        <li><a href="#">Що робити, якщо товар мені не підійшов?</a></li>
        <li><a href="#">Як часто на сайті з'являються нові товари?</a></li>
    </ul> */ ?>

    <div class="faq-section__more">→ <a href="<?=$this->createUrl('page', ['alias' => $faq['page_alias']])?>"><?=CHtml::encode($faq['page_title'])?></a></div>
</section>
<!-- /.faq-section -->
<?php } ?>

<?php foreach ($banners_p_3 as $banner) { ?>
<section class="category category--dark-green">
    <div class="category__head wrap"<?php if (!empty($banner['banner_color'])) { ?> style="background-color: <?=$banner['banner_color']?>"<?php } ?>>
        <div class="category__title"><?=CHtml::encode($banner['banner_name'])?></div>
        <?php if (!empty($banner['banner_url']) && !empty($banner['banner_button'])) { ?>
        <div class="category__more-link"><a href="<?=CHtml::encode($banner['banner_url'])?>"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>><?=CHtml::encode($banner['banner_button'])?> ↗</a></div>
        <?php } ?>
    </div>
    <?php if (!empty($banner['products'])) { ?>
    <div class="category__products">
        <?php $this->renderPartial('productsList', ['products' => $banner['products']]); ?>
    </div>
    <?php } ?>
</section>
<!-- /.category -->
<?php } ?>
