<?php $holidays = ['01.01', '02.01', '07.01', '01.05']; ?>
<?php if ($product['product_instock'] == 'in_stock') { ?>
<div class="product-delivery__title"><?=Lang::t('product.tip.want')?></div>
<?php if ($product['product_stock_type'] == 'preorder' || $product['product_stock_type'] == 'online') { ?>
<?php
	$date = new DateTime('today');
	$days = ($product['product_stock_type'] == 'preorder') ? $product['product_preorder_days'] : $product['product_online_days'];

	for ($i = 0; $i < $days; $i++) {
		if ($date->format('D') == 'Sun' || in_array($date->format('d.m'), $holidays)) {
			$date->modify('+2 days');
		} else {
			$date->modify('+1 days');
		}
	}
	
	// check if not holiday
	if ($date->format('d.m') == '01.01') {
		$date->modify('+2 days');
	} elseif ($date->format('D') == 'Sun' || in_array($date->format('d.m'), $holidays)) {
		$date->modify('+1 days');
	}
?>
<div class="product-delivery__line">
	<?php /* <span><?=Lang::t('product.tip.onlineOnly')?></span> */ ?>
	<span><?=Lang::t('product.tip.npDelivery')?></span>
	<span><?=Yii::app()->dateFormatter->format('d MMMM', $date->getTimestamp())?></span>
</div>
<?php } else { ?>
<?php
	$np_date = '';
	$kh_stock = 0;
	$kh_date = '';
	$od_stock = 0;
	$od_date = '';
	
	$today = new DateTime('today');
	$tomorrow = new DateTime('tomorrow');
	$timepoint = new DateTime('today');
	$timepoint->setTime(13, 30, 0, 0);

	$delivery_date = new DateTime('now');

	if ($delivery_date->format('D') == 'Sun' || $delivery_date > $timepoint) {
		$delivery_date->modify('+1 days');
	}

	// check if not holiday
	if ($delivery_date->format('d.m') == '01.01') {
		$delivery_date->modify('+2 days');
	} elseif ($delivery_date->format('D') == 'Sun' || in_array($delivery_date->format('d.m'), $holidays)) {
		$delivery_date->modify('+1 days');
	}

	$delivery_date->setTime(0, 0, 0, 0);

	if ($delivery_date == $today) {
		$np_date = Lang::t('layout.tip.today');
	} elseif ($delivery_date == $tomorrow) {
		$np_date = Lang::t('layout.tip.tomorrow');
	} else {
		$np_date = Yii::app()->dateFormatter->format('d MMMM', $delivery_date->getTimestamp());
	}

	foreach ($product_stores as $product_store) {
		if ($product_store['store_name'] == 'U8 Харьков') {
			$kh_stock = $product_store['quantity'];
		} elseif ($product_store['store_name'] == 'U8 Одесса') {
			$od_stock = $product_store['quantity'];
		}
	}

	$worktime = new DateTime('today');
	$worktime->setTime(21, 0, 0, 0);

	$kh_stock_date = new DateTime('now');
	$od_stock_date = new DateTime('now');

	$days_to_store_delivery = 4;

	if ($kh_stock && $od_stock) {
		if ($kh_stock_date > $worktime) {
			$kh_stock_date->modify('+1 days');
			$od_stock_date->modify('+1 days');
		}
	} elseif ($kh_stock) {
		if ($kh_stock_date > $worktime) {
			$kh_stock_date->modify('+1 days');
		}

		for ($i = 0; $i < $days_to_store_delivery; $i++) {
			if ($od_stock_date->format('D') == 'Sun' || in_array($od_stock_date->format('d.m'), $holidays)) {
				$od_stock_date->modify('+2 days');
			} else {
				$od_stock_date->modify('+1 days');
			}
		}

		// check if not holiday
		if ($od_stock_date->format('d.m') == '01.01') {
			$od_stock_date->modify('+2 days');
		} elseif ($od_stock_date->format('D') == 'Sun' || in_array($od_stock_date->format('d.m'), $holidays)) {
			$od_stock_date->modify('+1 days');
		}
	} elseif ($od_stock) {
		if ($od_stock_date > $worktime) {
			$od_stock_date->modify('+1 days');
		}

		for ($i = 0; $i < $days_to_store_delivery; $i++) {
			if ($kh_stock_date->format('D') == 'Sun' || in_array($kh_stock_date->format('d.m'), $holidays)) {
				$kh_stock_date->modify('+2 days');
			} else {
				$kh_stock_date->modify('+1 days');
			}
		}

		// check if not holiday
		if ($kh_stock_date->format('d.m') == '01.01') {
			$kh_stock_date->modify('+2 days');
		} elseif ($kh_stock_date->format('D') == 'Sun' || in_array($kh_stock_date->format('d.m'), $holidays)) {
			$kh_stock_date->modify('+1 days');
		}
	}

	$kh_stock_date->setTime(0, 0, 0, 0);

	if ($kh_stock_date == $today) {
		$kh_date = Lang::t('layout.tip.today');
	} elseif ($kh_stock_date == $tomorrow) {
		$kh_date = Lang::t('layout.tip.tomorrow');
	} else {
		$kh_date = Yii::app()->dateFormatter->format('d MMMM', $kh_stock_date->getTimestamp());
	}

	$od_stock_date->setTime(0, 0, 0, 0);
	
	if ($od_stock_date == $today) {
		$od_date = Lang::t('layout.tip.today');
	} elseif ($od_stock_date == $tomorrow) {
		$od_date = Lang::t('layout.tip.tomorrow');
	} else {
		$od_date = Yii::app()->dateFormatter->format('d MMMM', $od_stock_date->getTimestamp());
	}
?>
<div class="product-delivery__line">
	<span><?=Lang::t('product.tip.npDelivery')?></span>
	<span><?=$np_date?></span>
</div>
<div class="product-delivery__line">
	<span><?=Lang::t('product.tip.storeKh')?></span>
	<span><?=$kh_date?></span>
</div>
<div class="product-delivery__line">
	<span><?=Lang::t('product.tip.storeOd')?></span>
	<span><?=$od_date?></span>
</div>
<?php } ?>
<?php } ?>