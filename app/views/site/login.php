<?php
/* @var $this SiteController */
?>
<div class="wrap">
    <div class="content-divider"></div>

    <div class="auth-form">
        <h1><?=CHtml::encode($this->pageTitle)?></h1>
        <?php if (Yii::app()->user->hasFlash('login')) { ?>
        <div class="success-msg"><?=Yii::app()->user->getFlash('login')?></div>
        <?php } else { ?>
        <form id="login-form" method="post" data-type="login" novalidate>
            <?php if (!empty($result['errorCode'])) { ?>
            <div class="error-msg"><?=$result['errorCode']?></div>
            <?php } elseif (Yii::app()->user->hasFlash('login_error')) { ?>
            <div class="error-msg"><?=Yii::app()->user->getFlash('login_error')?></div>
            <?php } ?>
            <div class="form-row clearfix">
                <div class="form-col form-col-100">
                    <label for="login-email"><?=Lang::t('login.label.login')?></label>
                    <input id="login-email" <?php if (isset($result['errorFields']['login'])) { ?> class="error-field"<?php } ?> type="email" name="login[login]" value="<?=CHtml::encode($model->login)?>">
                    <?php if (!empty($result['errorFields']['login'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['login'])?></div><?php } ?>
                </div>
            </div>
            <div class="form-row clearfix">
                <div class="form-col form-col-100">
                    <label for="login-password"><?=Lang::t('login.label.password')?></label>
                    <input id="login-password" <?php if (isset($result['errorFields']['password'])) { ?> class="error-field"<?php } ?> type="password" name="login[password]" value="">
                    <?php if (!empty($result['errorFields']['password'][0])) { ?><div class="error-msg"><?=implode('<br>', $result['errorFields']['password'])?></div><?php } ?>
                    <a href="<?=$this->createUrl('site/reset')?>"><?=Lang::t('login.link.forgotPassword')?></a>
                </div>
            </div>
            <div class="form-row form-row-btn clearfix">
                <div class="form-col form-col-100">
                    <button class="btn"><?=Lang::t('login.btn.logIn')?></button>
                </div>
            </div>
        </form>
        <?php } ?>
    </div>
</div>