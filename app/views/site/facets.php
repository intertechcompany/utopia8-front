<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php if ($sort != 'popular') { ?>
<input type="hidden" name="sort" value="<?=$sort?>">
<?php } ?>
<?php
	$this->renderPartial('facetPrice', ['price' => $facets->getPriceFacet()]);
?>
<?php
	$properties = $facets->getPropertiesFacets();

	if (!empty($properties)) {
		$total_properties = count($properties);
		$p_index = 0;

		foreach ($properties as $property_id => $property) {
			$property_class = [];

            if ($property['property_color']) {
				$property_class[] = ' category-filter--color';
			}
			
			if ($p_index > 0 && $total_properties > 2) {
				$property_class[] = ' category-filter--collapsed';
			}

			$p_index++;
?>
<?php if (!empty($property['values'])) { ?>
<div class="category-filter<?=implode('', $property_class)?>">
	<?php 
		$total_selected = 0;

		foreach ($property['values'] as $value_id => $value) {
			if ($value['selected']) {
				$total_selected++;
			}
		}
	?>
	<div class="category-filter__head">
		<a href="#" class="category-filter__toggle"><?=CHtml::encode($property['property_title'])?></a>
	</div>
	<div class="category-filter__list">
		<?php $index = 0; ?>
		<?php foreach ($property['values'] as $value_id => $value) { ?>
		<?php
			$value_color = '';

			if ($property['property_color']) {
				if (!empty($value['value_color'])) {
					$value_color = ' style="background: ' . $value['value_color'] . ';"';
				} elseif (!empty($value['value_multicolor'])) {
					$value_color = ' style="background: linear-gradient(90deg, rgba(100,115,255,1) 0%, rgba(96,255,245,1) 33%, rgba(113,255,127,1) 66%, rgba(191,189,171,1) 100%);"';
				}
			}
		?>
		<?php if ($value['total']) { ?>
		<div class="category-filter__item<?php if ($index >= 10) { ?> category-filter__item--more category-filter__item--hidden<?php } ?>">
			<label for="option-<?=$value_id?>" class="category-filter__option">
				<input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" <?php if ($value['selected']) { ?> checked<?php } ?>>
				<span class="category-filter__checkbox"<?=$value_color?>></span>
				<span class="category-filter__title"><?=CHtml::encode($value['value_title'])?></span>
				<span class="category-filter__counter"><?=$value['total']?></span>
			</label>
		</div>
		<?php } else { ?>
		<div class="category-filter__item<?php if ($index >= 10) { ?> category-filter__item--more category-filter__item--hidden<?php } ?>">
			<label for="option-<?=$value_id?>" class="category-filter__option category-filter__option--disabled">
				<input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" disabled <?php if ($value['selected']) { ?> checked<?php } ?>>
				<span class="category-filter__checkbox"<?=$value_color?>></span>
				<span class="category-filter__title"><?=CHtml::encode($value['value_title'])?></span>
				<span class="category-filter__counter">-</span>
			</label>
		</div>
		<?php } ?>
		<?php $index++; } ?>
		<?php if (count($property['values']) > 10) { ?>
		<div class="category-filter__more">
			<a href="#" class="category-filter__more-toggle">
				<span><?=Lang::t('catalog.btn.filterShowMore')?></span>
				<span><?=Lang::t('catalog.btn.filterShowLess')?></span>
			</a>
		</div>
		<?php } ?>
	</div>
</div>
<?php } ?>
<?php /*
<div class="filter__group">
	<div class="filter__title"><a class="collapsed" href="#"><?=CHtml::encode($property['property_title'])?><i></i></a></div>
	<div class="filter__options hidden">
		<?php if (!empty($property['values'])) { ?>
		<?php
			uasort($property['values'], function($a, $b) {
				return strnatcmp($a['value_title'], $b['value_title']);
			});
		?>
		<ul class="filter__list<?php if ($property['property_size']) { ?> filter__list--size<?php } ?> list-unstyled">
			<?php foreach ($property['values'] as $value_id => $value) { ?>
			<?php if ($value['total']) { ?>
			<li><label for="option-<?=$value_id?>"><input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" <?php if ($value['selected']) { ?> checked<?php } ?>><span><?=CHtml::encode($value['value_title'])?>&nbsp;<small>(<?=$value['total']?>)</small></span></label></li>
			<?php } else { ?>
			<li><label class="disabled" for="option-<?=$value_id?>"><input id="option-<?=$value_id?>" type="checkbox" name="filter[p][]" value="<?=$value_id?>" disabled<?php if ($value['selected']) { ?> checked<?php } ?>><span><?=CHtml::encode($value['value_title'])?></span></label></li>
			<?php } ?>
			<?php } ?>
		</ul>
		<?php } else { ?>
		<p>Нет доступных опций</p>
		<?php } ?>
	</div>
</div> */ ?>
<?php } } ?>

<?php /*
	$length = $facets->getLengthFacet();

	if ($length['min'] != $length['max']) {
		if ($length['from'] == $length['min']) {
			$length['from'] = '';
		}

		if ($length['to'] == $length['max']) {
			$length['to'] = '';
		}
?>
<div class="f-block">
	<div class="fb-title"><a class="collapsed" href="#"><?=Lang::t('facets.tip.length')?><i></i></a></div>
	<div class="fb-price hidden">
		<span>
			<label for="filter-from-length"><?=Lang::t('global.tip.from')?></label><input id="filter-from-length" type="text" name="filter[length][from]" placeholder="<?=$length['min']?>" value="<?=$length['from']?>">
		</span>
		<span>
			<label for="filter-to-length"><?=Lang::t('facets.tip.to')?></label><input id="filter-to-length" type="text" name="filter[length][to]" placeholder="<?=$length['max']?>" value="<?=$length['to']?>">
		</span>

		<div class="fb-range-slider">
			<input id="range-slider-length" type="hidden" name="length_hidden" data-min="<?=$length['min']?>" data-max="<?=$length['max']?>" data-from="<?=$length['from']?>" data-to="<?=$length['to']?>" disabled>
		</div>

		<div class="fb-apply hidden">
			<button class="btn"><?=Lang::t('facets.btn.apply')?></button>
		</div>
	</div>
</div>
<?php } ?>

<?php
	$width = $facets->getWidthFacet();

	if ($width['min'] != $width['max']) {
		if ($width['from'] == $width['min']) {
			$width['from'] = '';
		}

		if ($width['to'] == $width['max']) {
			$width['to'] = '';
		}
?>
<div class="f-block">
	<div class="fb-title"><a class="collapsed" href="#"><?=Lang::t('facets.tip.width')?><i></i></a></div>
	<div class="fb-price hidden">
		<span>
			<label for="filter-from-width"><?=Lang::t('global.tip.from')?></label><input id="filter-from-width" type="text" name="filter[width][from]" placeholder="<?=$width['min']?>" value="<?=$width['from']?>">
		</span>
		<span>
			<label for="filter-to-width"><?=Lang::t('facets.tip.to')?></label><input id="filter-to-width" type="text" name="filter[width][to]" placeholder="<?=$width['max']?>" value="<?=$width['to']?>">
		</span>

		<div class="fb-range-slider">
			<input id="range-slider-width" type="hidden" name="width_hidden" data-min="<?=$width['min']?>" data-max="<?=$width['max']?>" data-from="<?=$width['from']?>" data-to="<?=$width['to']?>" disabled>
		</div>

		<div class="fb-apply hidden">
			<button class="btn"><?=Lang::t('facets.btn.apply')?></button>
		</div>
	</div>
</div>
<?php } */ ?>