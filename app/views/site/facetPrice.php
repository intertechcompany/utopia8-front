<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	
	if ($price['min'] != $price['max']) {
?>
<div class="category-filter">
	<div class="category-filter__head">
		<a href="#" class="category-filter__toggle"><?=Lang::t('catalog.tip.filterPrice')?></a>
	</div>
	<div class="category-filter__price">
		<span>
			<input class="category-filter__price-from" type="text" name="from" placeholder="<?=$price['min']?>" value="<?=($price['current_from'] === false || $price['current_from'] < $price['min']) ? $price['min'] : $price['current_from']?>">
			<?php if ($price['current_from'] === false) { ?>
			<input type="hidden" name="filter[price][from]" value="" disabled>
			<?php } else { ?>
			<input type="hidden" name="filter[price][from]" value="<?=$price['current_from']?>">
			<?php } ?>
		</span>
		<span class="category-filter__price-sep">–</span>
		<span>
			<input class="category-filter__price-to" type="text" name="to" placeholder="<?=$price['max']?>" value="<?=($price['current_to'] === false || $price['current_to'] > $price['max']) ? $price['max'] : $price['current_to']?>">
			<?php if ($price['current_to'] === false) { ?>
			<input type="hidden" name="filter[price][to]" value="" disabled>
			<?php } else { ?>
			<input type="hidden" name="filter[price][to]" value="<?=$price['current_to']?>">
			<?php } ?>
		</span>
		<span class="category-filter__price-currency"><?=Lang::t('layout.tip.uah')?></span>
		<button type="button" class="category-filter__apply category-filter__apply--hidden"><?=Lang::t('catalog.btn.filterApply')?></button>

		<div class="category-filter__slider">
			<div class="category-filter__slider-range" data-min="<?=$price['min']?>" data-max="<?=$price['max']?>" data-from="<?=$price['from']?>" data-to="<?=$price['to']?>"></div>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="category-filter category-filter--hidden">
	<div class="category-filter__head">
		<a href="#" class="category-filter__toggle"><?=Lang::t('catalog.tip.filterPrice')?></a>
	</div>
	<div class="category-filter__price">
		<?php if ($price['current_from'] === false) { ?>
		<input type="hidden" name="filter[price][from]" value="" disabled>
		<?php } else { ?>
		<input type="hidden" name="filter[price][from]" value="<?=$price['current_from']?>">
		<?php } ?>
		<?php if ($price['current_to'] === false) { ?>
		<input type="hidden" name="filter[price][to]" value="" disabled>
		<?php } else { ?>
		<input type="hidden" name="filter[price][to]" value="<?=$price['current_to']?>">
		<?php } ?>
	</div>
</div>
<?php } ?>