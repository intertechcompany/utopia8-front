<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php if (!empty($categories)) { ?>
<div class="h-menu">
	<div class="wrap">
		<ul class="list-unstyled">
			<?php foreach ($categories as $category) { ?>
			<li><a href="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>"><span><?=CHtml::encode($category['category_name'])?></span></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<?php } ?>