<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($collections as $collection) { ?><div class="product-card collection-card">
	<?php
		$collection_url = $this->createUrl('site/collection', array('category_alias' => $category['category_alias'], 'collection_alias' => $collection['collection_alias']));

		if (!empty($collection['collection_photo'])) {
			$collection_image = json_decode($collection['collection_photo'], true);
			$collection_image_size = $collection_image['size']['catalog'];
			$collection_image = $assetsUrl . '/collection/' . $collection['collection_id'] . '/' . $collection_image['path']['catalog'];
		} else {
			$collection_image = '';
		}

		$product_price = (float) $collection['product_price'];
		$product_pack_size = (float) $collection['product_pack_size'];
		$product_pack_size = empty($product_pack_size) ? 1 : $product_pack_size;

		if ($collection['product_price_type'] == 'package' || ($collection['product_price_type'] == 'per_meter' && $product_pack_size > 1)) {
			$collection_price = round($product_price / $product_pack_size, 2);
		} else {
			$collection_price = $product_price;
		}
		
		$discount_price = Product::getDiscountPrice($collection_price, $category['category_id']);
	?>
	<div class="pc-img">
		<?php if (!empty($collection_image)) { ?>
		<a href="<?=$collection_url?>"><img src="<?=$collection_image?>" width="<?=$collection_image_size['w']?>" height="<?=$collection_image_size['h']?>" alt="<?=CHtml::encode($collection['collection_title'])?>"></a>
		<?php } else { ?>
		<a href="<?=$collection_url?>"></a>
		<?php } ?>
	</div>
	<div class="pc-head">
		<div class="pc-title"><a href="<?=$collection_url?>"><?=CHtml::encode($collection['collection_title'])?></a></div>
		<?php if (!empty($collection['collection_country'])) { ?>
		<div class="pc-brand"><?=CHtml::encode($collection['collection_country'])?></div>
		<?php } ?>
	</div>
	<div class="pc-bottom">
		<div class="pc-price">
			<?php if ($discount_price['discount']) { ?>
			<span class="pcp-standart"><i class="pcp-tip"><?=Lang::t('global.tip.standartPrice')?></i> <span><small><?=Lang::t('global.tip.from')?></small> <?=number_format($collection_price, 2, '.', '')?>€</span></span>
			<span class="pcp-actual"><i class="pcp-tip"><?=Lang::t('global.tip.yourPrice')?></i> <span><small><?=Lang::t('global.tip.from')?></small> <?=number_format($discount_price['price'], 2, '.', '')?>€</span></span>
			<?php } else { ?>
			<span class="pcp-actual pcp-100"><span><small><?=Lang::t('global.tip.from')?></small> <?=number_format($collection_price, 2, '.', '')?>€</span></span>
			<?php } ?>
		</div>
	</div>
</div><?php } ?>