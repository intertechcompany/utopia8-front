<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];

    // variants
	$is_variants_type = ($product['product_price_type'] == 'variants') ? true : false;
	$variant_id = 0;
	$variants = array();
	$variants_lang = array();
	$selected_variant = array();
	$active_properties = array();
	
	// options
	$options = array();
	$selected_options = array();
	$options_price = 0;
	$options_regular_price = 0;

	$variant_properties_map = [];

	if ($is_variants_type && !empty($product_variants['variants'])) {
		$variants_lang = array(
			'uah' => Lang::t('layout.tip.uah'),
			'outOfStock' => Lang::t('product.tip.outOfStock'),
		);
		
		// prepare selected variant
		if (!empty($vid) && isset($product_variants['variants'][$vid])) {
			$selected_variant = $product_variants['variants'][$vid];
			$product_sku = $selected_variant['variant_sku'];

			// get selected properties
			$selected_properties = $selected_variant['values'];
			$total_selected = count($selected_properties);

			// iterate each selected property value
			foreach ($selected_properties as $selected_property_id => $selected_value_id) {
				// iterate each variant
				foreach ($product_variants['variants'] as $variant_index => $variant) {
					// iterate variant values
					foreach ($variant['values'] as $property_id => $value_id) {
						// collect properties only for selected values
						if ($property_id != $selected_property_id) {
							continue;
						}

						if (!isset($active_properties[$property_id])) {
							$active_properties[$property_id] = array();
						}

						$active_properties[$property_id][$value_id] = $value_id;
					}
				}
			}
		}

		// prepare variants
		foreach ($product_variants['variants'] as $product_variant) {
			$values = $product_variant['values'];

			sort($values, SORT_NUMERIC);
			$variant_key = implode('_', $values);

			$variant_price = (float) $product_variant['variant_price'];
			$variant_regular_price = (float) $product_variant['variant_price_old'];

			if (empty($variant_regular_price)) {
				$variant_regular_price = $variant_price;
			}

			$variant_photos = array();

			if (!empty($product_variant['photos'])) {
				foreach ($product_variant['photos'] as $variant_photo) {
					$variant_photo['photo_path'] = json_decode($variant_photo['photo_path'], true);
					$variant_photo['photo_size'] = json_decode($variant_photo['photo_size'], true);
					
					$variant_photos[] = $variant_photo;
				}
			}

			$variant_current_price = $variant_price;
			
			$variants[$variant_key] = array(
				'variant_id' => $product_variant['variant_id'],
				'sku' => $product_variant['variant_sku'],
				'in_stock' => $product_variant['variant_instock'],
				'current_price' => $variant_current_price,
				'price' => $variant_price,
				'regular_price' => $variant_regular_price,
				'price_type' => $product_variant['variant_price_type'],
				'values' => $product_variant['values'],
				'photos' => $variant_photos,
				'delivery' => $this->renderPartial('productVariantDelivery', [
					'product' => $product,
					'variant' => $product_variant,
					'variant_stores' => $product_variant['stores'],
				], true),
			);

			foreach ($product_variant['values'] as $property_id => $value_id) {
                if (!isset($variant_properties_map[$value_id])) {
					$variant_properties_map[$value_id] = [];
                }
				
				foreach ($product_variant['values'] as $c_property_id => $c_value_id) {
					if ($c_property_id == $property_id) {
						continue;
					}

					if (isset($variant_properties_map[$value_id][$c_property_id])) {
						if (!in_array($c_value_id, $variant_properties_map[$value_id][$c_property_id])) {
							$variant_properties_map[$value_id][$c_property_id][] = $c_value_id;
						}
					} else {
						$variant_properties_map[$value_id][$c_property_id] = [$c_value_id];
					}
				}
			}

			if (!empty($selected_variant) && $product_variant['variant_id'] == $selected_variant['variant_id']) {
				$selected_variant['json'] = $variants[$variant_key];
				$selected_variant['delivery'] = $variants[$variant_key]['delivery'];
				
				// set price to selected variant
				$selected_variant['price_data'] = array(
					'price' => $variant_price,
					'regular_price' => $variant_regular_price,
					'price_type' => $product_variant['variant_price_type'],
				);
			}
		}
	}

	if (!empty($product_options['options'])) {
		// prepare options lists
		foreach ($product_options['options'] as $product_option) {
			$value_id = $product_option['value_id'];
			
			$options[$value_id] = array(
				'option_id' => $product_option['option_id'],
				'price' => (float) $product_option['option_price'],
				'regular_price' => (float) $product_option['option_price'],
			);

			if (!empty($option_ids) && in_array($value_id, $option_ids)) {
				$selected_options[$value_id] = array(
					'option_id' => $product_option['option_id'],
				);
			}
		}
	}

	// setup product price and other attributes
	if (empty($selected_variant)) {
		$product_price = (float) $product['product_price'];
		$discount_price = (float) $product['product_price_old'];
		$price_type = $product['product_price_type'];
	} else {		
		$variant_id = $selected_variant['variant_id'];
		$price_type = $selected_variant['price_data']['price_type'];
		
		$product_price = $selected_variant['price_data']['price'];
		$discount_price = $selected_variant['price_data']['regular_price'];
	}
?>
<main id="product" class="product" data-url="<?=$this->canonicalUrl?>" data-id="<?=$product['product_id']?>">
	<?php if (!empty($categories[$product['category_id']]['parents'])) { ?>
	<div class="product__breadcrumbs wrap">
		<?php foreach ($categories[$product['category_id']]['parents'] as $index => $parent_id) { ?>
		<a href="<?=$this->createUrl('category', ['alias' => $categories[$parent_id]['category_alias']])?>"><?=CHtml::encode($categories[$parent_id]['category_name'])?></a> › 
		<?php } ?>
		<span><?=CHtml::encode($product['product_title'])?></span>
	</div>
	<?php } ?>

	<div class="product__gallery">
		<?php if (!empty($product['product_tip'])) { ?>
		<div class="product__ticker product__ticker--mobile"><?=CHtml::encode($product['product_tip'])?></div>
		<?php } ?>
		
		<?php if (!empty($product_photos)) { ?>
		<div class="product-gallery-mobile">    
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php foreach ($product_photos as $photo) { ?>
					<?php
						$photo_path = json_decode($photo['photo_path'], true);
						$photo_size = json_decode($photo['photo_size'], true);
					?>
					<div
						class="swiper-slide"
						style="padding-bottom: <?=round($photo_size['large']['1x']['h'] / $photo_size['large']['1x']['w'] * 100, 5)?>%"
					>
						<picture>
							<?php /* <source type="image/webp" data-srcset="images/product/tmfvzira_l.webp, images/product/tmfvzira_l@2x.webp 2x"> */ ?>
							<?php if (!empty($photo_path['large']['2x'])) { ?>
							<source data-srcset="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>, <?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['2x']?> 2x">
							<?php } ?>
							<img class="lazyload" data-src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>" width="<?=$photo_size['large']['1x']['w']?>" height="<?=$photo_size['large']['1x']['h']?>" alt="<?=CHtml::encode($product['product_title'])?>">
						</picture>
					</div>
					<?php } ?>
					<?php if (!empty($product['product_video'])) { ?>
					<?php $video = json_decode($product['product_video'], true); ?>
					<div class="swiper-slide">
						<div class="product-gallery-mobile__video-wrap" style="padding-bottom: <?=round($video['params']['height'] / $video['params']['width'] * 100, 5)?>%"></div>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php if (!empty($product['product_video'])) { ?>
			<a href="#" class="product-gallery-mobile__video"><?=Lang::t('product.btn.video')?></a>
			<?php } ?>
			<?php if (count($product_photos) > 1) { ?>
			<div class="product-gallery-mobile__pages"></div>
			<?php } ?>
		</div>

		<div class="product-gallery">
			<ul class="product-gallery__nav list-unstyled">
				<?php foreach ($product_photos as $index => $photo) { ?>
				<?php
					$photo_path = json_decode($photo['photo_path'], true);
					$photo_size = json_decode($photo['photo_size'], true);
				?>
				<li>
					<a href="#" class="product-gallery__nav-item<?php if ($index == 0) { ?> product-gallery__nav-item--active<?php } ?>">
						<picture>
						<?php /* <source type="image/webp" srcset="images/product/tmfvzira_l.webp, images/product/tmfvzira_l@2x.webp 2x"> */ ?>
						<?php if (!empty($photo_path['thumb']['2x'])) { ?>
						<source srcset="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['thumb']['1x']?>, <?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['thumb']['2x']?> 2x">
						<?php } ?>
						<img src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['thumb']['1x']?>" width="<?=$photo_size['thumb']['1x']['w']?>" height="<?=$photo_size['thumb']['1x']['h']?>" alt="<?=CHtml::encode($product['product_title'])?>">
						</picture>
					</a>
				</li>
				<?php } ?>
				<?php if (!empty($product['product_video'])) { ?>
				<li>
					<a href="#" class="product-gallery__nav-item product-gallery__nav-item--video">
						<span><?=Lang::t('product.btn.video')?></span>
					</a>
				</li>
				<?php } ?>
			</ul>
			<ul class="product-gallery__photos list-unstyled">
				<?php foreach ($product_photos as $index => $photo) { ?>
				<?php
					$photo_path = json_decode($photo['photo_path'], true);
					$photo_size = json_decode($photo['photo_size'], true);
				?>
				<?php if ($index == 2 && !empty($product['product_tip'])) { ?>
				<li class="product__ticker product__ticker--large">
					<?=CHtml::encode($product['product_tip'])?>
				</li>
				<?php } ?>
				<li>
					<a 
						href="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['original']['1x']?>"
						style="padding-bottom: <?=round($photo_size['large']['1x']['h'] / $photo_size['large']['1x']['w'] * 100, 5)?>%"
						data-index="<?=$index?>"
					>
						<picture>
							<?php /* <source type="image/webp" data-srcset="images/product/tmfvzira_l.webp, images/product/tmfvzira_l@2x.webp 2x"> */ ?>
							<?php if (!empty($photo_path['large']['2x'])) { ?>
							<source data-srcset="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>, <?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['2x']?> 2x">
							<?php } ?>
							<img class="lazyload" data-src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['large']['1x']?>" width="<?=$photo_size['large']['1x']['w']?>" height="<?=$photo_size['large']['1x']['h']?>" alt="<?=CHtml::encode($product['product_title'])?>"> 
						</picture>
					</a>
				</li>
				<?php } ?>
				<?php if (count($product_photos) < 3 && !empty($product['product_tip'])) { ?>
				<li class="product__ticker product__ticker--large">
					<?=CHtml::encode($product['product_tip'])?>
				</li>
				<?php } ?>
				<?php if (!empty($product['product_video'])) { ?>
				<?php
					$video = json_decode($product['product_video'], true);
					$video_url = $assetsUrl . '/product/' . $product['product_id'] . '/' . $video['web_file'];
				?>
				<li>
					<div id="product-video" class="product-video" data-src="<?=$video_url?>" style="padding-bottom: <?=round($video['params']['height'] / $video['params']['width'] * 100, 5)?>%"></div>
				</li>
				<?php } ?>
			</ul>

			<div class="product-gallery-popup product-gallery-popup--hidden" style="display:none">
				<button type="button" class="product-gallery-popup__close"></button>
				<button type="button" class="product-gallery-popup__arrow product-gallery-popup__arrow--prev"></button>
				<button type="button" class="product-gallery-popup__arrow product-gallery-popup__arrow--next"></button>
				
				<div class="product-gallery-popup__slides swiper-container">
					<div class="swiper-wrapper">
						<?php foreach ($product_photos as $index => $photo) { ?>
						<?php
                            $photo_path = json_decode($photo['photo_path'], true);
                            $photo_size = json_decode($photo['photo_size'], true);
                        ?>
						<div class="swiper-slide">
							<picture>
								<?php /* <source type="image/webp" srcset="images/product/tmfvzira_l.webp, images/product/tmfvzira_l@2x.webp 2x"> */ ?>
								<?php /* <source srcset="images/product/tmfvzira_l.jpeg, images/product/tmfvzira_l@2x.jpeg 2x"> */ ?>
								<img src="<?=$assetsUrl?>/product/<?=$product['product_id']?>/<?=$photo_path['original']['1x']?>" alt="<?=CHtml::encode($product['product_title'])?>"> 
							</picture>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<!-- /.product__gallery -->

	<div class="product__content">
		<div class="product__main wrap">
			<?php if (!empty($product_badges)) { ?>
			<div class="product__badges">
				<?php foreach ($product_badges as $badge) { ?>
				<?php 
					if (empty($badge['badge_logo'])) {
						continue;
					}

					$badge_logo = json_decode($badge['badge_logo'], true);
					$badge_icon = $assetsUrl . '/badge/' . $badge_logo['file'];
				?>
				<?php if ($badge['badge_icon_only']) { ?>
				<div class="product__badge product__badge--icon">
					<img src="<?=$badge_icon?>" alt="<?=CHtml::encode($badge['badge_name'])?>" width="<?=$badge_logo['w']?>" height="<?=$badge_logo['h']?>">
					<div class="badge badge--product">
						<div class="badge__head">
							<span class="badge__icon"><img src="<?=$badge_icon?>" alt="<?=CHtml::encode($badge['badge_name'])?>" width="<?=$badge_logo['w']?>" height="<?=$badge_logo['h']?>"></span>
							<div class="badge__title"><?=CHtml::encode($badge['badge_name'])?></div>
						</div>
						<?php if (!empty($badge['badge_description'])) { ?>
						<div class="badge__text">
							<?=$badge['badge_description']?>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } else { ?>
				<div class="product__badge">
					<?=CHtml::encode($badge['badge_name'])?> <img src="<?=$badge_icon?>" alt="<?=CHtml::encode($badge['badge_name'])?>" width="<?=$badge_logo['w']?>" height="<?=$badge_logo['h']?>">
					<?php if (!empty($badge['badge_description'])) { ?>
					<div class="badge badge--product">
						<div class="badge__head">
							<span class="badge__icon"><img src="<?=$badge_icon?>" alt="<?=CHtml::encode($badge['badge_name'])?>" width="<?=$badge_logo['w']?>" height="<?=$badge_logo['h']?>"></span>
							<div class="badge__title"><?=CHtml::encode($badge['badge_name'])?></div>
						</div>
						<div class="badge__text">
							<?=$badge['badge_description']?>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
				<?php } ?>
			</div>
			<?php } ?>
		
			<div class="product__head">
				<h1 class="product__title"><?=CHtml::encode($product['product_title'])?></h1>
				<div class="product__price" data-price="<?=round($product['product_price'])?>" data-price-regular="<?=round($product['product_price_old'])?>">
					<?=round($product_price)?> <?=Lang::t('layout.tip.uah')?>
					<?php if (!empty($discount_price) && $discount_price > $product_price) { ?>
					<br><small><s><?=round($discount_price)?></s> <?=Lang::t('layout.tip.uah')?></small>
					<?php } ?>
				</div>
			</div>
			<?php if (!empty($product['product_description'])) { ?>
			<?php
				$product_description = str_replace(["\r", "\n"], '', $product['product_description']);
				$product_description = str_replace([
					"<br>",
					"</p>",
					"</ul>",
					"</ol>",
					"</h2>",
					"</h3>",
					"</h4>",
				], [
					"<br>\n",
					"</p>\n\n",
					"</ul>\n\n",
					"</ol>\n\n",
					"</h2>\n\n",
					"</h3>\n\n",
					"</h4>\n\n",
				], $product_description);
				$product_description = strip_tags($product_description);
			?>
			<?php if (mb_strlen($product_description, 'utf-8') > 110) { ?>
			<div class="product__intro">
				<?php 
					$product_intro = mb_substr($product_description, 0, 110, 'utf-8');
					$product_intro = preg_replace('#(\s+)[^\s+]$#ui', '', trim($product_intro));
				?>
				<?=str_replace("\n", "<br>", $product_intro)?>...
				<a href="#"><?=Lang::t('product.tip.descriptionFull')?></a>
			</div>
			<div class="product__description product__description--hidden">
				<?=$product['product_description']?>
				<a href="#" class="product__description-collapse"><?=Lang::t('product.tip.descriptionShort')?></a>
			</div>
			<?php } else { ?>
			<div class="product__description">
				<?=$product['product_description']?>
			</div>
			<?php } ?>
			<?php } ?>
			<div class="product__articul"><?=CHtml::encode($product['product_alias'])?></div>
			<?php if (!empty($product_colors)) { ?>
			<ul class="product__colors list-unstyled">
				<?php foreach ($product_colors as $product_color) { ?>
				<?php
					$style_color = '';
					
					if (!empty($product_color['value_color'])) {
						$style_color = ($product_color['value_color'] == '#ffffff') ? ' style="background-color: #fff; border: 1px solid #000;"' : ' style="background-color: ' . $product_color['value_color'] . ';"';
					} elseif (!empty($product_color['value_multicolor'])) {
						$style_color = ' style="background-image: linear-gradient(90deg, rgba(100,115,255,1) 0%, rgba(96,255,245,1) 33%, rgba(113,255,127,1) 66%, rgba(191,189,171,1) 100%);"';
					}
				?>
				<?php if ($product_color['product_id'] == $product['product_id']) { ?>
				<li><span class="product__color product__color--active"<?=$style_color?>></span></li>
				<?php } else { ?>
				<li><a href="<?=$this->createUrl('product', ['alias' => $product_color['product_alias']])?>" title="<?=CHtml::encode($product_color['product_title'])?>" class="product__color"<?=$style_color?>></a></li>
				<?php } ?>
				<?php } ?>
			</ul>
			<?php } ?>
			<?php if (isset($size) && !empty($size)) { ?>
                <div class="product__sizes">
                    <a href="#" id="sizes-toggle"><?=Lang::t('product.link.sizeGuide')?></a>
                </div>
			<?php } ?>
			<form action="<?=$this->createUrl('cart')?>" class="product__buy" method="post">
				<input type="hidden" name="action" value="add">
				<input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
				<?php if ($is_variants_type) { ?>
				<input type="hidden" name="cart[variant_id]" value="<?=$variant_id?>" id="pb-variant">
				<?php } ?>
				<input type="hidden" name="cart[qty]" value="1" id="pb-qty">

				<?php if ($product['product_price_type'] == 'variants') { ?>
				<?php if (!empty($product_variants['variants'])) { ?>
				<div id="product-variants" data-variants="<?=CHtml::encode(json_encode($variants))?>" data-values="<?=CHtml::encode(json_encode($variant_properties_map))?>" data-lang="<?=CHtml::encode(json_encode($variants_lang))?>"<?php if (!empty($selected_variant)) { ?> data-variant="<?=CHtml::encode(json_encode($selected_variant['json']))?>"<?php } ?>>
					<?php foreach ($product_variants['properties'] as $property_id => $property) { ?>
					<div id="variants-<?=$property_id?>" class="product__option" data-property="<?=$property_id?>">
						<div class="product__select">
							<?php
								$selected_option = !empty($property['property_selector']) ? CHtml::encode($property['property_selector']) : Lang::t('product.select.choose') . ' ' . mb_strtolower(CHtml::encode($property['property_title']), 'utf-8');
								$options = [];
							?>
							<?php foreach ($property['values'] as $value_id => $value) { ?>
							<?php
								$variant_attributes = '';

								if (!empty($selected_variant)) {
									if (!isset($active_properties[$property_id][$value_id])) {
										$variant_attributes = ' disabled';
									} elseif (in_array($value_id, $selected_variant['values'])) {
										$selected_option = CHtml::encode($value['value_title']);
										$variant_attributes = ' data-selected="1" selected';
									}
								}
							
								$options[] = '<option value="' . $value_id . '"' . $variant_attributes . '>' . CHtml::encode($value['value_title']) . '</option>';
							?>
							<?php } ?>
							<div class="product__select-value"><?=$selected_option?></div>
							<select name="variants[<?=$property_id?>]">
								<option value=""><?=!empty($property['property_selector']) ? CHtml::encode($property['property_selector']) : Lang::t('product.select.choose') . ' ' . mb_strtolower(CHtml::encode($property['property_title']), 'utf-8') ?></option>
								<?=implode("\n", $options)?>
							</select>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
				<?php } ?>

				<?php
					$is_disabled = false;
					$in_stock = 0;

                    if ($product['product_instock'] == 'out_of_stock') {
						$btn_text = Lang::t('product.tip.outOfStock');
						$is_disabled = true;
                    } elseif ($product['product_stock_type'] == 'preorder') {
						$btn_text = Lang::t('product.btn.preorder', $product['product_preorder_days']);
						$in_stock = 1;
					} else {
						$btn_text = Lang::t('product.btn.addToCart');
						$in_stock = 1;
					}

                    if ($is_variants_type && !empty($selected_variant) && $selected_variant['variant_instock'] == 'out_of_stock') {
						$is_disabled = true;
					}
				?>
				<?php if ($is_disabled) { ?>
				<button class="product__btn" data-stock="<?=$in_stock?>" data-text="<?=CHtml::encode($btn_text)?>" disabled><?=Lang::t('product.tip.outOfStock')?></button>
				<?php } else { ?>
				<button class="product__btn" data-stock="<?=$in_stock?>" data-text="<?=CHtml::encode($btn_text)?>"><?=CHtml::encode($btn_text)?></button>
				<?php } ?>
			</form>
			<?php if ($product['product_instock'] == 'in_stock') { ?>
			<?php /* <div class="product__gift">
				<a href="#"><?=Lang::t('product.btn.gift')?> <i class="icon-inline icon-gift"></i> <ins></ins></a>
			</div> */ ?>
			<?php } ?>

			<?php /* <button class="product__btn">Оформить предзаказ (<?=Yii::t('app', '{n} день|{n} дня|{n} дней|{n} дня', $product['product_online_days'])?>)</button> */ ?>

			<?php if (!empty($selected_variant)) { ?>
			<div class="product-delivery" data-delivery="<?=CHtml::encode($this->renderPartial('productDelivery', ['product' => $product, 'product_stores' => $product_stores], true))?>">
				<?=$selected_variant['delivery']?>
			</div>
			<?php } else { ?>
			<div class="product-delivery">
				<?php $this->renderPartial('productDelivery', ['product' => $product, 'product_stores' => $product_stores]); ?>
			</div>
			<?php } ?>
		</div>
		<!-- /.product__main -->
		
		<div class="product__extra wrap">
			<div class="product-info product-info--collapsed">
				<div class="product-info__title"><?=Lang::t('product.tip.return')?></div>
				<div class="product-info__content">
					<?php if (!empty($reclamation)) { ?>
					<?=is_array($reclamation) ? $reclamation['reclamation_description'] : $reclamation?>
					<?php } else { ?>
					<?=Lang::t('product.html.return')?>
					<?php } ?>
				</div>
			</div>
			<?php if (!empty($care)) { ?>
			<div class="product-info product-info--collapsed">
				<div class="product-info__title"><?=Lang::t('product.tip.care')?></div>
				<div class="product-info__content">
					<?=is_array($care) ? $care['care_description'] : $care?>
				</div>
			</div>
			<?php } ?>
			<?php if (!empty($brand['brand_description'])) { ?>
			<div class="product-info product-info--collapsed">
				<div class="product-info__title"><?=Lang::t('product.tip.brand')?></div>
				<div class="product-info__content">
					<?=$brand['brand_description']?>
				</div>
			</div>
			<?php } ?>
			<div class="product-info product-info--collapsed">
				<div class="product-info__title"><?=Lang::t('product.tip.paymentAndDelivery')?></div>
				<div class="product-info__content">
					<?=Lang::t('product.html.paymentAndDeliveryText')?>
				</div>
			</div>
		</div>
		<!-- /.product__extra -->
	</div>
</main>
<!-- /.product -->

<?php if (isset($size) && !empty($size)) { ?>
<aside id="sizes" class="sizes sizes--hidden">
	<div class="sizes__overlay"></div>
	<div class="sizes__container">
		<button type="button" class="sizes__close"></button>
		<div class="sizes__content">
			<div class="wrap">
				<div class="sizes__title"><?=Lang::t('product.tip.sizeTitle')?></div>
				<div class="sizes__description">
					<?= str_replace(['<table', '</table>'], ['<div class="table-responsive"><table', '</table></div>'], $size['size_description']); ?>
				</div>
			</div>
		</div>
	</div>
</aside>
<?php } ?>

<?php if (!empty($related_products)) { ?>
<section class="category category--related category--light-brown">
	<div class="category__head wrap">
		<div class="category__title"><?=Lang::t('product.tip.related')?></div>
	</div>
	<div class="category__products">
		<?php $this->renderPartial('productsList', array('products' => $related_products)); ?>
	</div>
</section>
<?php } ?>