<?php
	$facet_url = [
		'alias' => $category['category_alias'],
	];

	if ($sort != 'popular') {
		$facet_url['sort'] = $sort;
	}

	$reset_all_url = $this->createUrl('category', $facet_url);;

	$price_facet = $facets->getPriceFacet();

	// if ($price_facet['min'] != $price_facet['from'] || $price_facet['max'] != $price_facet['to']) {
	if ($price_facet['current_from'] !== false || $price_facet['current_to'] !== false) {
		$facet_url['filter']['price'] = [
			'from' => $price_facet['current_from'],
			'to' => $price_facet['current_to'],
		];
	}

	/*
	$length_facet = $facets->getLengthFacet();

	if ($length_facet['min'] != $length_facet['from'] || $length_facet['max'] != $length_facet['to']) {
		$facet_url['filter']['length'] = array(
			'from' => $length_facet['from'],
			'to' => $length_facet['to'],
		);
	}

	$width_facet = $facets->getWidthFacet();

	if ($width_facet['min'] != $width_facet['from'] || $width_facet['max'] != $width_facet['to']) {
		$facet_url['filter']['width'] = array(
			'from' => $width_facet['from'],
			'to' => $width_facet['to'],
		);
	} */

	$selected_facets = $facets->getSelectedFacets();
	$values = array_column($selected_facets, 'value_id');
?>
<ul class="category__reset list-unstyled">
	<li class="category__reset-btn category__reset-btn--all"><a href="<?=$reset_all_url?>"><?=Lang::t('catalog.btn.filterReset')?></a></li>
	<?php if (isset($facet_url['filter']['price'])) { ?>
	<?php 
		$current_facet_url = $facet_url;
		unset($current_facet_url['filter']['price']);

		if (!empty($values)) {
			$current_facet_url['filter']['p'] = $values;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<?php /* <li class="category__reset-btn"><a href="<?=$facet_reset_url?>"><?=CHtml::encode(Lang::t('facets.tip.filterPrice', array('{from}' => $facet_url['filter']['price']['from'], '{to}' => $facet_url['filter']['price']['to'])))?><i class="icon icon-reset"></i></a> */ ?>
	<li class="category__reset-btn"><a href="<?=$facet_reset_url?>">Ціна <?=$facet_url['filter']['price']['from']?>–<?=$facet_url['filter']['price']['to']?> грн<i class="icon-inline icon-reset"></i></a></li>
	<?php } ?>

	<?php foreach ($selected_facets as $facet) { ?>
	<?php
		$current_facet_url = $facet_url;
		$values_tmp = $values;

		$value_position = array_search($facet['value_id'], $values_tmp);
		unset($values_tmp[$value_position]);

		if (!empty($values_tmp)) {
			$current_facet_url['filter']['p'] = $values_tmp;
		}

		$facet_reset_url = $this->createUrl('category', $current_facet_url);
	?>
	<li class="category__reset-btn"><a href="<?=$facet_reset_url?>"><?=CHtml::encode($facet['value_title'])?><i class="icon-inline icon-reset"></i></a></li>
	<?php } ?>
</ul>