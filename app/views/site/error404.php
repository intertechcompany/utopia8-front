<?php
	/* @var $this SiteController */
	/* @var $error array */

	$this->pageTitle = Lang::t('seo.meta.title404');;
	$this->pageDescription = '';
	$this->pageKeywords = '';

	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="extra extra--alt">
	<div class="extra__video">
		<div class="extra__video-wrap">
			<video autoplay muted loop playsinline>
				<source type="video/mp4" src="<?=$assetsUrl?>/404.mp4"></source>
			</video>
		</div>
	</div>
	<div class="extra__content">
		<div class="extra__title"><?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('error404.title.notFound'))?></div>
		<div class="extra__text">
			<?=Lang::t('error404.tip.notFoundText')?>
		</div>
	</div>
</main>
<?php if (!empty($products)) { ?>
<section class="category category--blue">
	<div class="category__head wrap">
		<div class="category__title"><?=Lang::t('layout.tip.newestProducts')?></div>
	</div>
	<div class="category__products">
		<?php $this->renderPartial('productsList', ['products' => $products]); ?>
	</div>
</section>
<?php } ?>