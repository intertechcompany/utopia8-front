<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];

    $discount_value = 0;
?>
<main class="checkout">
	<div class="checkout__head wrap">
		<h1 class="checkout__title"><?=Lang::t('checkout.tip.checkoutTitle')?></h1>
	</div>
	<form action="<?=$this->createUrl('checkout')?>" id="checkout" class="checkout__form form wrap" method="post" data-baseurl="<?=Yii::app()->request->getBaseUrl()?>" data-has-preorder="<?=(int) $has_preorder?>" novalidate>
		<div class="checkout-column">
			<div class="checkout__group">
				<div class="checkout__group-title"><?=Lang::t('checkout.tip.stepPersonal')?></div>
				<div class="form__row">
					<div class="form__column form__column--50">
						<input type="tel" id="checkout-phone" class="form__input" name="order[phone]" value="<?=CHtml::encode($customer['phone'])?>" placeholder="<?=Lang::t('checkout.placeholder.phone')?>">
					</div>
					<div class="form__column form__column--50">
						<input type="email" id="checkout-email" class="form__input" name="order[email]" value="<?=CHtml::encode($customer['email'])?>" placeholder="<?=Lang::t('checkout.placeholder.email')?>">
					</div>
					<div class="form__column form__column--50">
						<input type="text" id="checkout-name" class="form__input" name="order[first_name]" value="<?=CHtml::encode($customer['first_name'])?>" placeholder="<?=Lang::t('checkout.placeholder.name')?>">
					</div>
					<div class="form__column form__column--50">
						<input type="text" id="checkout-surname" class="form__input" name="order[last_name]" value="<?=CHtml::encode($customer['last_name'])?>" placeholder="<?=Lang::t('checkout.placeholder.lastname')?>">
					</div>
				</div>
			</div>
			<div id="checkout-delivery" class="checkout__group">
				<div class="checkout__group-title"><?=Lang::t('checkout.tip.stepDelivery')?></div>
				<?php if (Yii::app()->language == 'en') { ?>
				<div class="form__row">
					<div class="form__column form__label"><?=Lang::t('checkout.tip.intlDelivery')?></div>
				</div>
				<?php } ?>
				<div class="form__row">
					<div class="form__column form__column--50">
						<label for="checkout-np-city" class="form__label"><?=Lang::t('checkout.label.city')?></label>
						<select id="checkout-np-city" name="order[np_city]" class="form__input">
                            <option value=""><?=Lang::t('checkout.label.city')?></option>
                            <?php if (!empty($np_cities)) { ?>
                            <?php $field = (Yii::app()->language == 'ru') ? 'city_name_ru' : 'city_name'; ?>
                            <?php foreach ($np_cities as $np_city) { ?>
                            <option value="<?=CHtml::encode($np_city[$field])?>"<?php if ($customer['np_city'] == $np_city['city_name'] || $customer['np_city'] == $np_city['city_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_city[$field])?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
					</div>
				</div>
				<div class="form__row form__row--delivery form__row--radio<?php if (empty($customer['np_city'])) { ?> form__row--hidden<?php } ?>">
					<div class="form__column"<?php if ($has_preorder || ($customer['np_city'] != 'Одеса' && $customer['np_city'] != 'Одесса' && $customer['np_city'] != 'Харків' && $customer['np_city'] != 'Харьков')) { ?> style="display:none"<?php } ?>>
						<label for="delivery-1" class="form__radio">
							<input type="radio" id="delivery-1" name="order[delivery]" value="1" data-type="pickup"<?php if ($has_preorder || ($customer['np_city'] != 'Одеса' && $customer['np_city'] != 'Одесса' && $customer['np_city'] != 'Харків' && $customer['np_city'] != 'Харьков')) { ?> disabled<?php } elseif ($delivery == 1) { ?> checked<?php } ?>>
							<span id="pickup-kh"<?php if ($customer['np_city'] != 'Харків' && $customer['np_city'] != 'Харьков') { ?> style="display:none"<?php } ?>>
								<?=Lang::t('checkout.label.pickupKh')?>
							</span>
							<span id="pickup-od"<?php if ($customer['np_city'] != 'Одеса' && $customer['np_city'] != 'Одесса') { ?> style="display:none"<?php } ?>>
								<?=Lang::t('checkout.label.pickupOd')?>
							</span>
						</label>
					</div>
					<div class="form__column">
						<label for="delivery-2" class="form__radio">
							<input type="radio" id="delivery-2" name="order[delivery]" value="2" data-type="np_department"<?php if ($delivery == 2) { ?> checked<?php } ?>>
							<span><?=Lang::t('checkout.label.npOffice')?></span>
						</label>
					</div>
					<div class="form__column">
						<label for="delivery-3" class="form__radio">
							<input type="radio" id="delivery-3" name="order[delivery]" value="3" data-type="np_address"<?php if ($delivery == 3) { ?> checked<?php } ?>>
							<span><?=Lang::t('checkout.label.npCourier')?></span>
						</label>
					</div>
				</div>
				<div class="form__row form__row--np_department<?php if ($delivery != 2) { ?> form__row--hidden<?php } ?>">
					<div class="form__column form__column--50">
						<select id="checkout-np-department" name="order[np_department]" class="form__input"<?php if (empty($np_departments) || $delivery != 2) { ?> disabled<?php } ?>>
                            <option value="">Выберите отделение</option>
                            <?php if (!empty($np_departments)) { ?>
                            <?php $field = (Yii::app()->language == 'ru') ? 'department_name_ru' : 'department_name'; ?>
                            <?php foreach ($np_departments as $np_department) { ?>
                            <option value="<?=CHtml::encode($np_department[$field])?>"<?php if ($customer['np_department'] == $np_department['department_name'] || $customer['np_department'] == $np_department['department_name_ru']) { ?> selected<?php } ?>><?=CHtml::encode($np_department[$field])?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
					</div>
				</div>
				<div class="form__row form__row--np_address<?php if ($delivery != 3) { ?> form__row--hidden<?php } ?>">
					<div class="form__column form__column--50">
						<input type="text" id="checkout-address" name="order[address]" class="form__input" value="<?=CHtml::encode($customer['address'])?>" placeholder="Адрес">
					</div>
				</div>
				<?php /* <div class="form__row" data-type="np_address">
					<div class="form__column form__column--50">
						<input type="text" id="checkout-np-building" class="form__input" placeholder="Дом">
					</div>
					<div class="form__column form__column--50">
						<input type="text" id="checkout-np-apartment" class="form__input" placeholder="Квартира/офис">
					</div>
				</div> */ ?>
			</div>
			<div class="checkout__group">
				<div class="checkout__group-title"><?=Lang::t('checkout.tip.stepPayment')?></div>
				<div class="form__row form__row--payment">
					<div class="form__column">
						<label for="payment-1" class="form__radio">
							<input type="radio" id="payment-1" name="order[payment]" value="1"<?php if ($payment == 1) { ?> checked<?php } ?>>
							<span><?=Lang::t('checkout.label.privat')?></span>
						</label>
					</div>
					<div class="form__column">
						<label for="payment-2" class="form__radio">
							<input type="radio" id="payment-2" name="order[payment]" value="2"<?php if ($payment == 2) { ?> checked<?php } ?>>
							<span><?=Lang::t('checkout.label.card')?></span>
						</label>
					</div>
					<div class="form__column"<?php if ($has_preorder || ($customer['np_city'] != 'Одеса' && $customer['np_city'] != 'Одесса' && $customer['np_city'] != 'Харків' && $customer['np_city'] != 'Харьков')) { ?> style="display:none"<?php } ?>>
						<label for="payment-3" class="form__radio">
							<input type="radio" id="payment-3" name="order[payment]" value="3"<?php if ($has_preorder || ($customer['np_city'] != 'Одеса' && $customer['np_city'] != 'Одесса' && $customer['np_city'] != 'Харків' && $customer['np_city'] != 'Харьков')) { ?> disabled<?php } elseif ($payment == 3) { ?> checked<?php } ?>>
							<span><?=Lang::t('checkout.label.cash')?></span>
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="checkout-column">
			<div id="cart" class="checkout__cart" data-action="<?=$this->createUrl('cart')?>">
				<div class="cart-details__title"><?=Lang::t('checkout.tip.cart')?> (<span><?=$total?></span>)</div>
				<div class="cart-products__list">
					<?php $this->renderPartial('cartList', [
                        'cart' => $cart,
                        'box_banner' => $box_banner,
                        'postcard_banner' => $postcard_banner,
                    ]); ?>
				</div>
			</div>
			<div class="checkout__summary">
				<div class="checkout__total checkout__total--order"><span><?=Lang::t('checkout.tip.order')?></span> <span><?=$price?> <?=Lang::t('layout.tip.uah')?></span></div>
				<?php /* <div class="checkout__total checkout__total--delivery"><span>Доставка</span> <span>120 грн</span></div>
				<div class="checkout__total checkout__total--promo"><span>Промокод -15%</span> <span>-350 грн</span></div> */ ?>
				<div class="checkout__total checkout__total--overall"><span><?=Lang::t('checkout.tip.total')?></span> <span><?=$price?> <?=Lang::t('layout.tip.uah')?></span></div>

				<div class="form__row form__row--btn">
					<div class="form__column">
						<button class="form__btn"<?php if ($has_unavailable) { ?> disabled<?php } ?>><?=Lang::t('checkout.btn.submit')?></button>
					</div>
				</div>

				<div class="checkout__additional">
					<div class="form__row">
						<div class="form__column">
							<div class="form__select">
								<div class="form__select-value"><?=Lang::t('checkout.label.communicate')?></div>
								<select id="checkout-communication" name="order[zip]">
									<option value=""><?=Lang::t('checkout.label.communicate')?></option>
									<option value="email">Email</option>
									<option value="telegram">Telegram</option>
									<option value="sms">SMS</option>
									<option value="call"><?=Lang::t('checkout.select.call')?></option>
									<option value="viber">Viber</option>
								</select>
							</div>
						</div>
						<div class="form__column">
							<textarea id="checkout-comment" name="order[comment]" placeholder="<?=Lang::t('checkout.placeholder.comment')?>" rows="5" class="form__textarea"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</main>
<!-- /.checkout -->
