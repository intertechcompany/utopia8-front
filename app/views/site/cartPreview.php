<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<?php if (empty($cart)) { ?>
<div class="cart-popup__empty">Ваша корзина пуста!</div>
<div class="cart-popup__btns">
	<a href="#" class="cart-popup__btn cart-popup__btn--continue">Продолжить покупки</a>
</div>
<?php } else { ?>
<?php
	// get first item
	foreach ($cart as $cart_index => $cart_item) {
		$product = $cart_item['data'];
		$variant = $cart_item['variant'];
		$options = $cart_item['options'];
		
		$product_id = $product['product_id'];
		$product_title = CHtml::encode($product['product_title']);

		if (!empty($variant['values'])) {
			$variant_values = array();
	
			foreach ($variant['values'] as $value) {
				$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
			}
		}

		break;
	}
?>
<div class="cart-popup__title">Товар добавлен в корзину</div>
<div class="cart-popup__message"><b><?=CHtml::encode($product_title)?><?php if (!empty($variant_values)) { ?> (<?=implode(', ', $variant_values)?>)<?php } ?></b></div>
<div class="cart-popup__btns">
	<a href="#" class="cart-popup__btn cart-popup__btn--continue">Продолжить покупки</a>
	<a href="<?=$this->createUrl('site/cart')?>" class="cart-popup__btn">Оформить заказ</a>
</div>
<?php /*
<div id="cart" class="cart">
	<div class="cart-head clearfix">
		<h1><?=Lang::t('cart.title.shoppingCart')?></h1>

		<ul class="list-inline">
			<li class="ch-price"><?=Lang::t('cart.th.price')?></li><!--
			--><li class="ch-qty"><?=Lang::t('cart.th.quantity')?></li><!--
			--><li class="ch-amount"><?=Lang::t('cart.th.amount')?></li>
		</ul>
	</div>

	<form class="cart-items" action="<?=$this->createUrl('cart')?>" method="post">
		<?php $this->renderPartial('cartList', array('cart' => $cart)); ?>
	</form>

	<div class="cart-footer">
		<div class="cart-next">
			<div class="cart-total"><?=Lang::t('cart.tip.total')?> <span><?=number_format($price, 2, '.', '')?>€</span></div>
			<div class="cart-checkout"><a href="<?=$this->createUrl('checkout')?>" class="btn"><?=Lang::t('cart.btn.checkout')?></a></div>
		</div>
		<div class="cart-continue">
			<button type="button" class="btn btn-gray"><?=Lang::t('cart.btn.continue')?></button>
		</div>
	</div>
</div>*/ ?>
<?php } ?>