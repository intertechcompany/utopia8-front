<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="knowledge-base">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>

		<h1 class="knowledge-base__title"><?=$this->pageTitle?></h1>

		<div class="knowledge-base__sections">
			<?php foreach ($categories as $category) { ?>
			<div class="info-block info-block--knowledge-base-category" data-title="<?=CHtml::encode($category['category_name'])?>">
				<div class="info-block__img">
					<?php if (!empty($category['category_photo'])) { ?>
					<?php
						$category_image = '';
						$category_image_2x = '';
					
						if (!empty($category['category_photo'])) {
							$category_image = json_decode($category['category_photo'], true);
							$category_image = $assetsUrl . '/base_category/' . $category['category_id'] . '/' . $category_image['1x']['path'];
							
							if (!empty($category_image['2x'])) {
								$category_image_2x = $assetsUrl . '/base_category/' . $category['category_id'] . '/' . $category_image['2x']['path'];
							}
						}
					?>
					<picture>
						<!-- <source type="image/webp" srcset="images/kb_1.webp, images/kb_1@2x.webp 2x"> -->
						<?php if (!empty($category_image_2x)) { ?>
						<source srcset="<?=$category_image?>, <?=$category_image_2x?> 2x">
						<?php } ?>
						<img src="<?=$category_image?>" alt="">
					</picture>
					<?php } ?>
				</div>
				<div class="info-block__content">
					<div class="info-block__title"><?=CHtml::encode($category['category_name'])?></div>
					<ul class="info-block__list list-unstyled">
						<?php foreach ($category['bases'] as $base) { ?>
						<li><a href="<?=$this->createUrl('basearticle', ['category_alias' => $category['category_alias'], 'alias' => $base['base_alias']])?>"><?=CHtml::encode($base['base_title'])?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>

		<section class="questions">
            <div class="questions__title"><?=Lang::t('layout.tip.questions')?></div>
            <div class="questions__tip">
                <ul class="questions__contacts list-unstyled">
                    <li><span><?=Lang::t('about.tip.call')?>:</span> <a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
                    <li><span><?=Lang::t('about.tip.write')?>:</span> <a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
                    <li><?=Lang::t('layout.tip.glad')?></li>
                </ul>
            </div>
        </section>
	</div>
</main>
