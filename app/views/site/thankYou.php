<?php 
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();

	$videos = [
		'cat_u8.mp4',
		'cat_pink_u8.mp4',
		'dog_u8.mp4',
		'keanu_u8.mp4',
		'snoop_u8.mp4',
		'snowwhite_u8.mp4',
	];
?>
<main class="extra">
	<div class="extra__video">
		<div class="extra__video-wrap">
			<video autoplay muted loop playsinline>
				<source type="video/mp4" src="<?=$assetsUrl?>/<?=$videos[mt_rand(0, count($videos) - 1)]?>"></source>
			</video>
		</div>
	</div>
	<div class="extra__content">
		<div class="extra__title"><?=Lang::t('thankYou.tip.title')?></div>
		<div class="extra__text">
			<?php
				if ($order['zip'] == 'email') {
					$communicate = 'Email';
				} elseif ($order['zip'] == 'telegram') {
					$communicate = 'Telegram';
				} elseif ($order['zip'] == 'sms') {
					$communicate = 'SMS';
				} elseif ($order['zip'] == 'call') {
					$communicate = Lang::t('checkout.select.call');
				} elseif ($order['zip'] == 'viber') {
					$communicate = 'Viber';
				}
			?>
			<?php if (empty($communicate)) { ?>
			<?=Lang::t('thankYou.html.textAlt', ['{order_id}' => $order_id])?>
			<?php } else { ?>
			<?=Lang::t('thankYou.html.text', ['{order_id}' => $order_id, '{communicate}' => $communicate])?>
			<?php } ?>
		</div>
	</div>
</main>

<?php /*
<main class="thank-you wrap">
	<div class="thank-you__container">
		<?php if (isset($order_status)) { ?>
			<?php if ($order_status == 'paid') { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentSuccess')?></h1>
			<?php } elseif ($order_status == 'processing') { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentPending')?></h1>
			<?php } else { ?>
			<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.paymentFailure')?></h1>
			<?php } ?>
		<?php } else { ?>
		<h1 class="thank-you__title"><?=Lang::t('thankYou.tip.newOrder')?></h1>
		<?php } ?>
		
		<div class="thank-you__tip">
			<?php if (isset($order_status) && $order_status == 'payment_error') { ?>
			<b><?=Lang::t('thankYou.tip.paymentSystemStatus')?> <?=CHtml::encode($payment_message)?></b><br><br>
			<?php } ?>
			<?=Lang::t('thankYou.tip.orderNumber')?> <b><?=$order_id?></b><br><br>
			<?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('thankYou.tip.manager', ['{phone}' => preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone'])), '{phone_formatted}' => CHtml::encode(Yii::app()->params->settings['phone'])]))?>
		</div>
		<div class="thank-you__back"><a href="<?=$this->createUrl('site/index')?>" class="btn"><?=Lang::t('thankYou.btn.backToHome')?></a></div>
	</div>

	<form id="subscribe" class="subscribe" action="<?=$this->createUrl('ajax/subscribe')?>" method="post" novalidate>
		<div class="subscribe__title"><?=Lang::t('home.tip.subscribeTitle')?></div>
		<div class="subscribe__content">
			<ul class="subscribe__special list-unstyled">
				<li><?=Lang::t('home.tip.followInsta')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">@fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.followFacebook')?> <a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">fresh.black.okay</a></li>
				<li><?=Lang::t('home.tip.subscribe')?></li>
			</ul>
			<div class="subscribe__group">
				<input type="email" class="subscribe__input" name="subscribe[email]" placeholder="Email" required>
				<button class="subscribe__btn">&gt;</button>
			</div>
		</div>
	</form>
	<!-- /.subscribe -->
</main>
*/ ?>