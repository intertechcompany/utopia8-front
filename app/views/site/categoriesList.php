<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$action_id = $this->getAction()->getId(); 
?>
<?php if (!empty($categories)) { ?>
<?php foreach ($categories as $category) { ?><div class="hc-block<?php if (empty($category['sub']) && $action_id != 'index') { ?> hc-short<?php } ?>">
	<div class="hc-inner">
		<?php
			$category_url = $this->createUrl('category', array('alias' => $category['category_alias']));

			if (!empty($category['category_photo'])) {
				$category_image = json_decode($category['category_photo'], true);
				$category_image = $assetsUrl . '/category/' . $category['category_id'] . '/' . $category_image['path'];
			} else {
				$category_image = '';
			}
		?>
		<div class="hc-head"<?php if (!empty($category_image)) { ?> style="background-image: url(<?=$category_image?>);"<?php } ?>>
			<a class="hc-title" href="<?=$category_url?>"><span><i class="icon-inline icon-hc-arrow"></i><?=CHtml::encode($category['category_name'])?></span></a>
		</div>
		<?php if (!empty($category['sub'])) { $i = 0; ?>
		<ul class="hc-subcategories list-unstyled">
			<?php foreach ($category['sub'] as $category) { ?>
			<?php if ($i == 4) { ?><li class="hc-show-all"><a class="collapsed" href="#"><span class="hc-more"><?=Lang::t('category.link.showAll')?></span><span class="hc-less"><?=Lang::t('category.link.showLess')?></span><i class="icon-inline icon-hc-arrow-down"></i></a></li><?php } ?>
			<li<?php if ($i >= 4) { ?> class="hidden"<?php } ?>><a href="<?=$this->createUrl('category', array('alias' => $category['category_alias']))?>"><?=CHtml::encode($category['category_name'])?></a></li>
			<?php $i++; } ?>
		</ul>
		<?php } ?>
	</div>
</div><?php } ?>
<?php } ?>