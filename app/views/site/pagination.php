<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<?php if ($pages->pageCount > 1) { ?>
<?php if ($pages->currentPage + 1 < $pages->pageCount) { ?>
<a href="<?=$this->createUrl($pages->route, array_merge($pages->params, ['page' => $pages->currentPage + 2]))?>" class="category__more">
    <i></i>
    <span><?=Lang::t('catalog.btn.showMore')?></span>
</a>
<?php } ?>
<?php
    $this->widget('LinkPager', array(
        'pages' => $pages,
        'maxButtonCount' => 5,
        'prevPageLabel' => '<span>' . Lang::t('catalog.link.prevPage') . '</span><i>←</i>',
        'nextPageLabel' => '<i>→</i><span>' . Lang::t('catalog.link.nextPage') . '</span>',
        'htmlOptions' => array(
            'class' => 'pagination list-unstyled',
        ),
    ));
?>
<?php } ?>