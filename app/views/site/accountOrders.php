<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="wrap">
    <div class="content-divider"></div>
    <div class="breadcrumbs">
        <a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a>
        <span class="b-sep">|</span>
        <a href="<?=$this->createUrl('site/account')?>"><?=Lang::t('layout.link.account')?></a>
        <span class="b-sep">|</span>
        <?=CHtml::encode($this->pageTitle)?>
    </div>
    <?php $this->renderPartial('accountMenu'); ?>
    <div class="account-wrap clearfix">
        <div class="account-orders">
            <h1 class="account-title"><?=Lang::t('account.title.ordersHistory')?></h1>

            <div class="account-orders-list">
                <?php if (!empty($orders)) { ?>
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?=Lang::t('account.th.date')?></th>
                            <th><?=Lang::t('account.th.amount')?></th>
                            <th><?=Lang::t('account.th.status')?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orders as $order) { ?>
                        <?php
                            $date = new DateTime($order['created'], new DateTimeZone(Yii::app()->timeZone));

                            $status_class = '';

                            switch ($order['status']) {
                                case 'processing':
                                    $status_class = 'status-warning';
                                    $status = Lang::t('account.tip.orderStatusInProcessing');
                                    break;
                                case 'paid':
                                    $status_class = 'status-success';
                                    $status = Lang::t('account.tip.orderStatusPaid');
                                    break;
                                case 'completed':
                                    $status_class = 'status-success';
                                    $status = Lang::t('account.tip.orderStatusCompleted');
                                    break;
                                case 'cancelled':
                                    $status_class = 'status-cancel';
                                    $status = Lang::t('account.tip.orderStatusCancelled');
                                    break;
                                case 'payment_error':
                                    $status_class = 'status-error';
                                    $status = Lang::t('account.tip.orderStatusPaymentError');
                                    break;
                                default:
                                    $status = Lang::t('account.tip.orderStatusNew');
                            }
                        ?>
                        <tr>
                            <td><?=$order['order_id']?></td>
                            <td><?=$date->format('d.m.Y')?></td>
                            <td><?=$order['price']?> грн.</td>
                            <td<?php if (!empty($status_class)) { ?> class="<?=$status_class?>"<?php } ?>><?=$status?></td>
                            <td>
                                <a href="<?=$this->createUrl('site/order', array('order_id' => $order['order_id']))?>"><?=Lang::t('account.link.viewOrder')?></a>&nbsp;
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php } else { ?>
                <p><?=Lang::t('account.tip.noOrders')?></p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>