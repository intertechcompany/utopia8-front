<?php
	/* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $categories = Yii::app()->params->categories;
?>
<?php if (empty($box_banner['products']) && empty($postcard_banner['products'])) { ?>
<div class="product__gift" style="margin-bottom: 0">
    <?php /* <a href="#"><?=Lang::t('cartSide.btn.gift')?> <i class="icon-inline icon-gift"></i><ins></ins></a> */ ?>
</div>
<?php } else { ?>
<div class="product__gift">
    <a href="#" class="cart__toggle-gifts"><?=Lang::t('cart.btn.gift')?> <ins></ins></a>
</div>

<?php $gift_products = []; ?>
<div class="gifts gifts--hidden">
    <div class="gifts__tabs">
        <?php if (!empty($box_banner['products'])) { $gift_products[] = $box_banner; ?>
        <a href="#" class="gifts__tab gifts__tab--active"><?=Lang::t('cart.btn.giftBoxes')?></a>
        <?php } ?> 
        <?php if (!empty($box_banner['products']) && !empty($postcard_banner['products'])) { ?> / <?php } ?>
        <?php if (!empty($postcard_banner['products'])) { $gift_products[] = $postcard_banner; ?>
        <a href="#" class="gifts__tab"><?=Lang::t('cart.btn.giftPostcards')?></a>
        <?php } ?>
    </div>
    <div class="gifts__panes">
        <?php foreach ($gift_products as $index => $banner) { ?>
        <div class="gifts__pane<?php if ($index == 0) { ?> gifts__pane--active<?php } ?>">
            <div class="gifts__slider swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($banner['products'] as $product) { ?>
                    <div class="swiper-slide">
                        <div class="gift">
                            <?php
                                $product_url = Yii::app()->createUrl('site/product', ['alias' => $product['product_alias']]);

                                if (!empty($product['product_photo'])) {
                                    $product_image = json_decode($product['product_photo'], true);
                                    $product_image_size_2x = $product_image['size']['catalog']['2x'];
                                    $product_image_2x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['2x'];
                                    $product_image_size_1x = $product_image['size']['catalog']['1x'];
                                    $product_image_1x = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog']['1x'];
                                } else {
                                    $product_image_2x = '';
                                    $product_image_1x = '';
                                }

                                $product_price = (float) $product['product_price'];
                                $product_price_old = (float) $product['product_price_old'];
                            ?>
                            <div class="gift__image">
                                <a href="<?=$product_url?>">
                                    <?php if (!empty($product_image_1x)) { ?>
                                    <picture>
                                        <?php /* <source type="image/webp" srcset="images/gift_1.webp, images/gift_1@2x.webp 2x"> */ ?>
                                        <source srcset="<?=$product_image_1x?>, <?=$product_image_2x?> 2x">
                                        <img src="<?=$product_image_1x?>" alt="<?=CHtml::encode($product['product_title'])?>">
                                    </picture>
                                    <?php } ?>
                                </a>
                            </div>
                            <div class="gift__title">
                                <div class="gift__title-wrap"><a href="<?=$product_url?>"><?=CHtml::encode($product['product_title'])?></a></div>
                                <?=$product_price == 0 ? Lang::t('layout.tip.free') : round($product_price) . ' ' . Lang::t('layout.tip.uah')?>
                            </div>
                            <?php if ($product['product_instock'] == 'in_stock') { ?>
                            <form action="<?=Yii::app()->createUrl('cart')?>" class="gift__buy" method="post">
                                <input type="hidden" name="action" value="add">
                                <input type="hidden" name="cart[product_id]" value="<?=$product['product_id']?>">
                                <input type="hidden" name="cart[qty]" value="1">
                                <button class="gift__btn"><?=Lang::t('cart.btn.addGift')?></button>
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if (!empty($banner['category_id']) && isset($categories[$banner['category_id']])) { ?>
                    <div class="swiper-slide" style="width: 130px;">
                        <div class="gift">
                            <a href="<?=Yii::app()->createUrl('site/category', ['alias' => $categories[$banner['category_id']]['category_alias']])?>" class="gift__show-all"><span><?=Lang::t('cart.btn.showAllGifts')?></span></a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php if (count($banner['products']) > 2) { ?>
            <button type="button" class="gifts__arrow gifts__arrow--left"></button>
            <button type="button" class="gifts__arrow gifts__arrow--right"></button>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>
<!-- ./gifts -->
<?php } ?>