<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<div class="breadcrumbs">
	<a href="<?=Yii::app()->homeUrl?>"><?=Lang::t('layout.link.breadcrumbsHome')?></a> 
	<span class="b-sep">|</span> 
	<?=Category::renderBreadrumbs($this, $categories, $categories[$category['category_id']]['parents'], 0)?>
	<?=CHtml::encode($collection['collection_title'])?>
</div>

<div id="collection" class="collection clearfix">
	<div class="collection-content">
		<h1><?=CHtml::encode($collection['collection_title'])?></h1>
		
		<?php if (!empty($collection['collection_country']) || !empty($products)) { ?>
		<ul class="collection-data list-unstyled">
			<?php if (!empty($collection['collection_country'])) { ?><li><?=Lang::t('collection.tip.manufacturer')?> <span><?=CHtml::encode($collection['collection_country'])?></span></li><?php } ?>
			<?php
				// get min price
				$min_price = null;

				foreach ($products as $product) {
					$product_price = (float) $product['product_price'];
					$product_pack_size = (float) $product['product_pack_size'];

					if ($product['product_price_type'] == 'package') {
						$product_pack_size = empty($product_pack_size) ? 1 : $product_pack_size;
						$product_price = round($product_price / $product_pack_size, 2);

						if ($min_price === null || $product_price < $min_price) {
							$min_price = $product_price;
							$product_price_suffix = Lang::t('global.tip.perm2');
						}
					} elseif ($product['product_price_type'] == 'per_meter') {
						if ($min_price === null || $product_price < $min_price) {
							$min_price = $product_price;
							$product_price_suffix = Lang::t('global.tip.perm');
						}
					} else {
						if ($min_price === null || $product_price < $min_price) {
							$min_price = $product_price;
							$product_price_suffix = Lang::t('global.tip.perPiece');
						}
					}
				}

				$discount_price = Product::getDiscountPrice($min_price, $category['category_id'], $category['brand_id']);

				if ($discount_price['discount']) {
					$min_price = $discount_price['price'];
				}
			?>
			<li><?=Lang::t('collection.tip.price')?> <span><?=Lang::t('collection.tip.priceFrom')?><?=number_format($min_price, 2, '.', '')?> <?=$product_price_suffix?></span></li>
		</ul>
		<?php } ?>

		<?php if (!empty($collection['collection_description'])) { ?>
		<?php if (empty($collection['collection_description_full'])) { ?>
		<div class="collection-description">
			<a href="#" class="btn collection-more"><i class="icon-inline icon-arrow-down"></i></a>
		<?php } else { ?>
		<div class="collection-description full">
		<?php } ?>
			<?=$collection['collection_description']?>
		</div>
		<?php } ?>
	</div>

	<?php if (!empty($collection_photos)) { ?>
	<div class="collection-gallery">
		<div class="fotorama" data-auto="false">
			<?php foreach ($collection_photos as $photo) { ?>
			<?php
				$photo_path = json_decode($photo['photo_path'], true);
				$photo_size = json_decode($photo['photo_size'], true);
			?>
			<a href="<?=$assetsUrl?>/collection/<?=$collection['collection_id']?>/<?=$photo_path['large']?>">
				<img src="<?=$assetsUrl?>/collection/<?=$collection['collection_id']?>/<?=$photo_path['thumb']?>" width="70" height="70">
			</a>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>

<?php if (!empty($products)) { ?>
<div id="catalog-list" class="catalog-list search-results">
	<?php $this->renderPartial('productsList', array('products' => $products, 'is_catalog' => true)); ?>
</div>
<!-- /.catalog-list -->
<?php } ?>