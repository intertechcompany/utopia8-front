<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<?php foreach ($cart as $cart_index => $cart_item) { ?>
<?php
	$product = $cart_item['data'];
	$variant = $cart_item['variant'];
	$options = $cart_item['options'];
	
	$product_id = $product['product_id'];
	$product_url_params = array('alias' => $product['product_alias']);

	if (!empty($variant)) {
		$variant_id = $variant['variant_id'];
		$product_url_params['vid'] = $variant['variant_id'];
	} else {
		$variant_id = 0;
	}
	
	if (!empty($options)) {
		$option_id = json_encode(array_keys($options));
		$product_url_params['options'] = array();	
	
		foreach ($options as $option) {
			$product_url_params['options'][] = $option['value_id'];
		}
	} else {
		$option_id = '';
	}
	
	$product_url = Yii::app()->createUrl('site/product', $product_url_params);

	if (!empty($variant['photo_path'])) {
		$product_image = json_decode($variant['photo_path'], true);
		$product_image_size = json_decode($variant['photo_size'], true);
		$product_image_size = $product_image_size['catalog'];
		$product_image_1x = $assetsUrl . '/product/' . $product_id . '/variant/' . $variant_id . '/' . $product_image['catalog'];
	} elseif (!empty($product['product_photo'])) {
		$product_image = json_decode($product['product_photo'], true);
		$product_image_size = $product_image['size']['catalog'];
		$product_image_1x = $assetsUrl . '/product/' . $product_id . '/' . $product_image['path']['catalog']['1x'];
		$product_image_2x = $assetsUrl . '/product/' . $product_id . '/' . $product_image['path']['catalog']['2x'];
	} else {
		$product_image_1x = '';
	}

	$product_sku = !empty($variant) ? $variant['variant_sku'] : $product['product_sku'];
	$price_type = !empty($variant) ? $variant['variant_price_type'] : $product['product_price_type'];
	$product_title = CHtml::encode($product['product_title']);

	$regular_price_old = !empty($variant) ? (int) $variant['variant_price_old'] : (int) $product['product_price_old'];
	$regular_price = !empty($variant) ? $variant['variant_price'] : $product['product_price'];
	
	if (!empty($options)) {
		foreach ($options as $option) {
			$regular_price += (float) $option['option_price'];
		}
	}
	
	$has_discount = false;

	$discount_price = 0; // Product::getDiscountPrice($regular_price, $product['category_id'], $product['brand_id']);
	$cart_price_qty = $cart_item['price'];

	if (!empty($discount_price['discount'])) {
		$has_discount = true;
	}

	// $discount_price['price'] = number_format((float) $discount_price['price'], 2, '.', '');
	$regular_price = number_format((float) $regular_price, 2, '.', '');

	$qty_in_stock = !empty($variant) ? $variant['variant_stock_qty'] : $product['product_stock_qty'];
	$cart_item_disabled = (Yii::app()->params->settings['stock'] != 'none' && !$qty_in_stock) ? true : false;
?>
<div class="cart-product<?php if ($cart_item_disabled) { ?> cart-product--na<?php } ?>">
	<div class="cart-product__img">
		<?php if (!empty($product_image_1x)) { ?>
		<a href="<?=$product_url?>">
			<picture>
				<?php /* <source type="image/webp" srcset="images/p2.webp, images/p2@2x.webp 2x"> */ ?>
				<source srcset="<?=$product_image_1x?>, <?=$product_image_2x?> 2x">
				<img src="<?=$product_image_1x?>" alt="<?=$product_title?>">
			</picture>
		</a>
		<?php } ?>
	</div>
	<div class="cart-product__content">
		<?php if ($cart_item_disabled) { ?>
		<div class="cart-product__title"><?=Lang::t('cart.tip.notAvailable')?></div>
		<?php } else { ?>
		<div class="cart-product__title"><a href="<?=$product_url?>"><?=$product_title?></a></div>
		<?php } ?>
		
		<?php if (!empty($variant['values'])) { ?>
		<?php foreach ($variant['values'] as $value) { ?>
		<div class="cart-product__option"><?=CHtml::encode($value['property_title'])?>: <?=CHtml::encode($value['value_title'])?></div>
		<?php } ?>
		<?php } ?>

		<div class="cart-product__control">
			<?php /*<div class="cart-product__price"><?=number_format($cart_item['price'], 0, '.', ' ')?> <?=Lang::t('layout.tip.uah')?></div>*/ ?>
			<div class="cart-product__price">
				<?=round($cart_item['price'])?> <?=Lang::t('layout.tip.uah')?>
				<?php if (!empty($regular_price_old)) { ?>
				<br><small><s><?=round($regular_price_old)?></s> <?=Lang::t('layout.tip.uah')?></small>
				<?php } ?>
			</div>
			<div class="cart-product__quantity">
				<button type="button" class="cart-product__quantity-btn cart-product__quantity-btn--subtract" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>" data-action="subtract"></button>
				<input type="text" class="cart-product__quantity-input" name="cart[qty]" value="<?=$cart_item['qty']?>" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>">
				<button type="button" class="cart-product__quantity-btn cart-product__quantity-btn--add<?php if ($cart_item['qty'] == $qty_in_stock) { ?> cart-product__quantity-btn--disabled<?php } ?>" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>" data-action="add"></button>
			</div>
		</div>

		<button type="button" class="cart-product__remove" data-id="<?=$product_id?>" data-vid="<?=$variant_id?>" data-oid="<?=$option_id?>" data-action="remove"><?=Lang::t('cart.btn.remove')?></button>
	</div>
	<?php /* if ((isset($box_banner['category_id']) && $product['category_id'] == $box_banner['category_id']) || (isset($postcard_banner['category_id']) && $product['category_id'] == $postcard_banner['category_id'])) { */ ?>
	<?php if (isset($postcard_banner['category_id']) && $product['category_id'] == $postcard_banner['category_id']) { ?>
	<div class="cart-product__comment">
		<textarea class="cart-product__comment-input" name="cart[comment]" rows="5" placeholder="<?=CHtml::encode(Lang::t('cart.placeholder.wishes'))?>" data-index="<?=$cart_index?>"><?=isset($cart_item['comment']) ? CHtml::encode($cart_item['comment']) : ''?></textarea>
	</div>
	<?php } ?>
</div>
<?php } ?>