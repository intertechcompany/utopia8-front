<?php
	/* @var $this SiteController */
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<main class="news">
	<div class="wrap">
		<?php $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); ?>
	
		<?php if (empty($category)) { ?>
		<h1 class="news__title"><?=$this->pageTitle?></h1>
		<?php } else { ?>
		<h1 class="news__title"><?=CHtml::encode($category['category_name'])?></h1>
		<?php } ?>
		<ul class="news__tags list-unstyled">
			<li><a href="<?=$this->createUrl('site/news')?>" class="news__tag<?php if (empty($category)) { ?> news__tag--active<?php } ?>">Всі новини</a></li>
			<?php foreach ($categories as $category_item) { ?>
			<li><a href="<?=$this->createUrl('site/newscategory', ['alias' => $category_item['category_alias']])?>" class="news__tag<?php if (!empty($category['category_id']) && $category_item['category_id'] == $category['category_id']) { ?> news__tag--active<?php } ?>"><?=CHtml::encode($category_item['category_name'])?></a></li>
			<?php } ?>
		</ul>

		<?php if (!empty($news)) { ?>
		<div class="news-list">
			<?php $this->renderPartial('newsList', ['blogs' => $news]); ?>
		</div>
		<?php } else { ?>
		<p><?=Lang::t('news.tip.noArticlesFound')?></p>
		<?php } ?>
	</div>
</main>
