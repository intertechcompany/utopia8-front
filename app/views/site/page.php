<?php
	/* @var $this SiteController */
	$staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="page">
	<div class="page__head wrap">
		<h1 class="page__title"><?=CHtml::encode($page['page_title'])?></div>
	</div>
	<div class="page__content">
		<div class="page__main">
			<?php if ($page['page_type'] != 'faq') { ?>
			<div class="page__description">
				<?=$page['page_description']?>
			</div>
			<?php } ?>
			<?php if ($page['page_type'] == 'contact') { ?>
			<div class="stores">
				<div class="store">
					<div class="store__content">
						<div class="store__title"><?=Lang::t('page.tip.storeKhTitle')?></div>
						<div class="store__details">
							<?=Lang::t('page.html.storeKh')?>
						</div>
					</div>
					<div class="store__image">
						<picture>
							<source type="image/webp" srcset="<?=$staticUrl?>/images/store_kh.webp">
							<source srcset="<?=$staticUrl?>/images/store_kh.jpg">
							<img src="<?=$staticUrl?>/images/store_kh.jpg" alt=""> 
						</picture>
					</div>
				</div>
				<div class="store">
					<div class="store__content">
						<div class="store__title"><?=Lang::t('page.tip.storeOdTitle')?></div>
						<div class="store__details">
							<?=Lang::t('page.html.storeOd')?>
						</div>
					</div>
					<div class="store__image">
						<picture>
							<source type="image/webp" srcset="<?=$staticUrl?>/images/store_od.webp">
							<source srcset="<?=$staticUrl?>/images/store_od.jpg">
							<img src="<?=$staticUrl?>/images/store_od.jpg" alt=""> 
						</picture>
					</div>
				</div>
			</div>
			<?php } elseif ($page['page_type'] == 'faq') {
			    $faq_content = json_decode($page['page_content_faq'], true);
			?>
			<div class="page__description faq">
				<?php foreach ($faq_content as $faq_item) { ?>
				<div class="faq__item faq__item--collapsed">
					<div class="faq__question"><?=CHtml::encode($faq_item['title'])?></div>
					<div class="faq__answer">
						<?=$faq_item['text']?>
					</div>
				</div>
				<?php } ?>
			</div>	
			<?php } ?>
		</div>
		<aside class="page__side">
			<div class="page__side-inner">
				<div class="page-menu">
					<div class="page-menu__tip"><?=Lang::t('page.tip.info')?></div>
					<ul class="page-menu__list list-unstyled">
						<?php foreach ($pages as $page_item) { ?>
						<li><a href="<?=$this->createUrl('page', ['alias' => $page_item['page_alias']])?>" class="page-menu__link<?php if ($page_item['page_alias'] == $page['page_alias']) { ?> page-menu__link--active<?php } ?>"><?=CHtml::encode($page_item['page_title'])?></a></li>
						<?php } ?>
					</ul>
				</div>
				<div class="page__info">
					<?=str_replace(["\r", "\n"], ["", "<br>\n"], Lang::t('page.tip.infoText'))?>
				</div>
			</div>
		</aside>
	</div>
</main>
<!-- /.page -->