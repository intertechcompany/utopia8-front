<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<main class="category category--catalog">
    <div class="category__head<?php if ($facets->hasFilter()) { ?> category__head--has-reset<?php } ?> wrap">
        <?php if (count($categories[$category['category_id']]['parents']) > 1) { ?>
        <?php $last_index = count($categories[$category['category_id']]['parents']) - 1; ?>
        <div class="category__breadcrumbs">
            <?php foreach ($categories[$category['category_id']]['parents'] as $index => $parent_id) { ?>
            <?php if ($index == $last_index) { ?>
            <span><?=CHtml::encode($categories[$parent_id]['category_name'])?></span>
            <?php } else { ?>
            <a href="<?=$this->createUrl('category', ['alias' => $categories[$parent_id]['category_alias']])?>"><?=CHtml::encode($categories[$parent_id]['category_name'])?></a> › 
            <?php } ?>
            <?php } ?>
        </div>
        <?php } ?>
        <?php /* $this->widget('application.components.Breadcrumbs.Breadcrumbs', ['breadcrumbs' => $this->breadcrumbs]); */ ?>

        <div class="category__title">
            <?=!empty($category['category_title']) ? CHtml::encode($category['category_title']) : CHtml::encode($category['category_name'])?>
            <?php if ($pages->itemCount > 0) { ?>
            <small><span><?=$pages->itemCount?></span> <?=Lang::t('catalog.tip.pcs')?></small>
            <?php } ?>
        </div>

        <?php if ($facets->hasFilter()) { ?>
		<?php $this->renderPartial('facetsReset', array('facets' => $facets, 'category' => $category, 'sort' => $sort)); ?>
		<?php } ?>

        <?php
            $sort_params = array(
                'popular' => array(
                    'title' => Lang::t('catalog.select.sortPopular'),
                    'url' => null, // default value
                ),
                'price' => array(
                    'title' => Lang::t('catalog.select.sortPrice'),
                    'url' => 'price',
                ),
                'newest' => array(
                    'title' => Lang::t('catalog.select.sortNewest'),
                    'url' => 'newest',
                ),
            );
        ?>
        <div class="category__sort">
            <?=Lang::t('catalog.tip.sort')?>
            <div class="category__sort-select">
                <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                <?php if ($sort_index == $sort) { ?>
                <div class="category__sort-value"><?=CHtml::encode($sort_param['title'])?></div>
                <?php break; } ?>
                <?php } ?>
                <select>
                    <?php foreach ($sort_params as $sort_index => $sort_param) { ?>
                    <?php 
                        $sort_url_params = ['alias' => $category['category_alias']];

                        if (!empty($sort_param['url'])) {
                            $sort_url_params = [
                                'alias' => $category['category_alias'], 
                                'sort' => $sort_param['url'],
                            ];
                        }

                        if (!empty($filters)) {
                            $sort_url_params['filter'] = $filters;
                        }

                        $sort_url = $this->createUrl('category', $sort_url_params);
                    ?>
                    <option value="<?=$sort_url?>"<?php if ($sort_index == $sort) { ?> selected<?php } ?>><?=CHtml::encode($sort_param['title'])?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="category__catalog">
        <a href="#" class="category__mobile-filter"><i class="icon-inline icon-filter"></i><?=Lang::t('catalog.tip.filters')?></a>
        <div class="category__side">
            <form id="filter" class="category-filters" action="<?=$this->createUrl('category', ['alias' => $category['category_alias']])?>" method="get">
                <div class="category-filter">
                    <div class="category-filter__head"><?= empty($categories[$category['category_id']]['sub']) && isset($categories[$category['parent_id']]) ? $categories[$category['parent_id']]['category_name'] : $categories[$category['category_id']]['category_name'] ?></div>
                    <div class="category-filter__list">
                        <?php foreach ($subcategories as $subcategory) { ?>
                        <div class="category-filter__item">
                            <a href="<?=$this->createUrl('category', ['alias' => $subcategory['category_alias']])?>" class="category-filter__link<?php if ($subcategory['category_id'] == $category['category_id']) { ?> category-filter__link--active<?php } ?>">
                                <span class="category-filter__title"><?=CHtml::encode($subcategory['category_name'])?></span>
                                <span class="category-filter__counter"><?= $subcategory['total'] ? $subcategory['total'] : '-' ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if (!empty($tags)) { ?>
                <div class="category-filter">
                    <div class="category-filter__head"><?=Lang::t('catalog.tip.tags')?></div>
                    <div class="category-filter__list">
                        <?php foreach ($tags as $tag) { ?>
                        <div class="category-filter__item">
                            <a href="<?=$this->createUrl('tag', ['alias' => $tag['tag_alias'], 'cid' => $category['category_id']])?>" class="category-filter__link">
                                <span class="category-filter__title">
                                    <?php if (!empty($tag['tag_image'])) { $tag_image = json_decode($tag['tag_image'], true); ?>
                                    <img src="<?=$assetsUrl?>/tag/<?=$tag_image['file']?>" alt="" style="width: 20px; height: auto; margin-top: -2px; margin-right: 4px">
                                    <?php } ?>
                                    <?=CHtml::encode($tag['tag_name'])?>
                                </span>
                                <span class="category-filter__counter"><?= $tag['total'] ? $tag['total'] : '-' ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <?php if ($facets->hasFacets()) { ?>
			    <?php $this->renderPartial('facets', array('facets' => $facets, 'sort' => $sort)); ?>
                <?php } ?>
                <div class="category-filter__bottom<?php if (!$facets->hasFilter()) { ?> category-filter__bottom--hidden<?php } ?>">
                    <button type="button" class="category-filter__show category-filter__show--hidden"><?=Lang::t('catalog.btn.showFiltered')?> (<span><?=$pages->itemCount?></span>)</button>
                    <button type="button" class="category-filter__reset<?php if (!$facets->hasFilter()) { ?> category-filter__reset--hidden<?php } ?>"><?=Lang::t('catalog.btn.resetFiltered')?></button>
                </div>
		    </form>
        </div>
        <div class="category__products">
            <?php if (!empty($products)) { ?>
            <?php $this->renderPartial('productsList', ['products' => $products]); ?>
            <?php } else { ?>
            <p style="padding: 15px"><?=Lang::t('catalog.tip.productsNotFound')?></p>
            <?php } ?>

            <?php $this->renderPartial('pagination', ['pages' => $pages]); ?>
        </div>
    </div>
</main>