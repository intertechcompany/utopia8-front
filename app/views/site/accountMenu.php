<div class="account-menu-box">
    <div class="box-heading"><?= Lang::t('layout.title.account') ?></div>
    <div class="box-content">
        <ul>
            <li><a href="<?= $this->createUrl('site/account') ?>"><?= Lang::t('layout.title.account') ?></a></li>
            <li><a href="<?= $this->createUrl('site/accountedit') ?>"><?= Lang::t('layout.title.accountEdit') ?></a></li>
            <li><a href="<?= $this->createUrl('site/accountaddress') ?>"><?= Lang::t('layout.title.accountAddress') ?></a></li>
            <li><a href="<?= $this->createUrl('site/orders') ?>"><?= Lang::t('layout.title.ordersHistory') ?></a></li>
            <li><a href="<?= $this->createUrl('site/accountsubscription') ?>"><?= Lang::t('layout.title.editSubscription') ?></a></li>
            <li><a href="<?= $this->createUrl('site/logout') ?>"><?= Lang::t('layout.title.logout') ?></a></li>
        </ul>
    </div>
</div>