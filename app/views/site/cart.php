<?php
    /* @var $this SiteController */
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
?>
<main id="cart" class="cart cart--page">
    <div class="cart__head wrap">
        <h1 class="cart__title"><?=Lang::t('cart.tip.cartTitle')?></h1>
    </div>
    <div class="cart-details wrap">
        <?php if (empty($cart)) { ?>
        <p><?=Lang::t('cart.tip.empty')?></p>
        <?php } else { ?>
        <div class="cart-details__title"><?=Lang::t('cart.tip.products', $total)?></div>
        <div class="cart-products">
            <div class="cart-products__col">
                <form action="<?=$this->createUrl('cart')?>" class="cart-products__list" method="post">
                    <?php $this->renderPartial('cartList', [
                        'cart' => $cart,
                        'box_banner' => $box_banner,
                        'postcard_banner' => $postcard_banner,
                    ]); ?>
                </form>
                
                <?php $this->renderPartial('//site/gifts', [
                    'box_banner' => $box_banner,
                    'postcard_banner' => $postcard_banner,
                ]); ?>
            </div>

            <div class="cart-products__col">
                <div class="cart-products__summary">
                    <div class="cart__total">
                        <span><?=Lang::t('cart.tip.total')?></span>
                        <span><?=$price?> <?=Lang::t('layout.tip.uah')?></span>
                    </div>

                    <div class="cart__checkout"><a href="<?=$this->createUrl('checkout')?>" class="cart__checkout-btn<?php if ($has_unavailable) { ?> cart__checkout-btn--disabled<?php } ?>"><?=Lang::t('cart.btn.checkout')?></a></div>

                    <div class="cart__info">
                        <p>&nbsp;</p>
                        <?=Lang::t('cart.html.additinalInfo')?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</main>
<!-- /.cart -->
