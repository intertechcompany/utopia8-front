<table>
    <tbody>
        <tr>
            <td><label for="af-user_first_name"><?=Lang::t('account.label.firstName')?></label></td>
            <td><input id="af-user_first_name" type="text" name="user[user_first_name]" value="<?=CHtml::encode($user['user_first_name'])?>"></td>
        </tr>
        <tr>
            <td><label for="af-user_last_name"><?=Lang::t('account.label.lastName')?></label></td>
            <td><input id="af-user_last_name" type="text" name="user[user_last_name]" value="<?=CHtml::encode($user['user_last_name'])?>"></td>
        </tr>
        <tr>
            <td><label for="af-user_email"><?=Lang::t('account.label.email')?></label></td>
            <td><input id="af-user_email" type="email" name="user[user_email]" value="<?=CHtml::encode($user['user_email'])?>"></td>
        </tr>
        <tr>
            <td><label for="af-user_phone"><?=Lang::t('account.label.phone')?></label></td>
            <td><input id="af-user_phone" type="tel" name="user[user_phone]" value="<?=CHtml::encode($user['user_phone'])?>"></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="action" value="personal">
                <button class="btn"><?=Lang::t('account.btn.save')?></button>
            </td>
        </tr>
    </tbody>
</table>
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_zip">--><?//=Lang::t('checkout.label.zip')?><!--</label>-->
<!--		<input id="af-user_zip" type="text" name="user[user_zip]" value="--><?//=CHtml::encode($user['user_zip'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_city">--><?//=Lang::t('checkout.label.city')?><!--</label>-->
<!--		<input id="af-user_city" type="text" name="user[user_city]" value="--><?//=CHtml::encode($user['user_city'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_address">--><?//=Lang::t('checkout.label.address1')?><!--</label>-->
<!--		<input id="af-user_address" type="text" name="user[user_address]" value="--><?//=CHtml::encode($user['user_address'])?><!--">-->
<!--	</div>-->
<!--</div>-->
<!--<div class="form-row clearfix">-->
<!--	<div class="form-col form-col-100">-->
<!--		<label for="af-user_address_2">--><?//=Lang::t('checkout.label.address2')?><!--</label>-->
<!--		<input id="af-user_address_2" type="text" name="user[user_address_2]" value="--><?//=CHtml::encode($user['user_address_2'])?><!--">-->
<!--	</div>-->
<!--</div>-->