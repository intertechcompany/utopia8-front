��          �      l      �  
   �  O   �  y   <  I   �  i         j  "   y  !   �     �     �  p   �     [     z  (   �  :   �     �  
     �     �   �  �   �    �     �	     �	     �	     �	  8   
     :
     A
     V
     j
     q
  >   �
     �
     �
     �
  $   �
            D     K   ]  B   �                             
      	                                                               appE-mail app{n} артист|{n} артиста|{n} артистов|{n} артиста app{n} знаменитость|{n} знаменитости|{n} знаменитостей|{n} знаменитости app{n} модель|{n} модели|{n} моделей|{n} модели appВведите e-mail и пароль или войдите через социальную сеть! appВойти appВойти через Facebook appВойти через Google+ appВход appВход в кабинет appВы ввели e-mail в неверном формате, корректный формат mail@domain.com appЗабыли пароль? appЗапомнить меня appЗарегистрироваться appНе зарегистрированы на {sitename}? appПароль appили appнайден <strong>{n} артист</strong>|найдено <strong>{n} артиста</strong>|найдено <strong>{n} артистов</strong>|найдено <strong>{n} артиста</strong> appнайдена <strong>{n} знаменитость</strong>|найдено <strong>{n} знаменитости</strong>|найдено <strong>{n} знаменитостей</strong>|найдено <strong>{n} знаменитости</strong> appнайдена <strong>{n} модель</strong>|найдено <strong>{n} модели</strong>|найдено <strong>{n} моделей</strong>|найдено <strong>{n} модели</strong> Project-Id-Version: my-project.en 1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-26 14:29+0200
PO-Revision-Date: 
Last-Translator: Ievgenii Syrotenko <jjboun@gmail.com>
Language-Team: JustWeb <ask@justweb.com.ua>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-KeywordsList: _ngettext:1,2;t:1c,2
X-Generator: Poedit 1.6.10
X-Poedit-SearchPath-0: ../..
 E-mail {n} artist|{n} artists {n} celebrity|{n} celebrities {n} model|{n} models Enter e-mail and password or log in with social network! Log in Log in with Facebook Log in with Google+ Log in Log in to account Invalid e-mail! Enter correct e-mail in format mail@domain.com Forgot password? Remember me Sign Up Don't have an account in {sitename}? Password or found <strong>{n} artist</strong>|found <strong>{n} artists</strong> found <strong>{n} celebrity</strong>|found <strong>{n} celebrities</strong> found <strong>{n} model</strong>|found <strong>{n} models</strong> 