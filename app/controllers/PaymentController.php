<?php
class PaymentController extends Controller
{
	/**
	 * Overrided method from CController.
	 * Set global indexing site setting.
	 * 
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	/**
	 * Payment API response.
	 */
	public function actionApi()
	{
		$data = json_decode(Yii::app()->request->getRawBody(), true);

		if ($data === null) {
            throw new CException(json_last_error_msg(), json_last_error());
        }
		
		$payment = new Payment();
		$payment->callback($data);

		header('Content-type: application/json; charset=utf-8');
		echo $payment->response($data);
	}

	/**
	 * Payment result.
	 */
	public function actionResult()
	{
		$data = $_POST;
		
		$payment = new Payment();
		$payment->result($data);
	}
}