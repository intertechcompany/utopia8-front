<?php

class AjaxController extends Controller
{
	public $json = array();
	
	protected function beforeAction($action)
	{
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
		}
		
		return parent::beforeAction($action);
	}
	
	public function init() {        
		parent::init();
		Yii::app()->errorHandler->errorAction = 'ajax/error';
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// block direct access to ajax.php
		throw new CHttpException(403, 'Access denied.');
	}

	public function actionDiscount()
	{
		$discount = Yii::app()->request->getPost('discount');
		
		if (!empty($discount)) {
			$model = new DiscountForm();
			$model->attributes = $discount;

			if ($model->validate()) {
				Discount::model()->apply($model->discount);

				if ($model->discount['discount_type'] == 'percentage') {
					$discount_value_tip = '-' . $model->discount['discount_value'] . '%';
				} else {
					$discount_value_tip = '-' . $model->discount['discount_value'];
				}

				$price = Cart::model()->getPrice();
				$discount_value = Discount::getDiscountValue($price);
				
				$this->json = [
					'message' => Lang::t('checkout.tip.discount'),
					'code' => $model->discount['discount_code'],
					'value' => $discount_value,
					'price' => number_format($price + $discount_value, 0, '.', ' '),
				];
			} else {
				Discount::model()->clear();
				
				$errors = $model->jsonErrors();
				$error_message = implode("<br>\n", $errors['messages']);

				$this->json = [
					'error' => true,
					'errorCode' => '', // $error_message,
					'errorFields' => $errors['fields'],
				];
			}
		} else {
			throw new CHttpException(400, 'Bad request!');
		}
	}

	/**
     * Registration form.
     */
    public function actionRegistration()
    {
        $registration = Yii::app()->request->getPost('registration');

        if (!empty($registration)) {
            $model = new RequestForm();
            $model->attributes = $registration;

            if ($model->validate()) {
                $request_id = Request::model()->add($model);

                if ($request_id) {
					$subject = Lang::t('email.subject.registration', ['{request_id}' => $request_id]);
					$body = Yii::app()->getController()->renderPartial('//email/registration', [
						'model' => $model,
						'request_id' => $request_id,
					], true);

					// send email to admins
					$mailer = new EsputnikMailer();
					$emails = explode(', ', Yii::app()->params->settings['notify_mail']);
					$mailer->send($emails, $subject, $body);

					// send email to client
					$mailer->send([$model->email], $subject, $body);
					
					$this->json = [
                        'msg' => $this->renderPartial('success', [], true), // Lang::t('form.tip.success'),
                    ];
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('form.error.requestNotSent'),
                        'errorFields' => [],
                    ];
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = [
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                ];
            }
        }
	}
	
	/**
     * Subscribe form.
     */
    public function actionSubscribe()
    {
        $subscribe = Yii::app()->request->getPost('subscribe');

        if (!empty($subscribe)) {
            $model = new SubscriberForm();
            $model->attributes = $subscribe;

            if ($model->validate()) {
                $subscriber_id = Subscriber::model()->add($model);

                if ($subscriber_id) {
                    $this->json = [
                        'message' => Lang::t('subscribe.tip.success'),
                    ];
                } else {
                    $this->json = [
                        'error' => true,
                        'errorCode' => Lang::t('form.error.requestNotSent'),
                        'errorFields' => [],
                    ];
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = [
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                ];
            }
        }
    }

	public function actionNpDepartments()
	{
		header('Access-Control-Allow-Origin: *');
		
		$city = Yii::app()->request->getQuery('city', '');
		$city = trim($city);

		$this->json = [
			'options' => [],
		];

		if (!empty($city)) {
			$np_model = new NovaPoshta();
			$np_city = $np_model->getCity($city);
			
			if (!empty($np_city)) {
				$np_departments = $np_model->getDepartments($np_city['city_ref']);

				if (!empty($np_departments)) {
					$options = array();
					$field = (Yii::app()->language == 'ru') ? 'department_name_ru' : 'department_name';
					
					foreach ($np_departments as $np_department) {
						$this->json['options'][] = [
							'value' => $np_department[$field],
							'text' => $np_department[$field],
						];
					}

					$cost = $np_model->getDeliveryPrice($np_city['city_ref']);

					$this->json['cost'] = isset($cost[0]['Cost']) ? 'орієнтовна ціна ' . $cost[0]['Cost'] . ' грн' : 0;
				}
			}
		}
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			//echo $error['message'];
			$this->json = array('error' => 'Y', 'errorCode' => $error['message'], 'errorFields' => array());
		}
	}
	
	protected function afterAction($action)
	{
		header('Vary: Accept');
		 
		if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
			header('Content-type: application/json; charset=utf-8');
		}

		echo json_encode($this->json);
		Yii::app()->end();
	}
}