<?php
class SiteController extends Controller
{
	public $dataLayer = [];

	private $json = array();
	private $per_page = 29;

	/**
	 * Overrided method from CController.
	 * Set global indexing site setting.
	 * 
	 * @param CAction $action the action to be executed.
	 * @return boolean whether the action should be executed.
	 */
	protected function beforeAction($action)
	{
		if (!Yii::app()->user->isGuest && in_array($this->route, array('site/login', 'site/reset'))) {
			$this->redirect(array('site/account'));
		} elseif (Yii::app()->user->isGuest && in_array($this->route, array(
			'site/account',
			'site/orders',
			'site/order',
		))) {
			if (Yii::app()->request->isAjaxRequest) {
				echo json_encode(array(
					'redirect' => $this->createUrl('site/login'),
				));
			} else {
				$this->redirect(array('site/login'));
			}
		}

		$category_list = Category::model()->getCategoriesList();

		if (Yii::app()->params->settings['no_index']) {
			$this->noIndex = true;
		}
		
		return parent::beforeAction($action);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->pageTitle = Lang::t('seo.meta.titleHome');
		$this->pageDescription = Lang::t('seo.meta.descriptionHome');
		$this->canonicalUrl = $this->createAbsoluteUrl('index');

		$categories = Yii::app()->params->categories;
		$banners_1 = Banner::model()->getSliderBanners();
		$banners_2 = Banner::model()->getSection1Banners();
		$banners_3 = Banner::model()->getSection2Banners();
		
		$banners_p_1 = Banner::model()->getSection3Banners();
		$banners_p_2 = Banner::model()->getSection4Banners();
		$banners_p_3 = Banner::model()->getSection5Banners();
		
		$facets = Facet::model();
		// $facets->setFacetsInput($filters);
		// $facets->getCategoryFacets($category['category_id']);
		
		$products_1 = Product::model()->getCategoryProducts(0, 8, 2, $facets, 'popular');
		$products_2 = Product::model()->getCategoryProducts(0, 4, 13, $facets, 'popular');
		$products_sale = Product::model()->getSaleProducts(0, 4, 'popular');
		// $products = Product::model()->getBestSellersProducts();

        $faq = Page::model()->getPageByType('faq');

		$this->render('index', array(
			'categories' => $categories,
			'banners_1' => $banners_1,
			'banners_2' => $banners_2,
			'banners_3' => $banners_3,
			'banners_p_1' => $banners_p_1,
			'banners_p_2' => $banners_p_2,
			'banners_p_3' => $banners_p_3,
			'products_1' => $products_1,
			'products_2' => $products_2,
			'products_sale' => $products_sale,
            'faq' => $faq,
		));
	}

	/**
	 * Get newest products.
	 */
	public function actionNewest()
	{
		$categories = Yii::app()->params->categories;
		$categories_tree = Yii::app()->params->categories_tree;

		$this->pageTitle = 'Новинки';
		$this->pageDescription = '';
		$this->canonicalUrl = $this->createAbsoluteUrl('newest');

		$show_all = (bool) Yii::app()->request->getQuery('show_all', false);

		// get filters
		$filters = Yii::app()->request->getQuery('filter', array());

		// get sort
		$sort = Yii::app()->request->getQuery('sort');
		if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
			$sort = 'popular';
		}

		// set pages
		$pages = new CPagination();
		$pages->setPageSize($this->per_page);
		$pages->params = array();

		if (!empty($sort) && $sort != 'popular') {
			$pages->params['sort'] = $sort;
		}

		if (!empty($filters)) {
			$pages->params['filter'] = $filters;
		}

		// build facets
		$facets = Facet::model();
		$facets->setFacetsInput($filters);
		// $facets->getCategoryFacets($category['category_id']);

		$products = array();

		$total = Product::model()->getNewestProductsTotal($this->per_page);

		if ($total['total']) {
			$pages->setItemCount($total['total']);

			$offset = $pages->getOffset();
			$limit = $pages->getLimit();

			if ($show_all) {
				$limit = $total['total'];
			}
			
			// get products
			$products = Product::model()->getNewestProducts($offset, $limit, $sort);
		}

		$this->render('newestProducts', array(
			'categories' => $categories,
			'categories_tree' => $categories_tree,
			'products' => $products,
			'facets' => $facets,
			'sort' => $sort,
			'filters' => $filters,
			'pages' => $pages,
			'show_all' => $show_all,
		));
	}

	/**
	 * Get sale products.
	 */
	public function actionSale()
	{
		$categories = Yii::app()->params->categories;
		$categories_tree = Yii::app()->params->categories_tree;
		$brands = Yii::app()->params->brands;

		$this->pageTitle = 'SALE';
		$this->pageDescription = '';
		$this->canonicalUrl = $this->createAbsoluteUrl('sale');

		// sale brands and categories
		$sale_categories = [];
		$sale_brands = [];

		foreach ($categories as $category) {
			if (!empty($category['category_discount'])) {
				$sale_categories[] = $category['category_id'];
			}
		}
		
		foreach ($brands as $brand) {
			if (!empty($brand['brand_discount'])) {
				$sale_brands[] = $brand['brand_id'];
			}
		}

		// get filters
		$filters = Yii::app()->request->getQuery('filter', array());

		// get sort
		$sort = Yii::app()->request->getQuery('sort');
		if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
			$sort = 'popular';
		}

		// set pages
		$pages = new CPagination();
		$pages->setPageSize($this->per_page);
		$pages->params = array();

		if (!empty($sort) && $sort != 'popular') {
			$pages->params['sort'] = $sort;
		}

		if (!empty($filters)) {
			$pages->params['filter'] = $filters;
		}

		// build facets
		$facets = Facet::model();
		$facets->setFacetsInput($filters);
		// $facets->getCategoryFacets($category['category_id']);

		$products = array();

		$total = Product::model()->getSaleProductsTotal($this->per_page, $sale_categories, $sale_brands);

		if ($total['total']) {
			$pages->setItemCount($total['total']);

			$offset = $pages->getOffset();
			$limit = $pages->getLimit();
			
			// get products
			$products = Product::model()->getSaleProducts($offset, $limit, $sort, $sale_categories, $sale_brands);
		}

		$this->render('saleProducts', array(
			'categories' => $categories,
			'categories_tree' => $categories_tree,
			'products' => $products,
			'facets' => $facets,
			'sort' => $sort,
			'filters' => $filters,
			'pages' => $pages,
		));
	}

	/**
	 * Get catalog category.
	 * 
	 * @param string $alias category alias identifier.
	 * @throws CHttpException 404 error code when a category is not found in DB.
	 */
	public function actionCategory($alias)
	{
		$category_model = Category::model();

		// get category
		$category = $category_model->getCategoryByAlias($alias);
		
		if (!empty($category)) {
			$categories = Yii::app()->params->categories;
			$categories_tree = Yii::app()->params->categories_tree;

			$this->pageTitle = !empty($category['category_meta_title']) ? 
				$category['category_meta_title'] : 
				Lang::t('seo.metaTitle.category', ['{category_name}' => $category['category_name']]);
			$this->pageDescription = !empty($category['category_meta_description']) ?
				$category['category_meta_description'] : 
				Lang::t('seo.metaDescription.category', ['{category_name}' => $category['category_name']]);
			$this->canonicalUrl = $this->createAbsoluteUrl('category', array('alias' => $category['category_alias']));

			if (!empty($category['category_no_index'])) {
				$this->noIndex = true;
			}

			$this->breadcrumbs = [
				$category['category_name'] => $this->canonicalUrl,
			];

			$show_all = (bool) Yii::app()->request->getQuery('show_all', false);

			// get filters
			$filters = Yii::app()->request->getQuery('filter', array());

			// get sort
			$sort = Yii::app()->request->getQuery('sort');
			if (!in_array($sort, array('popular', 'price', 'newest'))) {
				$sort = 'popular';
			}

			// set pages
			$pages = new CPagination();
			$pages->setPageSize($this->per_page);
			$pages->params = array(
				'alias' => $alias,
			);

			if (!empty($sort) && $sort != 'popular') {
				$pages->params['sort'] = $sort;
			}

			if (!empty($filters)) {
				$pages->params['filter'] = $filters;
			}

			// build facets
			// if ($category['category_type'] == 'tags') {
            //     $facets = FacetTags::model();
            //     $facets->setFacetsInput($filters);
            //     $facets->getCategoryFacets($category['category_id']);
            // } else {
                $facets = Facet::model();
                $facets->setFacetsInput($filters);
                $facets->getCategoryFacets($category['category_id']);
            // }

			// count products by category
			$subcategories = [];
			
			if (isset($categories[$category['category_id']]['sub'])) {
				$subcategories = $categories[$category['category_id']]['sub'];
			} elseif (isset($categories[$category['parent_id']]['sub'])) {
				$subcategories = $categories[$category['parent_id']]['sub'];
			}

			$facets_empty = Facet::model();

			foreach ($subcategories as $index => $subcategory) {
			    // if ($subcategory['category_type'] == 'tags') {
                //    $subcategory_total = Product::model()->getCategoryProductsTotalByTags($this->per_page, $category['category_id'], $facets_empty);
                // } else {
                    $subcategory_total = Product::model()->getCategoryProductsTotal($this->per_page, $subcategory['category_id'], $facets_empty);
                // }
                $subcategories[$index]['total'] = $subcategory_total['total'];
			}

			if (empty($subcategories)) {
                $subcategories[0] = $category;
                $subcategory_total = Product::model()->getCategoryProductsTotalByTags($this->per_page, $category['category_id'], $facets_empty);
                $subcategories[0]['total'] = $subcategory_total['total'];
			}
			
			// get category tags
			$tags = Product::model()->getCategoryProductTags($category['category_id']);

			$products = array();

            // if ($category['category_type'] == 'tags') {
            //     $total = Product::model()->getCategoryProductsTotalByTags($this->per_page, $category['category_id'], $facets);
            // } else {
                $total = Product::model()->getCategoryProductsTotal($this->per_page, $category['category_id'], $facets);
            // }

			if ($total['total']) {
				$pages->setItemCount($total['total']);

				$offset = $pages->getOffset();
				$limit = $pages->getLimit();

				if ($show_all) {
					$limit = $total['total'];
				}
				
				// get products
                // if ($category['category_type'] == 'tags') {
                //     $products = Product::model()->getCategoryProductsByTags($offset, $limit, $category['category_id'], $facets, $sort);
                // } else {
                    $products = Product::model()->getCategoryProducts($offset, $limit, $category['category_id'], $facets, $sort);
                // }
			}

            if (Yii::app()->request->isAjaxRequest) {
				$facets_json = [];
				$facets_json['price'] = $this->renderPartial('facetPrice', ['price' => $facets->getPriceFacet()], true);

				$properties = $facets->getPropertiesFacets();

				if (!empty($properties)) {
					foreach ($properties as $property_id => $property) {
						if (!empty($property['values'])) {
							foreach ($property['values'] as $value_id => $value) {
								$facets_json['option-' . $value_id] = $value['total'];
							}
						}
					}
				}
				
				$this->json = [
					'total' => $total['total'],
					'reset' => $facets->hasFilter() ? $this->renderPartial('facetsReset', ['facets' => $facets, 'category' => $category, 'sort' => $sort], true) : '',
					'facets' => $facets->hasFacets() ? $facets_json : '',
					'catalog' => $this->renderPartial('productsList', ['products' => $products], true),
					'pagination' => $this->renderPartial('pagination', ['pages' => $pages], true),
				];
			} else {
				$this->render('categoryProducts', array(
					'category' => $category,
					'categories' => $categories,
					'categories_tree' => $categories_tree,
					'subcategories' => $subcategories,
					'tags' => $tags,
					'products' => $products,
					'facets' => $facets,
					'sort' => $sort,
					'filters' => $filters,
					'pages' => $pages,
					'show_all' => $show_all,
				));
			}
			
		} else {
			throw new CHttpException(404, 'Page not found');
		}
	}

	/**
	 * Get catalog tag.
	 * 
	 * @param string $alias tag alias identifier.
	 * @throws CHttpException 404 error code when a tag is not found in DB.
	 */
	public function actionTag($alias)
	{
		$tag_model = Tag::model();

		// get tag
		$tag = $tag_model->getTagByAlias($alias);
		
		if (!empty($tag)) {
			$categories = Yii::app()->params->categories;
			$categories_tree = Yii::app()->params->categories_tree;
			$category_id = (int) Yii::app()->request->getQuery('cid', 0);
			$category = isset($categories[$category_id]) ? $categories[$category_id] : [];

			$this->pageTitle = !empty($tag['tag_meta_title']) ? 
				$tag['tag_meta_title'] : 
				Lang::t('seo.metaTitle.category', ['{category_name}' => $tag['tag_name']]);
			$this->pageDescription = !empty($tag['tag_meta_description']) ?
				$tag['tag_meta_description'] : 
				Lang::t('seo.metaDescription.category', ['{category_name}' => $tag['tag_name']]);
			$this->canonicalUrl = $this->createAbsoluteUrl('tag', array('alias' => $tag['tag_alias']));

			if (!empty($tag['tag_no_index'])) {
				$this->noIndex = true;
			}

			$this->breadcrumbs = [
				$tag['tag_name'] => $this->canonicalUrl,
			];

			$show_all = (bool) Yii::app()->request->getQuery('show_all', false);

			// get sort
			$sort = Yii::app()->request->getQuery('sort');
			if (!in_array($sort, array('popular', 'price', 'newest'))) {
				$sort = 'popular';
			}

			// set pages
			$pages = new CPagination();
			$pages->setPageSize($this->per_page);
			$pages->params = array(
				'alias' => $alias,
			);

			if (!empty($sort) && $sort != 'popular') {
				$pages->params['sort'] = $sort;
			}

			if (!empty($category)) {
				$pages->params['cid'] = $category['category_id'];
			}

			// count products by category
			$subcategories = [];
			
			if (!empty($category)) {
				if (isset($categories[$category['category_id']]['sub'])) {
					$subcategories = $categories[$category['category_id']]['sub'];
				} elseif (isset($categories[$category['parent_id']]['sub'])) {
					$subcategories = $categories[$category['parent_id']]['sub'];
				}
			}

			$facets_empty = Facet::model();

			foreach ($subcategories as $index => $subcategory) {
			    $subcategories[$index]['total'] = Product::model()->getCategoryProductsTotal($this->per_page, $subcategory['category_id'], $facets_empty);
			}
			
			// get category tags
			$tags = [];
			
            if (!empty($category)) {
                $tags = Product::model()->getCategoryProductTags($category['category_id']);
            }

			$products = array();

            $total = Product::model()->getTagProductsTotal($this->per_page, $tag['tag_id']);
            
			if ($total['total']) {
				$pages->setItemCount($total['total']);

				$offset = $pages->getOffset();
				$limit = $pages->getLimit();

				if ($show_all) {
					$limit = $total['total'];
				}
				
				// get products
                $products = Product::model()->getTagProducts($offset, $limit, $tag['tag_id'], $sort);
			}

            if (Yii::app()->request->isAjaxRequest) {				
				$this->json = [
					'total' => $total['total'],
					'reset' => '',
					'facets' => '',
					'catalog' => $this->renderPartial('productsList', ['products' => $products], true),
					'pagination' => $this->renderPartial('pagination', ['pages' => $pages], true),
				];
			} else {
				$this->render('tagProducts', array(
					'tag' => $tag,
					'category' => $category,
					'categories' => $categories,
					'categories_tree' => $categories_tree,
					'subcategories' => $subcategories,
					'tags' => $tags,
					'products' => $products,
					'sort' => $sort,
					'pages' => $pages,
					'show_all' => $show_all,
				));
			}
			
		} else {
			throw new CHttpException(404, 'Page not found');
		}
	}

	/**
	 * Get brands list
	 */
	public function actionBrands()
	{
		$this->pageTitle = 'Бренды';
		$this->pageDescription = '';
		$this->canonicalUrl = $this->createAbsoluteUrl('brands');

		$brands = Brand::model()->getBrands();

		$this->render('brands', array(
			'brands' => $brands,
		));
	}

	/**
	 * Get catalog brand.
	 * 
	 * @param string $alias brand alias identifier.
	 * @throws CHttpException 404 error code when a brand is not found in DB.
	 */
	public function actionBrand($alias)
	{
		// get brand
		$brand = Brand::model()->getBrandByAlias($alias);
		
		if (!empty($brand)) {
			$categories = Yii::app()->params->categories;
			$categories_tree = Yii::app()->params->categories_tree;
			$brands = Yii::app()->params->brands;

			$this->pageTitle = !empty($brand['brand_meta_title']) ? $brand['brand_meta_title'] : $brand['brand_name'];
			$this->pageDescription = $brand['brand_meta_description'];
			$this->canonicalUrl = $this->createAbsoluteUrl('brand', array('alias' => $brand['brand_alias']));

			if (!empty($brand['brand_no_index'])) {
				$this->noIndex = true;
			}

			$show_all = (bool) Yii::app()->request->getQuery('show_all', false);

			// get filters
			$filters = Yii::app()->request->getQuery('filter', array());

			// get sort
			$sort = Yii::app()->request->getQuery('sort');
			if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
				$sort = 'popular';
			}

			// set pages
			$pages = new CPagination();
			$pages->setPageSize($this->per_page);
			$pages->params = array(
				'alias' => $alias,
			);

			if (!empty($sort) && $sort != 'popular') {
				$pages->params['sort'] = $sort;
			}

			if (!empty($filters)) {
				$pages->params['filter'] = $filters;
			}

			// build facets
			$facets = Facet::model();
			$facets->setFacetsInput($filters);
			// $facets->getCategoryFacets($category['category_id']);

			$products = array();

			$total = Product::model()->getBrandProductsTotal($this->per_page, $brand['brand_id'], $facets);

			if ($total['total']) {
				$pages->setItemCount($total['total']);

				$offset = $pages->getOffset();
				$limit = $pages->getLimit();

				if ($show_all) {
					$limit = $total['total'];
				}
				
				// get products
				$products = Product::model()->getBrandProducts($offset, $limit, $brand['brand_id'], $facets, $sort);
			}

			$this->render('brandProducts', array(
				'brand' => $brand,
				'brands' => $brands,
				'categories' => $categories,
				'categories_tree' => $categories_tree,
				'products' => $products,
				'facets' => $facets,
				'sort' => $sort,
				'filters' => $filters,
				'pages' => $pages,
				'show_all' => $show_all,
			));
		} else {
			throw new CHttpException(404, 'Page not found');
		}
	}

	/**
	 * Get catalog category collection.
	 * 
	 * @param string $category_alias category alias identifier.
	 * @param string $collection_alias category alias identifier.
	 * @throws CHttpException 404 error code when a category or collection is not found in DB.
	 */
	public function actionCollection($category_alias, $collection_alias)
	{
		$category_model = Category::model();
		$collection_model = Collection::model();

		// get category
		$category = $category_model->getCategoryByAlias($category_alias);
		
		if (empty($category)) {
			throw new CHttpException(404, 'Page not found');
		}

		// get collection
		$collection = $collection_model->getCollectionByAlias($collection_alias);
		
		if (empty($collection)) {
			throw new CHttpException(404, 'Page not found');
		}

		$categories = Yii::app()->params->categories;
		$categories_tree = Yii::app()->params->categories_tree;

		$this->pageTitle = !empty($collection['collection_meta_title']) ? $collection['collection_meta_title'] : $collection['collection_title'];
		$this->pageDescription = $collection['collection_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('collection', array('category_alias' => $category['category_alias'], 'collection_alias' => $collection['collection_alias']));

		if (!empty($collection['collection_no_index'])) {
			$this->noIndex = true;
		}

		// get photos
		$collection_photos = $collection_model->getCollectionPhotos($collection['collection_id']);

		// get products
		$products = Product::model()->getCategoryCollectionProducts($category['category_id'], $collection['collection_id']);

		$this->render('collection', array(
			'category' => $category,
			'categories' => $categories,
			'categories_tree' => $categories_tree,
			'collection' => $collection,
			'collection_photos' => $collection_photos,
			'products' => $products,
		));
	}

	/**
	 * Get product data and render it.
	 * 
	 * @param string $alias product alias identifier.
	 * @throws CHttpException 404 error code when a product is not found in DB.
	 */
	public function actionProduct($alias)
	{
		$product_model = Product::model();

		// get product
		$product = $product_model->getProductByAlias($alias);
		
		if (!empty($product)) {
			$vid = Yii::app()->request->getQuery('vid', 0);
			$options = Yii::app()->request->getQuery('options', []);
			
			$product_badges = $product_model->getProductBadges($product['product_id']);
			$product_colors = $product_model->getProductColors($product['product_id']);
			$product_stores = $product_model->getProductStores($product['product_id']);
			$product_properties = $product_model->getProductProperties($product['product_id']);
			$product_photos = $product_model->getProductPhotos($product['product_id']);

			$product_variants = array();

			if ($product['product_price_type'] == 'variants') {
				$product_variants = $product_model->getProductVariants($product['product_id']);
			}

			$product_options = $product_model->getProductOptions($product['product_id']);

			$category_model = Category::model();
			$categories = $category_model->getCategoriesList();

			$category = array();
			$brand = array();
			$collection = array();
			$collection_products = array();
			$related_products = array();

			// get category
			if (isset($categories[$product['category_id']])) {
				$category = $categories[$product['category_id']];

				$this->breadcrumbs = [
					$category['category_name'] => $this->createAbsoluteUrl('site/category', ['alias' => $category['category_alias']]),
					$product['product_title'] => $this->canonicalUrl,
				];
			} else {
				$this->breadcrumbs = [
					$product['product_title'] => $this->canonicalUrl,
				];
			}
 
			// get brand
			$brand = Brand::model()->getBrandById($product['brand_id']);

			/* // get collection
			if (!empty($product['collection_id'])) {
				$collection = Collection::model()->getCollectionById($product['collection_id']);

				if (!empty($collection)) {
					// get collection products
					$collection_products = Product::model()->getCollectionProducts($product['collection_id'], $product['product_id']);
				}
			} */

			// get related products
			$related_products = $product_model->getRelatedProducts($product['product_id'], $product['category_id']);

			$this->pageTitle = !empty($product['product_meta_title']) ? 
				$product['product_meta_title'] : 
				Lang::t('seo.metaTitle.product', ['{product_name}' => $product['product_title'], '{brand_name}' => $brand['brand_name']]);
			$this->pageDescription = !empty($product['product_meta_description']) ?
				$product['product_meta_description'] : 
				Lang::t('seo.metaDescription.product', ['{product_name}' => $product['product_title'], '{brand_name}' => $brand['brand_name']]);

			$this->canonicalUrl = $this->createAbsoluteUrl('product', array('alias' => $product['product_alias']));
			
			if (!empty($product['product_no_index'])) {
				$this->noIndex = true;
			}

			if (!empty($product['product_photo'])) {
				$product_photo = json_decode($product['product_photo'], true);
				
				if (!empty($product_photo['path']['large']['2x'])) {
					$this->ogImage = Yii::app()->assetManager->getBaseUrl() . '/product/' . $product['product_id'] . '/' . $product_photo['path']['large']['2x'];
					$this->ogImageWidth = $product_photo['size']['large']['2x']['w'];
					$this->ogImageHeight = $product_photo['size']['large']['2x']['h'];
				} else {
					$this->ogImage = Yii::app()->assetManager->getBaseUrl() . '/product/' . $product['product_id'] . '/' . $product_photo['path']['large']['1x'];
					$this->ogImageWidth = $product_photo['size']['large']['1x']['w'];
					$this->ogImageHeight = $product_photo['size']['large']['1x']['h'];
				}
			}

			$base_category = BaseCategory::model()->getBaseCategoryById($product['base_category_id']);

			// get size
			$size = '';
			
			if (!empty($product['product_size'])) {
				$size = $product['product_reclamation'];
			} elseif (!empty($product['size_id'])) {
				$size = Size::model()->getSizeById($product['size_id']);
			} elseif (!empty($base_category['size_id'])) {
				$size = Size::model()->getSizeById($base_category['size_id']);
			}

			// get reclamation
			$reclamation = '';
			
			if (!empty($product['product_reclamation'])) {
				$reclamation = $product['product_reclamation'];
			} elseif (!empty($product['reclamation_id'])) {
				$reclamation = Reclamation::model()->getReclamationById($product['reclamation_id']);
			} elseif (!empty($base_category['reclamation_id'])) {
				$reclamation = Reclamation::model()->getReclamationById($base_category['reclamation_id']);
			}

			// get care
			$care = '';
			
			if (!empty($product['product_care'])) {
				$care = $product['product_care'];
			} elseif (!empty($product['care_id'])) {
				$care = Care::model()->getCareById($product['care_id']);
			} elseif (!empty($base_category['care_id'])) {
				$care = Care::model()->getCareById($base_category['care_id']);
			}

			$this->render('product', array(
				'categories' => $categories,
				'product' => $product,
				'product_badges' => $product_badges,
				'product_colors' => $product_colors,
				'product_stores' => $product_stores,
				'product_properties' => $product_properties,
				'product_photos' => $product_photos,
				'product_variants' => $product_variants,
				'product_options' => $product_options,
				'brand' => $brand,
				'category' => $category,
				'size' => $size,
				'reclamation' => $reclamation,
				'care' => $care,
				// 'collection' => $collection,
				// 'collection_products' => $collection_products,
				'related_products' => $related_products,
				'vid' => $vid,
				'option_ids' => $options,
			));
		} else {
			throw new CHttpException(404, 'Page not found');
		}
	}

	/**
	 * Search products by keyword.
	 */
	public function actionSearch()
	{
		$keyword = Yii::app()->request->getQuery('keyword', null);
		
		// get sort
		$sort = Yii::app()->request->getQuery('sort');
		if (!in_array($sort, array('popular', 'price-asc', 'price-desc'))) {
			$sort = 'popular';
		}

		$pages = new CPagination();
		$products = [];
		$errors = '';

		$this->pageTitle = Lang::t('seo.meta.titleSearch');
		$this->pageDescription = '';
		$this->noIndex = true;

		$form = new SearchForm;
		$form->keyword = $keyword;

		// $categories = Yii::app()->params->categories;
		// $categories_tree = Yii::app()->params->categories_tree;

		if ($form->validate()) {
			$this->pageTitle .= ' «' . $form->keyword . '»';

			// get sort
			$sort = Yii::app()->request->getQuery('sort');
			if (!in_array($sort, array('popular', 'price', 'newest'))) {
				$sort = 'popular';
			}

			// set pages
			$pages->setPageSize($this->per_page);
			$pages->params = array(
				'keyword' => $form->keyword,
			);

			if (!empty($sort) && $sort != 'popular') {
				$pages->params['sort'] = $sort;
			}

			$sphinx = new Sphinx;

			if ($sphinx->isActive()) {
				$total = $sphinx->searchTotal($this->per_page, $form->keyword);
				
				if ($total['total']) {
					$pages->setItemCount($total['total']);
					$offset = $pages->getOffset();
					$limit = $pages->getLimit();
	
					$product_ids = $sphinx->search($offset, $limit, $form->keyword, $sort);
					$products = Product::model()->getSearchProducts($product_ids);
				}
			} else {
				$products = Product::model()->getProductsByKeyword($form->keyword, $sort);
			}
		} else {
			$validation_errors = $form->jsonErrors(); 
			$errors = implode("<br>\n", $validation_errors['messages']);
		}

		if (!empty($errors)) {
			$products = Product::model()->getNewestProducts(0, 8);
			
			$this->render('searchEmpty', array(
				'keyword' => $form->keyword,
				'errors' => $errors,
				'products' => $products,
			));
		} elseif (empty($products)) {
			$products = Product::model()->getNewestProducts(0, 8);
			
			$this->render('searchEmpty', array(
				'keyword' => $form->keyword,
				'products' => $products,
			));
		} else {
			if (Yii::app()->request->isAjaxRequest) {
				$this->json = [
					'total' => $total['total'],
					'catalog' => $this->renderPartial('productsList', ['products' => $products], true),
					'pagination' => $this->renderPartial('pagination', ['pages' => $pages], true),
				];
			} else {
                $this->render('search', array(
                    'keyword' => $form->keyword,
                    // 'categories' => $categories,
                    // 'categories_tree' => $categories_tree,
                    'products' => $products,
                    'pages' => $pages,
                    'sort' => $sort,
                    'errors' => $errors,
                ));
            }
		}
	}

	/**
	 * Cart items.
	 */
	public function actionCart()
	{
		$cart_model = Cart::model();
		$action = Yii::app()->request->getPost('action');
		$cart = Yii::app()->request->getPost('cart');

		$categories = Yii::app()->params->categories;
		$box_banner = Banner::model()->getBoxBanner();
    	$postcard_banner = Banner::model()->getPostcardBanner();

		if ($action == 'comment') {
			$index = Yii::app()->request->getPost('index');
			$comment = Yii::app()->request->getPost('comment');
			$cart_model->addComment($index, $comment);
			
			return;
		} elseif (!empty($cart) && in_array($action, array('add', 'update', 'price', 'remove', 'add_custom_product'))) {
			$model = new CartForm($action);
			$model->attributes = $cart;

			if ($model->validate()) {
				if ($action == 'add_custom_product') {
					$cart_model->addCustomProduct($model);
				} else {
					$cart_model->$action($model);
				}

				if (Yii::app()->request->isAjaxRequest) {
					$cart = $cart_model->getCartProducts();
					$has_unavailable = $cart_model->hasUnavailableProducts($cart);
					$has_preorder = $cart_model->hasPreorderProducts($cart);
					
					$this->json['unavailable'] = $has_unavailable ? 'У вас в корзине товары, которых нет в наличии. Для оформления заказа удалите недоступный товар.' : false;
					$this->json['has_preorder'] = $has_preorder;
					$this->json['total'] = $cart_model->getTotal();
					$this->json['total_title'] = Yii::t('app', '{n} Товар|{n} Товара|{n} Товаров|{n} Товара', $cart_model->getTotal());
					$this->json['price'] = round($cart_model->getPrice())  . ' грн'; // number_format($cart_model->getPrice(), 0, '.', ' ');
					$this->json['btn'] = $cart_model->getTotal();

					if ($action == 'add') {
						$this->json['html'] = $this->renderPartial('cartList', array(
							'cart' => $cart,
							'total' => $this->json['total'],
							'price' => $this->json['price'],
							'box_banner' => $box_banner,
							'postcard_banner' => $postcard_banner,
						), true);
					} else {
						if ($this->json['total']) {
							$this->json['html'] = $this->renderPartial('cartList', array(
								'cart' => $cart,
								'total' => $this->json['total'],
								'price' => $this->json['price'],
								'box_banner' => $box_banner,
								'postcard_banner' => $postcard_banner,
							), true);
						} else {
							$this->json['html'] = '<p class="text-center">Ваша корзина пуста!</p>';
						}
					}

					// usleep(350000);

					return;
				} else {
					$this->redirect(array('cart'));
				}
			} else {
				if (Yii::app()->request->isAjaxRequest) {
					$validation_errors = $model->jsonErrors(); 
					$errors = implode("\n", $validation_errors['messages']);

					$this->json = array(
						'error' => true,
						'errorCode' => $errors,
						'errorFields' => array(),
					);

					return;
				}
			}
		}

		$this->pageTitle = Lang::t('seo.meta.titleCart');
		$this->noIndex = true;
		
		$cart = $cart_model->getCartProducts();
		$has_unavailable = $cart_model->hasUnavailableProducts($cart);

		$this->render('cart', array(
			'cart' => $cart,
			'has_unavailable' => $has_unavailable,
			'total' => $cart_model->getTotal(),
			'price' => $cart_model->getPrice($cart),
            'categories' => $categories,
			'box_banner' => $box_banner,
			'postcard_banner' => $postcard_banner,
		));
	}

	/**
	 * Checkout process.
	 */
	public function actionCheckout()
	{
		$cart_model = Cart::model();
		$cart = $cart_model->getCartProducts();
		$has_unavailable = $cart_model->hasUnavailableProducts($cart);
		$has_preorder = $cart_model->hasPreorderProducts($cart);
		
		if (empty($cart)) {
            if (Yii::app()->request->isAjaxRequest) {
                $this->json = [
                    'error' => true,
                    'errorCode' => Lang::t('checkout.tip.emptyCart'),
                    'errorFields' => [],
                ];
                return;
            } else {
				$this->redirect(array('index'));
			}
		}

		$order_model = Order::model();
		$order = Yii::app()->request->getPost('order');

		$discount = Discount::model()->getDiscount();

		if (!empty($order)) {
			if ($has_unavailable) {
				$this->json = [
					'error' => true,
					'errorCode' => Lang::t('checkout.tip.notAllowedProduct'),
					'errorFields' => [],
				];
				return;
			}
			
			$model = new OrderForm();
			$model->attributes = $order;

			if ($model->validate()) {
				$order_model->setUser($model);
				$order_model->setDelivery($model);
				$order_model->setPayment($model);

				// $delivery_data = json_decode(Yii::app()->params->settings['delivery'], true);

				$cart_price = $cart_model->getPrice($cart);
				$discount_value = Discount::getDiscountValue($cart_price);

				$order_id = $order_model->add($model, $cart_price, $cart, $discount, $discount_value);

				if ($order_id) {
					$cart_model->clear();
					$order_model->clear();
					Discount::model()->clear();

					$order = $order_model->getOrderById($order_id);
					$order_products = $order_model->getOrderProducts($order_id);

					(new Api)->createOrder($order, $order_products);

					$thank_you = [
						'order_id' => $order_id,
						'dataLayer' => $order_model->getDataLayer($order, $order_products),
					];

					if (Yii::app()->params->settings['stock'] == 'order') {
						// $order_model->subtractOrderQty($order_id);
					}

					// $cart_price += $order['delivery_price'];
					$cart_price += $discount_value;

					/* $subject = Lang::t('email.subject.newOrder', ['{order_id}' => $order['order_id']]);
					$body = Yii::app()->getController()->renderPartial('//email/order', [
						'order' => $order,
						'order_products' => $order_products,
					], true);

					// send email to admins
					$mailer = new EsputnikMailer();
					$emails = explode(', ', Yii::app()->params->settings['notify_mail']);
					$mailer->send($emails, $subject, $body);

					// send email to client
					$mailer->send([$order['email']], $subject, $body); */
					
					// online payment
					/* if ($model->payment == 2 && !empty($order) && $cart_price < Yii::app()->params->settings['max_order_amount']) {
						$payment = new Payment;
						$this->json['paymentForm'] = $payment->getPaymentFormHtml($order_id, $cart_price, $order, $order_products);
						return;
					} */

					$order_model->setThankYou($thank_you);
					$this->json['thankYou'] = $this->createUrl('thankyou');
				} else {
					$this->json = [
						'error' => true,
						'errorCode' => Lang::t('checkout.error.orderFailed'), // Lang::t('checkout.error.couldNotCreateOrder'),
						'errorFields' => [],
					];
				}
			} else {
				$errors = $model->jsonErrors();
				
				$order_model->setUser($model);

				if (!in_array('delivery', $errors['fields'])) {
					$order_model->setDelivery($model);
				}
				
				if (!in_array('payment', $errors['fields'])) {
					$order_model->setPayment($model);
				}
				
				$this->json = array(
					'error' => true,
					'errorCode' => '', // implode("<br>\n", $errors['messages']),
					'errorFields' => $errors['fields'],
				);
			}

			return;
		}

		$this->pageTitle = Lang::t('seo.meta.titleCheckout');
		$this->noIndex = true;

		$customer = $order_model->getUser();

		$np = new NovaPoshta();
		$np_cities = $np->getCities();
		$np_departments = array();

		if (empty($customer['np_city'])) {
			require_once Yii::getPathOfAlias('application.vendor.GeoIP2') . DS . 'autoload.php';

			$reader = new GeoIp2\Database\Reader(Yii::app()->assetManager->getBasePath() . DS . 'geolite2_city.mmdb');

            if (Yii::app()->params->dev) {
                $_SERVER['REMOTE_ADDR'] = '79.110.133.118';
			}
			
			try {
				if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
					$ip_address = $_SERVER['HTTP_CF_CONNECTING_IP'];
				} elseif (isset($_SERVER['REMOTE_ADDR'])) {
					$ip_address = $_SERVER['REMOTE_ADDR'];
				} else {
					$ip_address = '127.0.0.1';
				}

                $record = $reader->city($ip_address);
                // $record->country->isoCode
                // $country = $record->country->name;

                // print($record->country->isoCode . "\n"); // 'UA'
                // print($record->country->name . "\n"); // 'Ukraine'
                // print($record->country->names['ru'] . "\n"); // 'Украина'
				// var_dump($record->city->names);
				
                if ($record->city->name == 'Kyiv') {
                    $customer['np_city'] = 'Київ';
                } elseif ($record->city->name == 'Odesa') {
					$customer['np_city'] = 'Одеса';
                } elseif ($record->city->name == 'Kharkiv') {
					$customer['np_city'] = 'Харків';
                } elseif ($record->city->name == 'Lviv') {
					$customer['np_city'] = 'Львів';
                } elseif ($record->city->name == 'Dnipro') {
					$customer['np_city'] = 'Дніпро';
				}
            } catch (Exception $e) {
                // GeoIp2\Exception\AddressNotFoundException
                // InvalidArgumentException
            }
		}
		
		if (!empty($customer['np_city'])) {
			$np_city = $np->getCity($customer['np_city']);
			
			if (!empty($np_city)) {
				$np_departments = $np->getDepartments($np_city['city_ref']);
			}
		}

        $categories = Yii::app()->params->categories;
		$box_banner = Banner::model()->getBoxBanner();
    	$postcard_banner = Banner::model()->getPostcardBanner();

		$this->render('checkout', array(
			'cart' => $cart,
			'has_unavailable' => $has_unavailable,
			'has_preorder' => $has_preorder,
			'discount' => $discount,
			'total' => $cart_model->getTotal(),
			'price' => $cart_model->getPrice(),
			'payment' => $order_model->getPayment(),
			'delivery' => $order_model->getDelivery(),
			'customer' => $customer,
			'np_cities' => $np_cities,
			'np_departments' => $np_departments,
			'categories' => $categories,
			'box_banner' => $box_banner,
			'postcard_banner' => $postcard_banner,
		));
	}

	/**
	 * Thank you page.
	 */
	public function actionThankYou()
	{
		$order_model = Order::model();
		$thank_you = $order_model->getThankYou();

		if ($thank_you === false) {
			$this->redirect(array('index'));
		}

		$payment_status = null;

		if (isset($thank_you['payment_status'])) {
			$payment_status = ($thank_you['payment_status'] == 'success') ? true : false;
		}

		$thank_you['order'] = $order_model->getOrderById($thank_you['order_id']);
		$this->dataLayer = $thank_you['dataLayer'];

		$this->pageTitle = Lang::t('seo.meta.titleThankYou');
		$this->render('thankYou', $thank_you);
	}

	/**
	 * School.
	 */
	public function actionSchool()
	{
		$this->pageTitle = Lang::t('seo.meta.titleSchool');
		$this->pageDescription = Lang::t('seo.meta.descriptionSchool');
		$this->canonicalUrl = $this->createAbsoluteUrl('school');

		$course_model = Course::model();
		$events = $course_model->getEvents();
		$courses = $course_model->getCourses();
		$authors = Author::model()->getAuthors();

		$this->breadcrumbs = [
            Lang::t('seo.meta.titleSchool') => $this->canonicalUrl,
        ];

		$this->render('school', [
			'events' => $events,
			'courses' => $courses,
			'authors' => $authors,
		]);
	}

	/**
	 * Get course data and render it.
	 * 
	 * @param string $alias course alias identifier.
	 * @throws CHttpException 404 error code when a course is not found in DB.
	 */
	public function actionCourse($alias)
	{
		$course = Course::model()->getCourseByAlias($alias);
		
        if (empty($course)) {
			throw new CHttpException(404, 'Page not found');
		}
		
		$this->pageTitle = !empty($course['course_meta_title']) ? $course['course_meta_title'] : $course['course_name'];
		$this->pageDescription = $course['course_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('course', ['alias' => $course['course_alias']]);

		if (!empty($course['course_no_index'])) {
			$this->noIndex = true;
		}

		if (!empty($course['course_image'])) {
			$course_image = json_decode($course['course_image'], true);
			$this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/course/' . $course['course_id'] . '/' . $course_image['details']['1x']['path'];
		}

		$this->breadcrumbs = [
            Lang::t('seo.meta.titleSchool') => $this->createAbsoluteUrl('site/school'),
		    $course['course_name'] => $this->canonicalUrl,
        ];

		$this->render('course', [
			'course' => $course,
		]);
	}

	/**
	 * Get news articles.
	 */
	public function actionNews()
	{
		$this->pageTitle = Lang::t('seo.meta.titleNews');
		$this->pageDescription = Lang::t('seo.meta.descriptionNews');
		$this->canonicalUrl = $this->createAbsoluteUrl('news');

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleNews') => $this->canonicalUrl,
		];

		$categories = BlogCategory::model()->getBlogCategoriesList();
		$news = Blog::model()->getBlogs();

		$this->render('news', array(
			'categories' => $categories,
			'news' => $news,
		));
	}
	
	/**
	 * Get news articles by category.
	 * 
	 * @param string $alias news category alias identifier.
	 * @throws CHttpException 404 error code when a category is not found in DB.
	 */
	public function actionNewsCategory($alias)
	{
		// get category
		$category = BlogCategory::model()->getBlogCategoryByAlias($alias);
		
		if (empty($category)) {
			throw new CHttpException(404, 'Page not found');
		}

		$this->pageTitle = !empty($category['category_meta_title']) ? $category['category_meta_title'] : $category['category_name'] . ' — ' . Lang::t('seo.meta.titleNews');
		$this->pageDescription = $category['category_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('newscategory', array('alias' => $category['category_alias']));

		if (!empty($category['category_no_index'])) {
			$this->noIndex = true;
		}

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleNews') => $this->createAbsoluteUrl('site/news'),
			$category['category_name'] => $this->canonicalUrl,
		];

		$categories = BlogCategory::model()->getBlogCategoriesList();
		$news = Blog::model()->getCategoryBlogs($category['category_id']);

		$this->render('news', array(
			'category' => $category,
			'categories' => $categories,
			'news' => $news,
		));
	}

	/**
	 * Get blog article data and render it.
	 * 
	 * @param string $alias blog article alias identifier.
	 * @throws CHttpException 404 error code when an article is not found in DB.
	 */
	public function actionNewsArticle($category_alias, $alias)
	{
		$news = Blog::model()->getBlogByAlias($alias);
		
        if (empty($news)) {
			throw new CHttpException(404, 'Page not found');
		}
		
		// get category
		$category = BlogCategory::model()->getBlogCategoryByAlias($category_alias);

		if (empty($category) || $news['category_alias'] != $category['category_alias']) {
			$this->redirect(['site/newsarticle', 'category_alias' => $news['category_alias'], 'alias' => $news['blog_alias']], true, 301);
		}
		
		$this->pageTitle = !empty($news['blog_meta_title']) ? $news['blog_meta_title'] : $news['blog_title'];
		$this->pageDescription = $news['blog_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('newsarticle', ['category_alias' => $news['category_alias'], 'alias' => $news['blog_alias']]);

		if (!empty($news['blog_no_index'])) {
			$this->noIndex = true;
		}

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleNews') => $this->createAbsoluteUrl('site/news'),
			$category['category_name'] => $this->createAbsoluteUrl('site/newscategory', ['alias' => $category['category_alias']]),
			$news['blog_title'] => $this->canonicalUrl,
		];

		if (!empty($news['blog_photo'])) {
			$news_image = json_decode($news['blog_photo'], true);
			$this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/blog/' . $news['blog_id'] . '/' . $news_image['details']['1x']['path'];
		}

		$this->render('newsArticle', array(
			'blog' => $news,
		));
	}

	/**
	 * Get base articles.
	 */
	public function actionBase()
	{
		$this->pageTitle = Lang::t('seo.meta.titleBase');
		$this->pageDescription = Lang::t('seo.meta.descriptionBase');
		$this->canonicalUrl = $this->createAbsoluteUrl('base');

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleBase') => $this->canonicalUrl,
		];

		$categories = BaseCategory::model()->getBaseCategories();

		$this->render('base', array(
			'categories' => $categories,
		));
	}
	
	/**
	 * Get base articles by category.
	 * 
	 * @param string $alias base category alias identifier.
	 * @throws CHttpException 404 error code when a category is not found in DB.
	 */
	public function actionBaseCategory($alias)
	{
		// get category
		$category = BaseCategory::model()->getBaseCategoryByAlias($alias);
		
		if (empty($category)) {
			throw new CHttpException(404, 'Page not found');
		}

		$this->pageTitle = !empty($category['category_meta_title']) ? $category['category_meta_title'] : $category['category_name'] . ' — ' . Lang::t('seo.meta.titleBase');
		$this->pageDescription = $category['category_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('basecategory', array('alias' => $category['category_alias']));

		if (!empty($category['category_no_index'])) {
			$this->noIndex = true;
		}

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleBase') => $this->createAbsoluteUrl('site/base'),
			$category['category_name'] => $this->canonicalUrl,
		];

		$this->render('baseCategory', array(
			'category' => $category,
		));
	}

	/**
	 * Get base article data and render it.
	 * 
	 * @param string $alias base article alias identifier.
	 * @throws CHttpException 404 error code when an article is not found in DB.
	 */
	public function actionBaseArticle($category_alias, $alias)
	{
		$base = Base::model()->getBaseByAlias($alias);
		
        if (empty($base)) {
			throw new CHttpException(404, 'Page not found');
		}
		
		// get category
		$category = BaseCategory::model()->getBaseCategoryByAlias($category_alias);

		if (empty($category) || $base['category_alias'] != $category['category_alias']) {
			$this->redirect(['site/basearticle', 'category_alias' => $base['category_alias'], 'alias' => $base['base_alias']], true, 301);
		}
		
		$this->pageTitle = !empty($base['base_meta_title']) ? $base['base_meta_title'] : $base['base_title'];
		$this->pageDescription = $base['base_meta_description'];
		$this->canonicalUrl = $this->createAbsoluteUrl('basearticle', ['category_alias' => $base['category_alias'], 'alias' => $base['base_alias']]);

		if (!empty($base['base_no_index'])) {
			$this->noIndex = true;
		}

		$this->breadcrumbs = [
			Lang::t('seo.meta.titleBase') => $this->createAbsoluteUrl('site/base'),
			$category['category_name'] => $this->createAbsoluteUrl('site/basecategory', ['alias' => $category['category_alias']]),
			$base['base_title'] => $this->canonicalUrl,
		];

		if (!empty($base['base_photo'])) {
			$base_image = json_decode($base['base_photo'], true);
			$this->ogImage = Yii::app()->getBaseUrl(true) . Yii::app()->assetManager->getBaseUrl() . '/base/' . $base['base_id'] . '/' . $base_image['details']['1x']['path'];
		}

		$this->render('baseArticle', array(
			'base' => $base,
		));
	}

	/**
	 * Get page data and render it.
	 * 
	 * @param string $alias page alias identifier.
	 * @throws CHttpException 404 error code when the page is not found in DB.
	 */
	public function actionPage($alias)
	{
		// get page
		$page = Page::model()->getPage($alias);
		
		if (!empty($page)) {
			$this->pageTitle = !empty($page['page_meta_description']) ? $page['page_meta_description'] : $page['page_title'];
			$this->pageDescription = $page['page_meta_description'];
			$this->canonicalUrl = $this->createAbsoluteUrl('page', array('alias' => $page['page_alias']));

			if (!empty($page['page_no_index'])) {
				$this->noIndex = true;
			}

			$this->breadcrumbs = [
				$page['page_title'] => $this->canonicalUrl,
			];

			$pages = Page::model()->getTopMenu();

			if ($page['page_type'] == 'about') {
				$this->render('about', [
					'page' => $page,
					'pages' => $pages,
				]);
			} else {
				$this->render('page', [
					'page' => $page,
					'pages' => $pages,
				]);
			}
		} else {
			throw new CHttpException(404, 'Page not found');
		}
	}
	
	public function actionWishlist()
	{
		$this->redirect(['site/login']);
	}

	/**
	 * Account dashboard.
	 */
	public function actionAccount()
	{
		$this->pageTitle = Lang::t('account.title.accountTitle');
		$this->noIndex = true;
		$this->render('account');
	}

    public function actionAccountEdit()
    {
        $action = Yii::app()->request->getPost('action');
        $user = Yii::app()->request->getPost('user');

        if (!empty($user) && in_array($action, array('personal', 'password')) && Yii::app()->request->isAjaxRequest) {
            $model = new AccountForm($action);
            $model->attributes = $user;

            if ($model->validate()) {
                $user_model = User::model();

                if ($action == 'personal') {
                    $rs = $user_model->save($model);

                    if ($rs) {
                        $user_data = $user_model->getUser();

                        $this->json = array(
                            'type' => 'personal',
                            'html' => $this->renderPartial('accountPersonal', array('user' => $user_data), true),
                            'form_html' => $this->renderPartial('accountPersonalForm', array('user' => $user_data), true),
                        );
                    }
                } elseif ($action == 'password') {
                    $rs = $user_model->setPassword($model);

                    if ($rs) {
                        Yii::app()->user->logout(false);
                        $user = $model->getUser();

                        $identity = new UserIdentity($user['user_email'], $model->password);

                        if ($identity->authenticate()) {
                            // 30 days
                            $duration = 3600 * 24 * 30;
                            Yii::app()->user->login($identity, $duration);

                            $this->json = array(
                                'type' => 'password',
                                'msg' => Lang::t('account.tip.passwordChanged'),
                            );
                        } else {
                            $this->json = array(
                                'error' => true,
                                'errorCode' => $identity->errorMessage,
                                'errorFields' => array(),
                            );
                        }
                    }
                }

                if (!$rs) {
                    $this->json = array(
                        'error' => true,
                        'errorCode' => Lang::t('account.error.errorOccured'),
                        'errorFields' => array(),
                    );
                }
            } else {
                $errors = $model->jsonErrors();
                $this->json = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }

            return;
        }

        $this->pageTitle = Lang::t('account.title.accountTitle');
        $this->noIndex = true;

        $account = array(
            'orders' => Order::model()->getUserOrders(6),
            'total' => Order::model()->getUserTotalAmount(),
            'user' => Yii::app()->user->user_data,
        );

        $this->render('accountEdit', $account);
    }

	/**
	 * Account orders.
	 */
	public function actionOrders()
	{
		$this->pageTitle = Lang::t('account.title.ordersHistory');
		$this->noIndex = true;

		$account = array(
			'orders' => Order::model()->getUserOrders(Yii::app()->user->id),
			'total' => Order::model()->getUserTotalAmount(),
			'user' => Yii::app()->user->user_data,
		);

		$this->render('accountOrders', $account);
	}

	/**
	 * Customer order details.
	 */
	public function actionOrder($order_id)
	{
		$order = Order::model()->getUserOrderById($order_id);

		if (empty($order)) {
			throw new CHttpException(404, 'Page not found');
		}

		$products = Order::model()->getUserOrderProducts($order_id);

		$this->pageTitle = Lang::t('accountOrder.meta.title', array('{order_id}' => $order_id));
		$this->noIndex = true;

		$this->render('accountOrder', array(
			'order' => $order,
			'products' => $products,
		));
	}

	/**
	 * Generate, download, send order invoice.
	 */
	public function actionInvoice($order_id)
	{
		$action = Yii::app()->request->getPost('action');
		$email = Yii::app()->request->getPost('email');
		$message = Yii::app()->request->getPost('message');

		$order_model = Order::model();
		$order = $order_model->getUserOrderById($order_id);

		if (empty($order)) {
			throw new CHttpException(404, 'Page not found');
		}

		$pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'order' . DS . $order['order_id'];

		if (!empty($order['invoice_pdf']) && is_file($pdf_path . DS . $order['invoice_pdf'])) {
			$pdf_file = $order['invoice_pdf'];
			$invoice_pdf = $pdf_path . DS . $pdf_file;
		} else {
			// generate PDF file
			$products = $order_model->getUserOrderProducts($order_id);
			$pdf_file = $order_model->generateInvoice($order, $products);

			if ($pdf_file !== false) {
				$invoice_pdf = $pdf_path . DS . $pdf_file;	
			}
		}

		if (empty($invoice_pdf)) {
			throw new CHttpException(404, 'Page not found');
		}

		if ($action == 'send' && Yii::app()->request->isAjaxRequest) {
			if (!empty($email) && preg_match('/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i', $email)) {
				// send email
				$body = trim(trim($message) . "\n\n" . Lang::t('pdf.tip.invoiceAttachment'));
				$body = str_replace("\n", "<br>\n", $body);

				$mailer = new SendGridMailer();
				$emails = $mailer->prepareEmailsFromString($email);
				$mailer->send($emails, Lang::t('pdf.tip.emailSubject') . $id, $body, array(
					array(
						'name' => $pdf_file,
						'file' => $invoice_pdf,
					),
				));

				$this->json = array(
					'message' => Lang::t('pdf.tip.invoiceSent'),
				);
			} else {
				$this->json = array(
					'error' => true,
					'errorMessage' => Lang::t('pdf.error.invalidEmail'),
				);
			}
		} else {
			// send file
			Yii::app()->request->sendFile($pdf_file, file_get_contents($invoice_pdf));
		}
	}

	/**
	 * Account offers.
	 */
	public function actionOffers()
	{
		$this->pageTitle = Lang::t('account.title.offersHistory');
		$this->noIndex = true;

		$account = array(
			'offers' => Order::model()->getUserOffers(),
			'total' => Order::model()->getUserTotalAmount(),
			'user' => Yii::app()->user->user_data,
		);

		$this->render('accountOffers', $account);
	}

	/**
	 * Customer offer details.
	 */
	public function actionOffer($offer_id)
	{
		$offer = Order::model()->getUserOfferById($offer_id);

		if (empty($offer)) {
			throw new CHttpException(404, 'Page not found');
		}

		$products = Order::model()->getUserOfferProducts($offer_id);

		$this->pageTitle = Lang::t('accountOffer.meta.title', array('{offer_id}' => $offer_id));
		$this->noIndex = true;

		$this->render('accountOffer', array(
			'offer' => $offer,
			'products' => $products,
		));
	}

	/**
	 * Generate, download, send offer invoice.
	 */
	public function actionInvoiceOffer($offer_id)
	{
		$action = Yii::app()->request->getPost('action');
		$email = Yii::app()->request->getPost('email');
		$message = Yii::app()->request->getPost('message');

		$order_model = Order::model();
		$offer = $order_model->getUserOfferById($offer_id);

		if (empty($offer)) {
			throw new CHttpException(404, 'Page not found');
		}

		$pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'offer' . DS . $offer['offer_id'];

		if (!empty($offer['offer_pdf']) && is_file($pdf_path . DS . $offer['offer_pdf'])) {
			$pdf_file = $offer['offer_pdf'];
			$offer_pdf = $pdf_path . DS . $pdf_file;
		} else {
			// generate PDF file
			$products = $order_model->getUserOfferProducts($offer_id);
			
			if (!empty($offer['user_id'])) {
				$seller = User::model()->getUserById($offer['user_id']);
			} else {
				$seller = array();
			}

			$pdf_file = $order_model->generateOffer($offer, $products, $seller);

			if ($pdf_file !== false) {
				$offer_pdf = $pdf_path . DS . $pdf_file;	
			}
		}

		if (empty($offer_pdf)) {
			throw new CHttpException(404, 'Page not found');
		}

		if ($action == 'send' && Yii::app()->request->isAjaxRequest) {
			if (!empty($email) && preg_match('/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i', $email)) {
				// send email
				$mailer = new SendGridMailer();
				$emails = $mailer->prepareEmailsFromString($email);
				
				if (!empty($message)) {
					$body = trim(trim($message) . "\n\n" . Lang::t('pdf.tip.invoiceAttachment'));
					$body = '<p>' . str_replace("\n", "<br>\n", $body) . '</p>';
				} else {
					$body = Lang::t('pdf.title.offerEmail');
				}
				
				$mailer->send($emails, Lang::t('pdf.title.offer', array('{offer_id}' => $offer['offer_id'])), $body, array(
					array(
						'name' => $pdf_file,
						'file' => $offer_pdf,
					),
				));

				$this->json = array(
					'message' => Lang::t('pdf.tip.invoiceSent'),
				);
			} else {
				$this->json = array(
					'error' => true,
					'errorMessage' => Lang::t('pdf.error.invalidEmail'),
				);
			}
		} else {
			// send file
			Yii::app()->request->sendFile($pdf_file, file_get_contents($offer_pdf));
		}
	}

	/**
	 * Login action.
	 */
	public function actionLogin()
	{
		$this->pageTitle = Lang::t('login.meta.title');
		$this->pageDescription = '';

		$this->noIndex = true;

		$model = new LoginForm;
		$login = Yii::app()->request->getPost('login');

		$result = array();

		if (!empty($login)) {
			$model->attributes = $login;
			
			if ($model->validate()) {
				$identity = new UserIdentity($model->login, $model->password);
				
				if ($identity->authenticate()) {
					// session duration is 30 days
					$duration = 3600 * 24 * 30;
					Yii::app()->user->login($identity, $duration);
					
					$redirect_url = $this->createUrl('site/account');

					if (Yii::app()->request->isAjaxRequest) {
					    $user = User::model()->getUser();
						$result = array(
						    'success' => true,
							'msg' => Lang::t('login.tip.loggedIn'),
							'redirect' => $redirect_url,
                            'user' => [
                                'id'    => $user['user_id'],
                                'email' => $user['user_email'],
                                'phone' => $user['user_phone'],
                                'first_name' => $user['first_name'],
                                'last_name'  => $user['last_name'],
                            ]
						);
					} else {
						$this->redirect($redirect_url);
					}
				} else {
					$model->addError('login', '');
					$model->addError('password', '');

					$errors = $model->jsonErrors();
					$result = array(
						'error' => true,
						'errorCode' => $identity->errorMessage,
						'errorFields' => $errors['fields'],
					);
				}
			} else {
				$errors = $model->jsonErrors();
				$result = array(
					'error' => true,
					'errorCode' => '', // implode("<br>\n", $errors['messages']),
					'errorFields' => $errors['fields'],
				);
			}
		}

		if (Yii::app()->request->isAjaxRequest) {
			$this->json = $result;
		} else {
			$this->render('login', array('model' => $model, 'result' => $result));
		}
	}

    /**
     * Registration action.
     */
    public function actionRegistration()
    {
        $this->pageTitle = Lang::t('registration.meta.title');
        $model = new RegistrationForm;
        $registration = Yii::app()->request->getPost('registration');

        $result = array();
        if (!empty($registration)) {
            $model->attributes = $registration;

            if ($model->validate()) {
                // add user
                $user_id = User::model()->add($model);
                if (!$user_id) {
                    $result = array(
                        'error' => true,
                        'errorCode' => 'Ошибка при регистрации. Свяжитесь с администрацией для решения проблемы.',
                        'errorFields' => array(),
                    );
                } else {
                    $identity = new UserIdentity($model->email, $model->password);

                    if ($identity->authenticate()) {
                        // session duration is 30 days
                        $duration = 3600 * 24 * 30;
                        Yii::app()->user->login($identity, $duration);
                        Yii::app()->request->redirect($this->createUrl('site/account'));
                    } else {
                        $errors = $model->jsonErrors();
                        $result = array(
                            'error'       => true,
                            'errorCode'   => $identity->errorMessage,
                            'errorFields' => $errors['fields'],
                        );
                    }
                }
            } else {
                $errors = $model->jsonErrors();
                $result = array(
                    'error' => true,
                    'errorCode' => '', // implode("<br>\n", $errors['messages']),
                    'errorFields' => $errors['fields'],
                );
            }
        }

        $this->render('registration', array('data' => $registration, 'result' => $result));
    }

	/**
	 * Password reset action.
	 */
	public function actionReset()
	{
		$this->pageTitle = Lang::t('resetPassword.meta.title');
		$this->pageDescription = '';

		$this->noIndex = true;
		
		$model = new ResetForm;
		$reset = Yii::app()->request->getPost('reset');
		$newpassword = Yii::app()->request->getPost('newpassword');
		
		$result = array();

		if (!empty($reset)) {
			$model->scenario = 'reset';
			$model->attributes = $reset;
			
			if ($model->validate()) {
				User::model()->setResetToken($model);
				
				if (Yii::app()->request->isAjaxRequest) {
					$result = array(
						'msg' => Lang::t('resetPassword.tip.sendInstructions'),
					);
				} else {
					Yii::app()->user->setFlash('reset', Lang::t('resetPassword.tip.sendInstructions'));
					$this->redirect(array('site/reset'));
				}
			} else {
				$errors = $model->jsonErrors();
				$result = array(
					'error' => true,
					'errorCode' => '', // implode("<br>\n", $errors['messages']),
					'errorFields' => $errors['fields'],
				);
			}
		} elseif (!empty($newpassword)) {
			$model->scenario = 'newpassword';
			$model->attributes = $newpassword;

			if ($model->validate()) {
				$rs = User::model()->setPassword($model, true);

				if ($rs) {
					$user = $model->getUser();
					$identity = new UserIdentity($user['user_email'], $model->password);
				
					if ($identity->authenticate()) {
						// session duration is 30 days
						$duration = 3600 * 24 * 30;
						Yii::app()->user->login($identity, $duration);

						$redirect_url = $this->createUrl('site/account');
					} else {
						Yii::app()->user->setFlash('login_error', $identity->errorMessage);
						$redirect_url = $this->createUrl('site/login');
					}

					if (Yii::app()->request->isAjaxRequest) {
						$result = array(
							'redirect' => $redirect_url,
						);
					} else {
						$this->redirect($redirect_url);
					}
				} else {
					$result = array(
						'error' => true,
						'errorCode' => Lang::t('resetPassword.error.passwordResetError'),
						'errorFields' => array(),
					);
				}
			} else {
				$errors = $model->jsonErrors();
				$result = array(
					'error' => true,
					'errorCode' => '', // implode("<br>\n", $errors['messages']),
					'errorFields' => $errors['fields'],
				);
			}
		}
		
		if (Yii::app()->request->isAjaxRequest) {
			$this->json = $result;
		} else {
			$template = 'resetPassword';

			$uid = Yii::app()->request->getQuery('uid');
			$token = Yii::app()->request->getQuery('token');
			
			if (!empty($uid) || !empty($token)) {
				$user = User::model()->getUserById($uid);
				
				if (!empty($user) && $user['user_reset_token'] == $token) {
					$template = 'newPassword';

					$result['uid'] = $uid;
					$result['token'] = $token;
				} else {
					Yii::app()->user->setFlash('reset_error', 'Invalid or expired reset link! Submit form again to reset password.');
					$this->redirect(array('site/reset'));
				}
			}

			$this->render($template, array('model' => $model, 'result' => $result));
		}
	}

	/**
	 * Logs out user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 * Sitemap.
	 */
	public function actionSitemap()
	{
		$this->pageTitle = 'Карта сайта';
		$this->pageDescription = '';

		$categories = Yii::app()->params->categories;
		$brands = Brand::model()->getBrandsSitemap();
		$products = Product::model()->getProductsSitemap();
		$pages = Page::model()->getPagesSitemap();

		$this->render('sitemap', array(
			'categories' => $categories,
			'brands' => $brands,
			'products' => $products,
			'pages' => $pages,
		));
	}

	/**
	 * Sitemap XML.
	 */
	public function actionSitemapXml()
	{
		if (Yii::app()->language != 'uk') {
			throw new CHttpException(404, 'Page not found');
		}
		
		// отключаем web-логи для ajax запросов
		foreach (Yii::app()->log->routes as $route) {
			if ($route instanceof CWebLogRoute || $route instanceof CProfileLogRoute) {
				$route->enabled = false;
			}
		}
		
		$date = date('c');
		$base_url = Yii::app()->getBaseUrl(true);
		
		$categories = Yii::app()->params->categories;
		// $brands = Brand::model()->getBrandsSitemap();
		// $products = Product::model()->getProductsSitemap();
		$pages = Page::model()->getPagesSitemap();

		// build xml output
		header('Content-type: text/xml');

		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->formatOutput = true;

		$urlset_el = $dom->createElement('urlset');
		$urlset_el->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		$urlset_el->setAttribute('xhtml', 'http://www.w3.org/1999/xhtml');
		$urlset_el->setAttribute('schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
		$dom->appendChild($urlset_el);

		$url_el = $dom->createElement('url');
		$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/index'));
		$url_el->appendChild($loc_el);
		$priority_el = $dom->createElement('priority', '1.0');
		$url_el->appendChild($priority_el);
		$changefreq_el = $dom->createElement('changefreq', 'daily');
		$url_el->appendChild($changefreq_el);
		$lastmod_el = $dom->createElement('lastmod', $date);
		$url_el->appendChild($lastmod_el);
		
		/* $alternate_el = $dom->createElement('xhtml:link');
		$alternate_el->setAttribute('rel', 'alternate');
		$alternate_el->setAttribute('hreflang', 'uk');
		$alternate_el->setAttribute('href', $this->createAbsoluteUrl('site/index'));
		$url_el->appendChild($alternate_el);

		$alternate_el = $dom->createElement('xhtml:link');
		$alternate_el->setAttribute('rel', 'alternate');
		$alternate_el->setAttribute('hreflang', 'ru');
		$alternate_el->setAttribute('href', $base_url . '/ru' . $this->createUrl('site/index'));
		$url_el->appendChild($alternate_el);
		
		$alternate_el = $dom->createElement('xhtml:link');
		$alternate_el->setAttribute('rel', 'alternate');
		$alternate_el->setAttribute('hreflang', 'en');
		$alternate_el->setAttribute('href', $base_url . '/en' . $this->createUrl('site/index'));
		$url_el->appendChild($alternate_el); */
		
		$urlset_el->appendChild($url_el);

		/* $url_el = $dom->createElement('url');
		$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/newest'));
		$url_el->appendChild($loc_el);
		$priority_el = $dom->createElement('priority', '0.9');
		$url_el->appendChild($priority_el);
		$changefreq_el = $dom->createElement('changefreq', 'weekly');
		$url_el->appendChild($changefreq_el);
		$lastmod_el = $dom->createElement('lastmod', $date);
		$url_el->appendChild($lastmod_el);
		$urlset_el->appendChild($url_el); */

		if (!empty($categories)) {
			foreach ($categories as $category) {
				$url_el = $dom->createElement('url');
				$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/category', array('alias' => $category['category_alias'])));
				$url_el->appendChild($loc_el);
				$priority_el = $dom->createElement('priority', '0.9');
				$url_el->appendChild($priority_el);
				$changefreq_el = $dom->createElement('changefreq', 'daily');
				$url_el->appendChild($changefreq_el);
				$lastmod_el = $dom->createElement('lastmod', $date);
				$url_el->appendChild($lastmod_el);

				/* $alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'uk');
				$alternate_el->setAttribute('href', $this->createAbsoluteUrl('site/category', array('alias' => $category['category_alias'])));
				$url_el->appendChild($alternate_el);

				$alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'ru');
				$alternate_el->setAttribute('href', $base_url . '/ru' . $this->createUrl('site/category', array('alias' => $category['category_alias'])));
				$url_el->appendChild($alternate_el);
				
				$alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'en');
				$alternate_el->setAttribute('href', $base_url . '/en' . $this->createUrl('site/category', array('alias' => $category['category_alias'])));
				$url_el->appendChild($alternate_el); */

				$urlset_el->appendChild($url_el);
			}
		}

		/* $url_el = $dom->createElement('url');
		$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/brands'));
		$url_el->appendChild($loc_el);
		$priority_el = $dom->createElement('priority', '0.9');
		$url_el->appendChild($priority_el);
		$changefreq_el = $dom->createElement('changefreq', 'weekly');
		$url_el->appendChild($changefreq_el);
		$lastmod_el = $dom->createElement('lastmod', $date);
		$url_el->appendChild($lastmod_el);
		$urlset_el->appendChild($url_el);

		if (!empty($brands)) {
			foreach ($brands as $brand) {
				$url_el = $dom->createElement('url');
				$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/brand', array('alias' => $brand['brand_alias'])));
				$url_el->appendChild($loc_el);
				$priority_el = $dom->createElement('priority', '0.9');
				$url_el->appendChild($priority_el);
				$changefreq_el = $dom->createElement('changefreq', 'weekly');
				$url_el->appendChild($changefreq_el);
				$lastmod_el = $dom->createElement('lastmod', $date);
				$url_el->appendChild($lastmod_el);
				$urlset_el->appendChild($url_el);
			}
		} */

		/* if (!empty($products)) {
			foreach ($products as $product) {
				$url_el = $dom->createElement('url');
				$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/product', array('alias' => $product['product_alias'])));
				$url_el->appendChild($loc_el);
				$priority_el = $dom->createElement('priority', '0.8');
				$url_el->appendChild($priority_el);
				$changefreq_el = $dom->createElement('changefreq', 'weekly');
				$url_el->appendChild($changefreq_el);
				$lastmod_el = $dom->createElement('lastmod', $date);
				$url_el->appendChild($lastmod_el);
				$urlset_el->appendChild($url_el);
			}
		} */
		
		if (!empty($pages)) {
			foreach ($pages as $page) {
				$url_el = $dom->createElement('url');
				$loc_el = $dom->createElement('loc', $this->createAbsoluteUrl('site/page', array('alias' => $page['page_alias'])));
				$url_el->appendChild($loc_el);
				$priority_el = $dom->createElement('priority', '0.5');
				$url_el->appendChild($priority_el);
				$changefreq_el = $dom->createElement('changefreq', 'weekly');
				$url_el->appendChild($changefreq_el);
				$lastmod_el = $dom->createElement('lastmod', $date);
				$url_el->appendChild($lastmod_el);

				/* $alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'uk');
				$alternate_el->setAttribute('href', $this->createAbsoluteUrl('site/page', array('alias' => $page['page_alias'])));
				$url_el->appendChild($alternate_el);

				$alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'ru');
				$alternate_el->setAttribute('href', $base_url . '/ru' . $this->createUrl('site/page', array('alias' => $page['page_alias'])));
				$url_el->appendChild($alternate_el);
				
				$alternate_el = $dom->createElement('xhtml:link');
				$alternate_el->setAttribute('rel', 'alternate');
				$alternate_el->setAttribute('hreflang', 'en');
				$alternate_el->setAttribute('href', $base_url . '/en' . $this->createUrl('site/page', array('alias' => $page['page_alias'])));
				$url_el->appendChild($alternate_el); */

				$urlset_el->appendChild($url_el);
			}
		}

		echo $dom->saveXML();
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			/* if ($error['code'] == 404) {
				$request_uri = Yii::app()->request->getRequestUri();
				$base_url = Yii::app()->request->getBaseUrl();
				
				if (!empty($base_url)) {
					$clean_uri = preg_replace('#^' . preg_quote($base_url, '#') . '#ui', '', $request_uri);
				} else {
					$clean_uri = $request_uri;
				}

				if ($clean_uri != '/404') {
					$this->redirect('/404', true, 301);
				}
			} */
			
			if (Yii::app()->request->isAjaxRequest) {
				$this->json = array(
					'error' => true,
					'errorCode' => $error['message'],
					'errorFields' => array(),
				);
			} else {
				if ($error['code'] == 404) {
					$error['products'] = Product::model()->getNewestProducts(0, 8);
					$this->render('//site/error404', $error);
				} else {
					$this->render('//site/error', $error);
				}
			}
		}
	}

	/**
	 * This method is invoked right after an action is executed.
	 */
	protected function afterAction($action)
	{
		if (Yii::app()->request->isAjaxRequest) {
			header('Vary: Accept');
			 
			if (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false) {
				header('Content-type: application/json; charset=utf-8');
			}

			if (isset($this->json['title'])) {
				$this->json['title'] .=  ' | ' . Yii::app()->name;
			}

			echo json_encode($this->json);
			Yii::app()->end();
		}
	}
}