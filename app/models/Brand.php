<?php
class Brand extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getBrandsList()
	{
		$brands = [];
		
		$brands_list = Yii::app()->db
			->createCommand("SELECT b.*, bl.brand_name 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY bl.brand_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($brands_list as $brand) {
			$brand_id = $brand['brand_id'];
			$brands[$brand_id] = $brand;
		}
			
		return $brands;
	}

	public function getBrands()
	{
		$brands = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY bl.brand_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $brands;
	}

	public function getBrandById($brand_id)
	{
		$brand = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE b.active = 1 AND b.brand_id = :brand_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
			->queryRow();
			
		return $brand;
    }
	
	public function getBrandByAlias($alias)
	{
		$brand = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE b.active = 1 AND b.brand_alias = :alias")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();
			
		return $brand;
	}
	
	public function getBrandsSitemap()
	{
		$brands = Yii::app()->db
			->createCommand("SELECT b.brand_id, b.brand_alias, bl.brand_name, bl.brand_no_index 
							 FROM brand as b 
							 JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY bl.brand_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $brands;
	}
}