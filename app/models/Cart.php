<?php
class Cart extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function initCart()
	{
		if (!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = array(
				'total' => 0,
				'price' => 0,
				'items' => array(),
			);
		}
	}

	public function getTotal()
	{
		return $_SESSION['cart']['total'];
	}
	
	public function getPrice()
	{
		return $_SESSION['cart']['price'];
	}

	public function getCartWeight()
	{
		$cart_items = $this->getCartProducts();
		$weight = 0;
		
		foreach ($cart_items as $cart_item) {
			if (!empty($cart_item['variant'])) {
				$product_weight = (float) $cart_item['variant']['variant_weight'];
			} else {
				$product_weight = (float) $cart_item['data']['product_weight'];
			}

			$product_weight = !empty($product_weight) ? $product_weight : 0.5;

			$weight += $product_weight * $cart_item['qty'];
		}
		
		return $weight;
	}

	public function getCurrencyPrice($cart)
	{
		$total_price = 0;

		foreach ($cart as $cart_index => $cart_item) {
			$total_price += Currency::getValue($cart_item['price']) * $cart_item['qty'];
		}

		return $total_price;
	}

	public function hasUnavailableProducts($cart)
	{
		foreach ($cart as $cart_index => $cart_item) {
			$product = $cart_item['data'];
			$variant = $cart_item['variant'];
			$qty_in_stock = !empty($variant) ? $variant['variant_stock_qty'] : $product['product_stock_qty'];

			if (Yii::app()->params->settings['stock'] != 'none' && !$qty_in_stock) {
				return true;
			}
		}

		return false;
	}

	public function hasPreorderProducts($cart)
	{
		foreach ($cart as $cart_index => $cart_item) {
			$product = $cart_item['data'];

			if (in_array($product['product_stock_type'], ['preorder', 'online'])) {
				return true;
			}
		}

		return false;
	}

	public function add($model)
	{
		$product = $model->getProduct();
		$variant = $model->getVariant();
		$options = $model->getOptions();

		if (!empty($options)) {
			$option_ids = $model->option_id;
			sort($option_ids, SORT_NUMERIC);

			if (!empty($variant)) {
				$cart_index = $model->product_id . '_' . $model->variant_id . '_O_' . implode('_', $option_ids);
				$regular_price = $variant['variant_price'];
			} else {
				$cart_index = $model->product_id . '_O_' . implode('_', $option_ids);
				$regular_price = $product['product_price'];
			}

			foreach ($options as $option) {
				$regular_price += (float) $option['option_price'];
			}
		} elseif (!empty($variant)) {
			$cart_index = $model->product_id . '_' . $model->variant_id;
			$regular_price = $variant['variant_price'];
		} else {
			$cart_index = $model->product_id;
			$regular_price = $product['product_price'];
		}

		$discount_price = 0; // Product::getDiscountPrice($regular_price, $product['category_id'], $product['brand_id']);
		$product_price = !empty($discount_price) ? $discount_price['price'] : $regular_price;

		if (isset($_SESSION['cart']['items'][$cart_index])) {
			$model->qty += $_SESSION['cart']['items'][$cart_index]['qty'];
			$this->update($model, true);
		} else {
			$_SESSION['cart']['items'][$cart_index] = array(
				'product_id' => $model->product_id,
				'variant_id' => (int) $model->variant_id,
				'option_id' => !empty($option_ids) ? $option_ids : null,
				'qty' => $model->qty,
				'price' => $product_price,
				'discount' => !empty($discount_price) ? $discount_price['discount'] : 0,
			);

			$this->recalculateCart();
		}
	}

	public function addCustomProduct($model)
	{
		$cart_index = 'C_' . strtoupper(uniqid());

		$_SESSION['cart']['items'][$cart_index] = array(
			'product_id' => 0,
			'variant_id' => 0,
			'option_id' => null,
			'qty' => 1,
			'price' => $model->price,
			'title' => $model->title,
			'discount' => 0,
		);

		$this->recalculateCart();
	}
	
	public function addComment($cart_index, $comment)
	{
		if (isset($_SESSION['cart']['items'][$cart_index])) {
			$_SESSION['cart']['items'][$cart_index]['comment'] = $comment;
		}
	}

	public function update($model, $shift = false)
	{
		$max_available_qty = false;
		
		if (!empty($model->option_id)) {
			$option_ids = $model->option_id;
			sort($option_ids, SORT_NUMERIC);

			if (!empty($model->variant_id)) {
				$cart_index = $model->product_id . '_' . $model->variant_id . '_O_' . implode('_', $option_ids);
			} else {
				$cart_index = $model->product_id . '_O_' . implode('_', $option_ids);
			}
		} elseif (!empty($model->variant_id)) {
			$cart_index = $model->product_id . '_' . $model->variant_id;

			$variant = $model->getVariant();
			$max_available_qty = $variant['variant_stock_qty'];
		} else {
			$cart_index = $model->product_id;
			
			$product = $model->getProduct();
			$max_available_qty = $product['product_stock_qty'];
		}

		if (!isset($_SESSION['cart']['items'][$cart_index])) {
			return false;
		}

		// filter available qty
        if (Yii::app()->params->settings['stock'] != 'none' && $max_available_qty !== false) {
			if ($max_available_qty < 1) {
				// not available in stock
				// remove item
				// $model->qty = 0;
				return false;
			} elseif ($model->qty > $max_available_qty) {
				// set max qty
				$model->qty = $max_available_qty;
			}
        }

		if ($model->qty == 0) {
			$this->remove($model);
		} else {
			$_SESSION['cart']['items'][$cart_index]['qty'] = $model->qty;

			if ($shift) {
				$item = $_SESSION['cart']['items'][$cart_index];
				unset($_SESSION['cart']['items'][$cart_index]);

				$_SESSION['cart']['items'][$cart_index] = $item;
			}

			$this->recalculateCart();
		}
	}

	public function price($model)
	{
		if (!empty($model->option_id)) {
			$option_ids = $model->option_id;
			sort($option_ids, SORT_NUMERIC);

			if (!empty($model->variant_id)) {
				$cart_index = $model->product_id . '_' . $model->variant_id . '_O_' . implode('_', $option_ids);
			} else {
				$cart_index = $model->product_id . '_O_' . implode('_', $option_ids);
			}
		} elseif (!empty($model->variant_id)) {
			$cart_index = $model->product_id . '_' . $model->variant_id;
		} else {
			$cart_index = $model->product_id;
		}

		if (!isset($_SESSION['cart']['items'][$cart_index])) {
			return false;
		}

		$_SESSION['cart']['items'][$cart_index]['price'] = $model->price;
		$_SESSION['cart']['items'][$cart_index]['discount'] = $model->discount;

		$this->recalculateCart();
	}

	public function remove($model)
	{
		if (!empty($model->option_id)) {
			$option_ids = $model->option_id;
			sort($option_ids, SORT_NUMERIC);

			if (!empty($model->variant_id)) {
				$cart_index = $model->product_id . '_' . $model->variant_id . '_O_' . implode('_', $option_ids);
			} else {
				$cart_index = $model->product_id . '_O_' . implode('_', $option_ids);
			}
		} elseif (!empty($model->variant_id)) {
			$cart_index = $model->product_id . '_' . $model->variant_id;
		} else {
			$cart_index = $model->product_id;
		}

		if (!isset($_SESSION['cart']['items'][$cart_index])) {
			return false;
		}

		unset($_SESSION['cart']['items'][$cart_index]);
		$this->recalculateCart();
	}

	public function clear()
	{
		$_SESSION['cart'] = array(
			'total' => 0,
			'price' => 0,
			'items' => array(),
		);
	}

	public function recalculateCart()
	{
		$total = 0;
		$price = 0;

		if (!empty($_SESSION['cart']['items'])) {
			foreach ($_SESSION['cart']['items'] as $item) {
				$total += $item['qty'];
				$price += $item['qty'] * $item['price'];
			}
		}

		$_SESSION['cart']['total'] = $total;
		$_SESSION['cart']['price'] = $price;
	}

	public function recalculateCartPrices()
	{
		$products = $this->getCartProducts();

		if (!empty($products)) {
			foreach ($products as $product) {
				$product_id = $product['product_id'];
				$product_data = $product['data'];

				$discount_price = Product::getDiscountPrice($product_data['product_price'], $product_data['category_id'], $product_data['brand_id']);

				if (!empty($discount_price)) {
					$_SESSION['cart']['items'][$product_id]['price'] = $discount_price['price'];
					$_SESSION['cart']['items'][$product_id]['discount'] = $discount_price['discount'];
				} else {
					$_SESSION['cart']['items'][$product_id]['price'] = $product_data['product_price'];
					$_SESSION['cart']['items'][$product_id]['discount'] = 0;
				}
			}
		}

		$this->recalculateCart();
	}

	public function getCartProducts()
	{
		$cart_products = array();

		// get product, variant ids, option ids
		$product_ids = array();
		$variant_ids = array();
		$option_ids = array();
		$has_custom_products = false;

		if (isset($_SESSION['cart']['items'])) {
			foreach ($_SESSION['cart']['items'] as $cart_index => $value) {
				if (preg_match('#^C_[0-9abcdef]{13}#ui', $cart_index, $cart_mathes)) {
					// custom product
					$has_custom_products = true;
					continue;
				} elseif (preg_match('#^(\d+)_(\d+)_O_#', $cart_index, $cart_mathes)) {
					$product_ids[] = $cart_mathes[1];
					$variant_ids[] = $cart_mathes[2];

					list($index_part, $options_list) = explode('_O_', $cart_index);
					$options_list = explode('_', $options_list);

					$option_ids = array_merge($option_ids, $options_list);
				} elseif (preg_match('#^(\d+)_O_#', $cart_index, $cart_mathes)) {
					$product_ids[] = $cart_mathes[1];

					list($index_part, $options_list) = explode('_O_', $cart_index);
					$options_list = explode('_', $options_list);

					$option_ids = array_merge($option_ids, $options_list);
				} elseif (preg_match('#^(\d+)_(\d+)$#', $cart_index, $cart_mathes)) {
					$product_ids[] = $cart_mathes[1];
					$variant_ids[] = $cart_mathes[2];
				} else {
					$product_ids[] = $cart_index;
				}
			}
		}

		if (!empty($product_ids)) {
			$products = Product::model()->getProductsByIds($product_ids, true);
			
			if (!empty($variant_ids)) {
				$variants = Product::model()->getVariantsByIds($variant_ids);
			}

			if (!empty($option_ids)) {
				$option_ids = array_unique($option_ids);
				$options = Product::model()->getOptionsByIds($option_ids);
			}

			if (!empty($products)) {
				$cart_products = array_reverse($_SESSION['cart']['items'], true);

				foreach ($cart_products as $cart_index => $cart_product) {
					if (preg_match('#^C_[0-9abcdef]{13}#ui', $cart_index)) {
						// custom product
						$cart_products[$cart_index]['data'] = array(
							'product_id' => $cart_index,
							'product_alias' => '',
							'product_sku' => '',
							'product_title' => $cart_product['title'],
							'product_price' => $cart_product['price'],
							'category_id' => 0,
							'product_pack_size' => 0,
							'product_price_type' => 'item',
						);
						$cart_products[$cart_index]['variant'] = array();
						$cart_products[$cart_index]['options'] = array();

						continue;
					} elseif (preg_match('#^(\d+)_(\d+)_O_#', $cart_index, $cart_mathes)) {
						$product_id = $cart_mathes[1];
						$variant_id = $cart_mathes[2];

						list($index_part, $options_list) = explode('_O_', $cart_index);
						$options_indices = explode('_', $options_list);

						$product_options = array();

						foreach ($options_indices as $options_index) {
							if (!isset($options[$options_index])) {
								break 2;
							}

							$product_options[$options_index] = $options[$options_index];
						}

						if (isset($products[$product_id]) && isset($variants[$variant_id])) {
							$cart_products[$cart_index]['data'] = $products[$product_id];
							$cart_products[$cart_index]['variant'] = $variants[$variant_id];
							$cart_products[$cart_index]['options'] = $product_options;
						}
					} elseif (preg_match('#^(\d+)_O_#', $cart_index, $cart_mathes)) {
						$product_id = $cart_mathes[1];

						list($index_part, $options_list) = explode('_O_', $cart_index);
						$options_indices = explode('_', $options_list);

						$product_options = array();

						foreach ($options_indices as $options_index) {
							if (!isset($options[$options_index])) {
								break 2;
							}

							$product_options[$options_index] = $options[$options_index];
						}

						if (isset($products[$product_id])) {
							$cart_products[$cart_index]['data'] = $products[$product_id];
							$cart_products[$cart_index]['variant'] = array();
							$cart_products[$cart_index]['options'] = $product_options;
						}
					} elseif (preg_match('#(\d+)_(\d+)#', $cart_index, $cart_mathes)) {
						$product_id = $cart_mathes[1];
						$variant_id = $cart_mathes[2];

						if (isset($products[$product_id]) && isset($variants[$variant_id])) {
							$cart_products[$cart_index]['data'] = $products[$product_id];
							$cart_products[$cart_index]['variant'] = $variants[$variant_id];
							$cart_products[$cart_index]['options'] = array();
						}
					} else {
						$product_id = $cart_index;

						if (isset($products[$product_id])) {
							$cart_products[$cart_index]['data'] = $products[$product_id];
							$cart_products[$cart_index]['variant'] = array();
							$cart_products[$cart_index]['options'] = array();
						}
					}
				}

				// remove empty products
				foreach ($cart_products as $cart_index => $cart_product) {
					if (!isset($cart_product['data'])) {
						// remove from cart
						$model = new CartForm('remove');
						$model->product_id = $cart_product['product_id'];
						$model->variant_id = $cart_product['variant_id'];
						$model->option_id = isset($cart_product['option_id']) ? $cart_product['option_id'] : null;
						$this->remove($model);

						unset($cart_products[$cart_index]);
					}
				}
			}
		} elseif ($has_custom_products) {
			$cart_products = array_reverse($_SESSION['cart']['items'], true);

			foreach ($cart_products as $cart_index => $cart_product) {
				if (preg_match('#^C_[0-9abcdef]{13}#ui', $cart_index)) {
					// custom product
					$cart_products[$cart_index]['data'] = array(
						'product_id' => $cart_index,
						'product_alias' => '',
						'product_sku' => '',
						'product_title' => $cart_product['title'],
						'product_price' => $cart_product['price'],
						'category_id' => 0,
						'product_pack_size' => 0,
						'product_price_type' => 'item',
					);
					$cart_products[$cart_index]['variant'] = array();
					$cart_products[$cart_index]['options'] = array();

					continue;
				}
			}
		}

		// clear cart if no products
		if (empty($cart_products)) {
			$this->clear();
		}

		// recalculate cart
		$this->recalculateCart();

		return $cart_products;		
	}
}