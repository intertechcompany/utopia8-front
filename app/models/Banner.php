<?php
class Banner extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getSliderBanners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'slider'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $banners;
	}
	
	public function getSection1Banners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'section_1'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $banners;
	}
	
	public function getSection2Banners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'section_2'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $banners;
	}

	public function getSection3Banners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'section_3'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($banners)) {
			foreach ($banners as $id => $banner) {
				if (!empty($banner['products'])) {
					$banners[$id]['products'] = Product::model()->getProductsByIds(explode(',', $banner['products']));
				}
			}
		}
			
		return $banners;
	}
	
	public function getSection4Banners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'section_4'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($banners)) {
			foreach ($banners as $id => $banner) {
				if (!empty($banner['products'])) {
					$banners[$id]['products'] = Product::model()->getProductsByIds(explode(',', $banner['products']));
				}
			}
		}
			
		return $banners;
	}
	
	public function getSection5Banners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'section_5'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($banners)) {
			foreach ($banners as $id => $banner) {
				if (!empty($banner['products'])) {
					$banners[$id]['products'] = Product::model()->getProductsByIds(explode(',', $banner['products']));
				}
			}
		}
			
		return $banners;
	}
	
	public function getInstagramBanners()
	{
		$banners = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'instagram'
							 ORDER BY b.banner_position, b.banner_id DESC
							 LIMIT 0,4")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $banners;
	}
	
	public function getCategoryBanners()
	{
		$banners = [];

		$banners_list = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'category'
							 ORDER BY b.banner_position, b.banner_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		foreach ($banners_list as $banner) {
			$category_id = $banner['category_id'];
			
			if (!isset($banners[$category_id]) || count($banners[$category_id]) < 6) {
				$banners[$category_id][] = $banner;
			}
		}

		return $banners;
	}

	public function getBoxBanner()
    {
        $banner =  Yii::app()->db
            ->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'box'")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->queryRow();

        if (!empty($banner)) {
            if (!empty($banner['products'])) {
                $banner['products'] = Product::model()->getProductsByIds(explode(',', $banner['products']));
            }
        }

        return $banner;
    }

    public function getPostcardBanner()
    {
        $banner = Yii::app()->db
            ->createCommand("SELECT b.*, bl.* 
							 FROM banner as b 
							 JOIN banner_lang as bl 
							 ON b.banner_id = bl.banner_id AND bl.language_code = :code AND bl.banner_visible = 1 
							 WHERE b.active = 1 AND b.banner_place = 'postcard'")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->queryRow();

        if (!empty($banner)) {
            if (!empty($banner['products'])) {
                $banner['products'] = Product::model()->getProductsByIds(explode(',', $banner['products']));
            }
        }

        return $banner;
    }
}