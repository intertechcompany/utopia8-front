<?php
class Care extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getCaresList()
	{
		$cares = [];
		
		$cares_list = Yii::app()->db
			->createCommand("SELECT c.*, cl.care_name 
							 FROM care as c 
							 JOIN care_lang as cl 
							 ON c.care_id = cl.care_id AND cl.language_code = :code AND cl.care_visible = 1  
							 ORDER BY cl.care_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($cares_list as $care) {
			$care_id = $care['care_id'];
			$cares[$care_id] = $care;
		}
			
		return $cares;
	}

	public function getCares()
	{
		$cares = Yii::app()->db
			->createCommand("SELECT c.*, cl.* 
							 FROM care as c 
							 JOIN care_lang as cl 
							 ON c.care_id = cl.care_id AND cl.language_code = :code AND cl.care_visible = 1  
							 ORDER BY cl.care_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $cares;
	}

	public function getCareById($care_id)
	{
		$care = Yii::app()->db
			->createCommand("SELECT c.*, cl.* 
							 FROM care as c 
							 JOIN care_lang as cl 
							 ON c.care_id = cl.care_id AND cl.language_code = :code AND cl.care_visible = 1 
							 WHERE c.care_id = :care_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':care_id', (int) $care_id, PDO::PARAM_INT)
			->queryRow();
			
		return $care;
    }
}