<?php
class Reclamation extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getReclamationsList()
	{
		$reclamations = [];
		
		$reclamations_list = Yii::app()->db
			->createCommand("SELECT r.*, rl.reclamation_name 
							 FROM reclamation as r 
							 JOIN reclamation_lang as rl 
							 ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code AND rl.reclamation_visible = 1  
							 ORDER BY rl.reclamation_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($reclamations_list as $reclamation) {
			$reclamation_id = $reclamation['reclamation_id'];
			$reclamations[$reclamation_id] = $reclamation;
		}
			
		return $reclamations;
	}

	public function getReclamations()
	{
		$reclamations = Yii::app()->db
			->createCommand("SELECT r.*, rl.* 
							 FROM reclamation as r 
							 JOIN reclamation_lang as rl 
							 ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code AND rl.reclamation_visible = 1  
							 ORDER BY rl.reclamation_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $reclamations;
	}

	public function getReclamationById($reclamation_id)
	{
		$reclamation = Yii::app()->db
			->createCommand("SELECT r.*, rl.* 
							 FROM reclamation as r 
							 JOIN reclamation_lang as rl 
							 ON r.reclamation_id = rl.reclamation_id AND rl.language_code = :code AND rl.reclamation_visible = 1 
							 WHERE r.reclamation_id = :reclamation_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':reclamation_id', (int) $reclamation_id, PDO::PARAM_INT)
			->queryRow();
			
		return $reclamation;
    }
}