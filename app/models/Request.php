<?php

class Request extends CModel
{
	public function rules()
	{
        return array();
    }

	public function attributeNames()
	{
		return array();
	}

	public static function model()
	{
        return new self();
    }

	public function add($model)
	{
		$request_id = null;
		$today = date('Y-m-d H:i:s');
		$user_id = (!Yii::app()->user->isGuest) ? Yii::app()->user->id : 0;

		$insert_request = [
			'created' => $today,
			'saved' => $today,
			'is_new' => 1,
			'course_id' => (int) $model->course_id,
			'first_name' => $model->first_name,
			'last_name' => $model->last_name,
			'email' => $model->email,
			'phone' => $model->phone,
			'message' => !empty($model->code) ? 'Промокод: ' . $model->code : '',
		];

		$builder = Yii::app()->db->schema->commandBuilder;

		try {
			$rs = $builder->createInsertCommand('request', $insert_request)->execute();

			if ($rs) {
				$request_id = (int) Yii::app()->db->getLastInsertID();
			}
		} catch (CDbException $e) {
			return false;
		}

		return $request_id;
	}
}
