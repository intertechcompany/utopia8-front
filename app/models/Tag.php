<?php
class Tag extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getTagByAlias($alias)
	{
		$tag = Yii::app()->db
			->createCommand("SELECT t.*, tl.* 
							 FROM tag as t 
							 JOIN tag_lang as tl 
							 ON t.tag_id = tl.tag_id AND tl.language_code = :code AND tl.tag_visible = 1 
							 WHERE t.tag_alias = :alias")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindParam(':alias', $alias, PDO::PARAM_STR)
			->queryRow();
			
		return $tag;
    }
}