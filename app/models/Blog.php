<?php
class Blog extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getBlogs()
	{
		$blogs = Yii::app()->db
			->createCommand("SELECT b.blog_id, b.blog_alias, b.blog_published, b.blog_photo, bl.blog_title, bl.blog_intro, bc.category_alias, bcl.category_name  
							 FROM blog as b 
							 JOIN blog_lang as bl 
							 ON b.blog_id = bl.blog_id AND bl.language_code = :code 
							 JOIN blog_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN blog_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code 
							 WHERE b.active = 1 AND b.blog_hide = 0 AND bc.active = 1 
							 ORDER BY b.blog_published DESC, b.blog_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $blogs;
	}
	
	public function getCategoryBlogs($category_id)
	{
		$blogs = Yii::app()->db
			->createCommand("SELECT b.blog_id, b.blog_alias, b.blog_published, b.blog_photo, bl.blog_title, bl.blog_intro, bc.category_alias, bcl.category_name  
							 FROM blog as b 
							 JOIN blog_lang as bl 
							 ON b.blog_id = bl.blog_id AND bl.language_code = :code 
							 JOIN blog_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN blog_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code 
							 WHERE b.active = 1 AND b.blog_hide = 0 AND b.category_id = :category_id AND bc.active = 1 
							 ORDER BY b.blog_published DESC, b.blog_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();
			
		return $blogs;
	}

	public function getBlogByAlias($alias)
	{
		$blog = Yii::app()->db
			->createCommand("SELECT b.*, bl.*, bc.category_alias, bcl.category_name 
							 FROM blog as b 
							 JOIN blog_lang as bl 
							 ON b.blog_id = bl.blog_id AND bl.language_code = :code 
							 JOIN blog_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN blog_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code
							 WHERE b.active = 1 AND b.blog_alias = :alias AND bc.active = 1 
							 LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $blog;
	}
}