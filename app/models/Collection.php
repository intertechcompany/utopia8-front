<?php
class Collection extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getCategoryCollectionsTotal($per_page, $category_id, $facets)
	{
		$join = '';
		$where = '';

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(DISTINCT p.collection_id) 
							 FROM product as p
							 JOIN collection as c 
							 ON p.collection_id = c.collection_id AND c.active = 1 
							 {$join}
							 WHERE p.active = 1 AND p.category_id = :category_id {$where}")
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryScalar();
		
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}

	public function getCategoryCollections($offset, $per_page, $category_id, $facets, $sort)
	{
		$join = '';
		$where = '';

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'c.collection_rating DESC, c.collection_id DESC';
		}

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$collections = Yii::app()->db
			->createCommand("SELECT c.collection_id, c.collection_alias, c.collection_photo, cl.collection_title, cl.collection_country, p.product_id, p.product_price, p.product_price_type, p.product_pack_size, p.collection_id 
							 FROM collection as c  
							 JOIN collection_lang as cl 
							 ON c.collection_id = cl.collection_id AND cl.language_code = :code AND cl.collection_visible = 1 
							 JOIN (
							 	SELECT pi.* 
								FROM (
									SELECT p.product_id, p.product_price, p.product_price_type, p.product_pack_size, p.collection_id 
									FROM product as p 
									{$join}
									WHERE p.active = 1 AND p.category_id = :category_id {$where} 
									ORDER BY p.product_price
								) as pi
								GROUP BY pi.collection_id
								ORDER BY NULL
							 ) as p 
							 ON c.collection_id = p.collection_id 
							 WHERE c.active = 1 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();
			
		return $collections;
	}

	public function getCollectionById($collection_id)
	{
		$collection = Yii::app()->db
			->createCommand("SELECT * FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code AND cl.collection_visible = 1 WHERE c.active = 1 AND c.collection_id = :collection_id LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
			->queryRow();

		return $collection;
	}

	public function getCollectionByAlias($alias)
	{
		$collection = Yii::app()->db
			->createCommand("SELECT * FROM collection as c JOIN collection_lang as cl ON c.collection_id = cl.collection_id AND cl.language_code = :code AND cl.collection_visible = 1 WHERE c.active = 1 AND c.collection_alias = :alias LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $collection;
	}

	public function getCollectionPhotos($collection_id)
	{
		$collection_photos = Yii::app()->db
			->createCommand("SELECT * FROM collection_photo WHERE collection_id = :id ORDER BY position")
			->bindValue(':id', (int) $collection_id, PDO::PARAM_STR)
			->queryAll();

		return $collection_photos;
	}
}