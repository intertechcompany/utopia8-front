<?php

/**
 * OrderForm class.
 * OrderForm is the data structure for keeping
 * write mail form data. It is used by the 'mail' action of 'AjaxController'.
 */
class OrderForm extends FormModel
{
	public $delivery;
	public $payment;
	public $first_name;
	public $last_name;
	public $full_name;
	public $phone;
	public $email;
	public $country;
	public $zip;
	public $region;
	public $city;
	public $address;
	public $np_city;
	public $np_department;
	public $np_address;
	public $np_building;
	public $np_apartment;
	public $comment;
	public $wishes;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'np_city',
				'required',
				'message' => Lang::t('checkout.error.city'),
			),
			array(
				'payment',
				'filter',
				'filter' => 'intval',
			),
			array(
				'payment',
				'isValidPayment',
				// 'in',
				// 'range' => array(1,2,3),
				// 'allowEmpty' => false,
				'message' => Lang::t('checkout.error.payment'),
			),
			array(
				'delivery',
				'filter',
				'filter' => 'intval',
			),
			array(
				'delivery',
				'isValidDelivery',
				// 'in',
				// 'range' => array(1,2,3),
				// 'allowEmpty' => false,
				'message' => Lang::t('checkout.error.delivery'),
			),
			array(
				'first_name, last_name, phone, email',
				'required',
				'message' => Lang::t('checkout.error.required'),
			),
			/* array(
				'first_name, last_name',
				'match',
				'pattern' => '/^[\p{L}]+$/ui',
				'message' => 'Допускаються тільки літери!',
				'skipOnError' => true,
			), */
			array(
				'full_name',
				'filter',
				'filter' => array($this, 'fullName'),
			),
			array(
				'phone',
				'match',
				'pattern' => '/^\+380 \d{2} \d{3} \d{4}$/ui',
				'message' => Lang::t('checkout.error.phone'),
				'skipOnError' => true,
			),
			array(
				'email',
				'email',
				'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
				'message' => Lang::t('checkout.error.email'),
				'skipOnError' => true,
			),
			/* array(
				'country, city, address',
				'isValidWorldwideDelivery',
				'message' => Lang::t('checkout.error.required'),
			), */
			array(
				'address',
				'isValidCourierDelivery',
				'message' => Lang::t('checkout.error.required'),
			),
			array(
				'np_department',
				'isValidNpDelivery',
				'message' => Lang::t('checkout.error.required'),
			),
			array(
				'zip, comment, wishes',
				'safe',
			),
		);
	}
	
	public function isValidPayment($attribute, $params)
	{
		$payments = [1,2];
		
		if ($this->np_city == 'Одеса' || $this->np_city == 'Одесса' || $this->np_city == 'Харків' || $this->np_city == 'Харьков') {
			$payments = [1,2,3];
		}
		
		if (!in_array($this->$attribute, $payments)) {
			$this->addError($attribute, $params['message']);
		}
	}
	
	public function isValidDelivery($attribute, $params)
	{
		$deliveries = [2,3];
		
		if ($this->np_city == 'Одеса' || $this->np_city == 'Одесса' || $this->np_city == 'Харків' || $this->np_city == 'Харьков') {
			$deliveries = [1,2,3];
		}
		
		if (!in_array($this->$attribute, $deliveries)) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function isValidCourierDelivery($attribute, $params)
	{
		if ($this->delivery == 3 && empty($this->$attribute)) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function isValidNpDelivery($attribute, $params)
	{
		if ($this->delivery == 2 && empty($this->$attribute)) {
			$this->addError($attribute, $params['message']);
		}
	}

	public function fullName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}
}