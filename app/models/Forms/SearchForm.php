<?php

/**
 * SearchForm class.
 * SearchForm is the data structure for keeping
 * subscribe form data. It is used by the 'search' action of 'SiteController'.
 */
class SearchForm extends FormModel
{
	public $keyword;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'keyword',
				'filter',
				'filter' => 'trim',
			),
			array(
				'keyword',
				'required',
				'message' => Lang::t('search.error.enterKeyword'),
			),
			array(
				'keyword',
				'length',
				'allowEmpty' => false,
				'min' => 3,
				'tooShort' => Lang::t('search.error.keywordLength'),
				'skipOnError' => true,
			),
		);
	}
}