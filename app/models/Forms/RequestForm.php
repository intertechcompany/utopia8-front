<?php

/**
 * RequestForm class.
 */
class RequestForm extends FormModel
{
    public $course_id;
    public $first_name;
    public $last_name;
    public $phone;
    public $email;
    public $code;

    private $course;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array(
                'course_id',
                'isValidCourse',
                'message' => Lang::t('course.error.invalidData'),
            ),
            array(
                'first_name, last_name, phone, email',
                'required',
                'message' => Lang::t('course.error.required'),
            ),
            /* array(
				'first_name, last_name',
				'match',
				'pattern' => '/^[\p{L}]+$/ui',
				'message' => 'Допускаються тільки літери!',
				'skipOnError' => true,
            ), */
            array(
				'phone',
				'match',
				'pattern' => '/^\+380 \d{2} \d{3} \d{4}$/ui',
				'message' => Lang::t('course.error.phone'),
				'skipOnError' => true,
			),
            array(
                'email',
                'email',
                'pattern' => '/^([a-z0-9]([\-\_\.]*[a-z0-9])*)+@([a-z0-9]([\-]*[a-z0-9])*\.)+[a-z]{2,}$/i',
                'message' => Lang::t('course.error.email'),
                'skipOnError' => true,
            ),
            array(
                'code',
                'safe',
            ),
        );
    }

    public function isValidCourse($attribute, $params)
	{
		$this->course = Course::model()->getCourseById($this->$attribute);
		
		if (empty($this->course) || strtotime($this->course['course_date']) <= time()) {
			$this->addError($attribute, $params['message']);
		}
    }
    
    public function getCourse()
    {
        return $this->course;
    }
}
