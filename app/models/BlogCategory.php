<?php
class BlogCategory extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getBlogCategoriesList()
	{
		$categories = [];
		
		$categories_list = Yii::app()->db
			->createCommand("SELECT b.*, bl.category_name 
							 FROM blog_category as b 
							 JOIN blog_category_lang as bl 
							 ON b.category_id = bl.category_id AND bl.language_code = :code AND bl.category_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY b.category_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($categories_list as $category) {
			$category_id = $category['category_id'];
			$categories[$category_id] = $category;
		}
			
		return $categories;
	}

	public function getBlogCategories()
	{
		$categories = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM blog_category as b 
							 JOIN blog_category_lang as bl 
							 ON b.category_id = bl.category_id AND bl.language_code = :code AND bl.category_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY b.category_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $categories;
	}

	public function getBlogCategoryById($category_id)
	{
		$category = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM blog_category as b 
							 JOIN blog_category_lang as bl 
							 ON b.category_id = bl.category_id AND bl.language_code = :code AND bl.category_visible = 1 
							 WHERE b.active = 1 AND b.category_id = :category_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryRow();
			
		return $category;
    }
	
	public function getBlogCategoryByAlias($alias)
	{
		$category = Yii::app()->db
			->createCommand("SELECT b.*, bl.* 
							 FROM blog_category as b 
							 JOIN blog_category_lang as bl 
							 ON b.category_id = bl.category_id AND bl.language_code = :code AND bl.category_visible = 1 
							 WHERE b.active = 1 AND b.category_alias = :alias")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();
			
		return $category;
	}
	
	public function getBlogCategoriesSitemap()
	{
		$categories = Yii::app()->db
			->createCommand("SELECT b.category_id, b.category_alias, bl.category_name, bl.category_no_index 
							 FROM blog_category as b 
							 JOIN blog_category_lang as bl 
							 ON b.category_id = bl.category_id AND bl.language_code = :code AND bl.category_visible = 1 
							 WHERE b.active = 1 
							 ORDER BY b.category_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $categories;
	}
}