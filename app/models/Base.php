<?php
class Base extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getBases()
	{
		$bases = Yii::app()->db
			->createCommand("SELECT b.base_id, b.base_alias, b.base_photo, bl.base_title, bl.base_intro, bc.category_alias, bcl.category_name  
							 FROM base as b 
							 JOIN base_lang as bl 
							 ON b.base_id = bl.base_id AND bl.language_code = :code 
							 JOIN base_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN base_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code 
							 WHERE b.active = 1 AND bc.active = 1 
							 ORDER BY b.base_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $bases;
	}
	
	public function getCategoryBases($category_id)
	{
		$bases = Yii::app()->db
			->createCommand("SELECT b.base_id, b.base_alias, b.base_photo, bl.base_title, bl.base_intro, bc.category_alias, bcl.category_name  
							 FROM base as b 
							 JOIN base_lang as bl 
							 ON b.base_id = bl.base_id AND bl.language_code = :code 
							 JOIN base_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN base_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code 
							 WHERE b.active = 1 AND b.category_id = :category_id AND bc.active = 1 
							 ORDER BY b.base_id DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();
			
		return $bases;
	}

	public function getBaseByAlias($alias)
	{
		$base = Yii::app()->db
			->createCommand("SELECT b.*, bl.*, bc.category_alias, bcl.category_name 
							 FROM base as b 
							 JOIN base_lang as bl 
							 ON b.base_id = bl.base_id AND bl.language_code = :code 
							 JOIN base_category as bc 
							 ON b.category_id = bc.category_id 
							 JOIN base_category_lang as bcl 
							 ON bc.category_id = bcl.category_id AND bcl.language_code = :code
							 WHERE b.active = 1 AND b.base_alias = :alias AND bc.active = 1 
							 LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $base;
	}
}