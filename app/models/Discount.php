<?php
class Discount extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function init()
	{
		if (!isset($_SESSION['discount'])) {
			$_SESSION['discount'] = [];
		}
	}

	public static function getDiscountValue($price)
	{
		if (empty($_SESSION['discount'])) {
			return 0;
		}

		$discount = $_SESSION['discount'];
		
		if ($discount['discount_type'] == 'percentage') {
			$discount_value = $price * -$discount['discount_value'] / 100;
		} else {
			$discount_value = -$discount['discount_value'];
		}

		return round($discount_value);
	}

	public function getDiscountByCode($dicount_code)
	{
		$discount = Yii::app()->db
			->createCommand("SELECT * FROM discount WHERE discount_code = :code AND active = 1 LIMIT 1")
			->bindValue(':code', $dicount_code, PDO::PARAM_STR)
			->queryRow();
			
		return $discount;
	}

	public function getDiscount()
	{
		$discount = $_SESSION['discount'];
		
		if (!empty($discount)) {
			$form = new DiscountForm();
			$form->attributes = [
				'code' => $discount['discount_code'],
			];

			if (!$form->validate()) {
				$this->clear();
			}
		}
		
		return $_SESSION['discount'];
	}

	public function apply($discount)
	{
		$_SESSION['discount'] = $discount;
	}

	public function clear()
	{
		$_SESSION['discount'] = [];
	}

	public function activate()
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_discount = array(
			'saved' => $today,
			'activated' => $_SESSION['discount']['activated'] + 1,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "discount_id = :discount_id" , 
				"params" => array(
					"discount_id" => (int) $_SESSION['discount']['discount_id'],
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('discount', $update_discount, $update_criteria)->execute();

			if ($rs) {
				$this->clear();
				
				return true;
			}
		} catch (CDbException $e) {
			// ...
		}

		return false;
	}
}
