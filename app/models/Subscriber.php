<?php

class Subscriber extends CModel
{
	public function rules()
	{
        return array();
    }

	public function attributeNames()
	{
		return array();
	}

	public static function model()
	{
        return new self();
    }

	public function add($model)
	{
		$subscriber_id = null;
		$today = date('Y-m-d H:i:s');

		$insert_subscriber = [
			'created' => $today,
			'saved' => $today,
			'subscriber_email' => $model->email,
		];

		$builder = Yii::app()->db->schema->commandBuilder;

		try {
			$rs = $builder->createInsertCommand('subscriber', $insert_subscriber)->execute();

			if ($rs) {
				$subscriber_id = (int) Yii::app()->db->getLastInsertID();
			}
		} catch (CDbException $e) {
			return false;
		}

		return $subscriber_id;
	}

	public function getSubscriberByEmail($email)
	{
		$subscriber = Yii::app()->db
			->createCommand("SELECT * FROM subscriber WHERE subscriber_email = :email LIMIT 1")
			->bindValue(':email', $email, PDO::PARAM_STR)
			->queryRow();
			
		return $subscriber;
	}
}
