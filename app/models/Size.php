<?php
class Size extends CModel
{
	private $per_page = 10;

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getSizesList()
	{
		$sizes = [];
		
		$sizes_list = Yii::app()->db
			->createCommand("SELECT s.*, sl.size_name 
							 FROM size as s 
							 JOIN size_lang as sl 
							 ON s.size_id = sl.size_id AND sl.language_code = :code AND sl.size_visible = 1  
							 ORDER BY sl.size_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($sizes_list as $size) {
			$size_id = $size['size_id'];
			$sizes[$size_id] = $size;
		}
			
		return $sizes;
	}

	public function getSizes()
	{
		$sizes = Yii::app()->db
			->createCommand("SELECT s.*, sl.* 
							 FROM size as s 
							 JOIN size_lang as sl 
							 ON s.size_id = sl.size_id AND sl.language_code = :code AND sl.size_visible = 1  
							 ORDER BY sl.size_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $sizes;
	}

	public function getSizeById($size_id)
	{
		$size = Yii::app()->db
			->createCommand("SELECT s.*, sl.* 
							 FROM size as s 
							 JOIN size_lang as sl 
							 ON s.size_id = sl.size_id AND sl.language_code = :code AND sl.size_visible = 1 
							 WHERE s.size_id = :size_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':size_id', (int) $size_id, PDO::PARAM_INT)
			->queryRow();
			
		return $size;
    }
}