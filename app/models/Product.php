<?php
class Product extends CModel
{
	private $product_extra_info = false;
	private $per_page = 10;
	private $catalog_fields = [
		'p.product_id',
		'p.product_alias',
		'p.product_sku',
		'p.category_id',
		'p.brand_id',
		'p.collection_id',
		'p.product_extended',
		'p.product_photo',
		'p.product_price_type',
		'p.product_price',
		'p.product_price_old',
		'p.product_instock',
		'p.product_stock_qty',
		'p.product_stock_type',
		'p.product_weight',
		'pl.product_title',
		'pl.product_special_title',
		'pl.product_special_tip',
		'pl.product_tags',
	];

	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function subtractStockQty($order_product)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$product_id = $order_product['product_id'];
		$variant_id = $order_product['variant_id'];
		$quantity = $order_product['quantity'];
		
		if (!empty($variant_id)) {
			$variant_qty = $order_product['variant_stock_qty'] - $quantity;
			
			$update = array(
				'saved' => $today,
				'variant_instock' => ($variant_qty > 0) ? 'in_stock' : 'out_of_stock',
				'variant_stock_qty' => $variant_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "variant_id = :id" , 
					"params" => array(
						"id" => (int) $variant_id,
					)
				)
			);
			
			try {
				$variant_rs = $builder->createUpdateCommand('product_variant', $update, $criteria)->execute();
	
				if ($variant_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		} else {
			$product_qty = $order_product['product_stock_qty'] - $quantity;
			
			$update = array(
				'saved' => $today,
				'product_instock' => ($product_qty > 0) ? 'in_stock' : 'out_of_stock',
				'product_stock_qty' => $product_qty,
			);
	
			$criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :id" , 
					"params" => array(
						"id" => (int) $product_id,
					)
				)
			);
			
			try {
				$product_rs = $builder->createUpdateCommand('product', $update, $criteria)->execute();
	
				if ($product_rs) {
					return true;
				}
			} catch (CDbException $e) {
				
			}
		}

		return false;
	}

	public function updateProductsStockStatus($products)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');
		
		foreach ($products as $product_id => $product_type) {
			$to_last_size = false;
			
			if ($product_type == 'variants') {
				// get in stock value
				$variants_summary = Yii::app()->db
					->createCommand("SELECT v.variant_id, vv.variant_instock, SUM(v.variant_stock_qty) as variant_stock_qty 
						FROM product_variant as v
						JOIN (
							SELECT MIN(variant_instock) as variant_instock 
							FROM product_variant 
							WHERE product_id = :pid
						) as vv
						WHERE v.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => $variants_summary['variant_instock'],
					'product_stock_qty' => $variants_summary['variant_stock_qty'],
				);

				$variants_available = Yii::app()->db
					->createCommand("SELECT COUNT(*) 
						FROM product_variant as v
						WHERE v.product_id = :pid AND v.variant_stock_qty > 0
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryScalar();

				$to_last_size = ($variants_available == 1) ? true : false;
			} else {
				$product_summary = Yii::app()->db
					->createCommand("SELECT p.product_instock, p.product_stock_qty 
						FROM product as p
						WHERE p.product_id = :pid 
						LIMIT 1")
					->bindValue(':pid', $product_id, PDO::PARAM_INT)
					->queryRow();
				
				$update_product = array(
					'saved' => $today,
					'product_instock' => ($product_summary['product_stock_qty'] > 0) ? 'in_stock' : 'out_of_stock',
					'product_stock_qty' => $product_summary['product_stock_qty'],
				);

				$to_last_size = ($product_summary['product_stock_qty'] == 1) ? true : false;
			}

			// update product
			$update_criteria = new CDbCriteria(
				array(
					"condition" => "product_id = :product_id" , 
					"params" => array(
						"product_id" => $product_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('product', $update_product, $update_criteria)->execute();
			
				if (Yii::app()->params->settings['last_size'] && $rs) {
					if ($to_last_size) {
						// move to last size
						$insert_category = [
							'product_id' => $product_id,
							'category_id' => Yii::app()->params->settings['last_size_cid'],
						];

						try {
							$builder->createMultipleInsertCommand('category_product', $insert_category)->execute();
						} catch (CDbException $e) {
							
						}
					} else {
						// remove from last size
						$delete_criteria = new CDbCriteria(
							array(
								"condition" => "product_id = :product_id AND category_id = :category_id" , 
								"params" => array(
									"product_id" => $product_id,
									"category_id" => Yii::app()->params->settings['last_size_cid'],
								)
							)
						);
						
						try {
							$builder->createDeleteCommand('category_product', $delete_criteria)->execute();
						}
						catch (CDbException $e) {
							// ...
						}
					}
				}
			} catch (CDbException $e) {
				// ...
			}
		}
	}

	private function getCatalogFields()
	{
		return implode(', ', $this->catalog_fields);
	}

	public function getNewestProductsTotal($per_page)
	{
		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 WHERE p.active = 1")
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}
	
	public function getNewestProducts($offset, $per_page)
	{
		$order_by = 'p.product_instock, p.product_published DESC, p.product_id DESC';
		
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}") // AND p.product_newest = 1 
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
		$products = $this->getProductsBadges($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}
	
	public function getSaleProducts($offset, $per_page, $sort)
	{
		$date = new DateTime('today', new DateTimeZone(Yii::app()->timeZone));
		$date->modify('-' . (int) Yii::app()->params->settings['newest'] . ' day');
		
		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}
		
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND p.product_price_old > 0
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}") // AND p.product_newest = 1 
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}
	
	public function getSaleProductsTotal($per_page, $categories, $brands)
	{
		if (empty($categories) && empty($brands)) {
			return array(
				'total' => 0,
				'pages' => 0,
			);
		}

		$where = [];

		if (!empty($categories)) {
			$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories) . ")";
			$where[] = "(p.category_id IN (" . implode(',', $categories) . ") OR p.product_id IN ({$additional_categories}))";
		}

        if (!empty($brands)) {
			$where[] = "p.brand_id IN (" . implode(',', $brands) . ")";
        }

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (" . implode(' OR ', $where) . ")")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}
	
	/* public function getSaleProducts($offset, $per_page, $sort, $categories, $brands)
	{
		if (empty($categories) && empty($brands)) {
			return [];
		}
		
		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		$where = [];

		if (!empty($categories)) {
			$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . implode(',', $categories) . ")";
			$where[] = "(p.category_id IN (" . implode(',', $categories) . ") OR p.product_id IN ({$additional_categories}))";
		}

        if (!empty($brands)) {
			$where[] = "p.brand_id IN (" . implode(',', $brands) . ")";
        }
		
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (" . implode(' OR ', $where) . ") 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	} */

	public function getBestSellersProducts()
	{
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.product_bestseller = 1 
							 ORDER BY p.product_rating DESC, p.product_id DESC 
							 LIMIT 0,4")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getSalesProducts()
	{
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.product_sale = 1 
							 ORDER BY p.product_rating DESC, p.product_id DESC 
							 LIMIT 0,20")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}
	
	public function getCategoryProductTags($category_id)
	{
		$tags = Yii::app()->db
			->createCommand("SELECT t.*, tl.* 
							 FROM category_tag as ct 
							 JOIN tag as t 
							 ON ct.tag_id = t.tag_id
							 JOIN tag_lang as tl 
							 ON t.tag_id = tl.tag_id AND tl.language_code = :code 
							 WHERE ct.category_id = :category_id 
							 GROUP BY t.tag_id
							 ORDER BY tl.tag_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($tags)) {
			foreach ($tags as $id => $tag) {
				$tags[$id]['total'] = Yii::app()->db
					->createCommand("SELECT COUNT(*)  
									FROM product_tag as pt 
									JOIN product as p 
									ON pt.product_id = p.product_id 
									WHERE pt.tag_id = :tag_id AND p.active = 1 
									GROUP BY pt.tag_id")
					->bindValue(':tag_id', (int) $tag['tag_id'], PDO::PARAM_INT)
					->queryScalar();
			}
		}
			
		return $tags;
	}

    public function getTagProductsTotal($per_page, $tag_id)
    {
        $total = Yii::app()->db
            ->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 JOIN product_tag as pt 
							 ON pt.product_id = p.product_id 
                             WHERE p.active = 1 AND pt.tag_id = :tag_id")
            ->bindValue(':tag_id', (int) $tag_id, PDO::PARAM_INT)
            ->queryScalar();

        return array(
            'total' => (int) $total,
            'pages' => ceil($total / $per_page),
        );
    }

	public function getCategoryProductsTotal($per_page, $category_id, $facets)
	{
		$categories = Yii::app()->params->categories;
		$categories_list = !empty($categories[$category_id]['children']) ? implode(',', $categories[$category_id]['children']) : $category_id;
		
		$join = '';
		$where = '';
		$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . $categories_list . ")";

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 {$join}
							 WHERE p.active = 1 AND (p.category_id IN (" . $categories_list . ") OR p.product_id IN ({$additional_categories})) {$where}")
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}

    public function getTagProducts($offset, $per_page, $tag_id, $sort)
    {
        switch ($sort) {
            case 'price':
                $order_by = 'p.product_instock, p.product_price';
                break;
            case 'newest':
                $order_by = 'p.product_instock, p.product_published DESC, p.product_id DESC';
                break;
            default:
                $order_by = 'p.product_instock, p.product_rating DESC, p.product_published DESC, p.product_id DESC';
        }

        $products = Yii::app()->db
            ->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1  
							 JOIN product_tag as pt 
							 ON pt.product_id = p.product_id 
                             WHERE p.active = 1 AND pt.tag_id = :tag_id  
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
            ->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
            ->bindValue(':tag_id', (int) $tag_id, PDO::PARAM_INT)
            ->queryAll();

        $products = $this->getProductsVariants($products);
        $products = $this->getProductsBadges($products);
        $products = $this->getListProductsProperties($products);

        return $products;
    }

	public function getCategoryProducts($offset, $per_page, $category_id, $facets, $sort)
	{
		$categories = Yii::app()->params->categories;
		$categories_list = !empty($categories[$category_id]['children']) ? implode(',', $categories[$category_id]['children']) : $category_id;

		$join = '';
		$where = '';
		$additional_categories = "SELECT product_id FROM category_product WHERE category_id IN (" . $categories_list . ")";

		switch ($sort) {
			case 'price':
				$order_by = 'p.product_instock, p.product_price';
				break;
			case 'newest':
				$order_by = 'p.product_instock, p.product_published DESC, p.product_id DESC';
				break;
			default:
				$order_by = 'p.product_instock, p.product_rating DESC, p.product_published DESC, p.product_id DESC';
		}

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1  
							 {$join}
							 WHERE p.active = 1 AND (p.category_id IN (" . $categories_list . ") OR p.product_id IN ({$additional_categories})) {$where} 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			// ->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->queryAll();
		
		$products = $this->getProductsVariants($products);
		$products = $this->getProductsBadges($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getBrandProductsTotal($per_page, $brand_id, $facets)
	{
		$join = '';
		$where = '';

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$total = Yii::app()->db
			->createCommand("SELECT COUNT(*)
							 FROM product as p 
							 {$join}
							 WHERE p.active = 1 AND p.brand_id = :brand_id {$where}")
			->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
			->queryScalar();
			
		return array(
			'total' => (int) $total,
			'pages' => ceil($total / $per_page),
		);
	}

	public function getBrandProducts($offset, $per_page, $brand_id, $facets, $sort)
	{
		$join = '';
		$where = '';

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		if ($facets->hasFilter()) {
			$facets_sql = $facets->getFacetFilterSql();

			$join = $facets_sql['join'];
			$where = ' AND ' . $facets_sql['where'];
		}

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 {$join}
							 WHERE p.active = 1 AND p.brand_id = :brand_id {$where} 
							 ORDER BY {$order_by} 
							 LIMIT {$offset},{$per_page}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':brand_id', (int) $brand_id, PDO::PARAM_INT)
			->queryAll();

		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	public function getCategoryCollectionProducts($category_id, $collection_id)
	{
		$order_by = 'p.product_rating DESC, p.product_id DESC';

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.category_id = :category_id AND p.collection_id = :collection_id 
							 ORDER BY {$order_by}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
			->queryAll();
			
		return $products;
	}

	public function getCollectionProducts($collection_id, $product_id)
	{
		$order_by = 'p.product_rating DESC, p.product_id DESC';

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND p.collection_id = :collection_id AND p.product_id != :product_id 
							 ORDER BY {$order_by}
							 LIMIT 0,10")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':collection_id', (int) $collection_id, PDO::PARAM_INT)
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();
			
		return $products;
	}

	public function getRelatedProducts($product_id, $category_id)
	{
		if (empty($category_id)) {
			return array();
		}
		
		$order_by = 'p.product_id DESC';

		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							FROM product as p 
							JOIN product_lang as pl 
							ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							WHERE p.active = 1 AND p.category_id = :category_id AND p.product_id != :product_id AND p.product_instock = 'in_stock' AND p.product_stock_type = 'store'
							ORDER BY {$order_by}
							LIMIT 0,40")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':category_id', (int) $category_id, PDO::PARAM_INT)
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		shuffle($products);
		$products = array_slice($products, 0, 5);

		$products = $this->getProductsVariants($products);
		$products = $this->getProductsBadges($products);
		$products = $this->getListProductsProperties($products);
			
		return $products;
	}

	public function getProductsByIds($product_ids, $get_cart_properties = false)
	{
		$products = array();

		$products_list = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND p.product_id IN (" . implode(',', $product_ids) . ")")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($products_list)) {
			$product_ids = array();

			foreach ($products_list as $product) {
				$product_id = $product['product_id'];

				$products[$product_id] = $product;
				$products[$product_id]['properties'] = array();

				if ($product['product_price_type'] != 'variants') {
					$product_ids[] = $product_id;
				}
			}

			if ($get_cart_properties && !empty($product_ids)) {
				// get product properties
				$products = $this->getProductsProperties($products, $product_ids);
			}
		}
			
		return $products;
	}

	public function getProductsBadgesList($product_ids)
	{
		$badges = [];
		
		$badges_list = Yii::app()->db
			->createCommand("SELECT pb.product_id, b.*, bl.badge_name, bl.badge_description 
							 FROM product_badge as pb 
							 JOIN badge as b 
							 ON pb.badge_id = b.badge_id 
							 JOIN badge_lang as bl 
							 ON b.badge_id = bl.badge_id AND bl.language_code = :code AND bl.badge_visible = 1 
							 WHERE pb.product_id IN (" . implode(',', $product_ids) . ")  
							 ORDER BY b.badge_icon_only, b.badge_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($badges_list)) {
			foreach ($badges_list as $badge) {
				$product_id = $badge['product_id'];
				$badges[$product_id][] = $badge;
			}
		}

		return $badges;
	}

	public function getProductsPropertiesList($product_ids)
	{
		$properties = [];
		
		$properties_list = Yii::app()->db
			->createCommand("SELECT pp.product_id, p.*, pl.property_title, pl.property_selector, v.value_id, v.value_icon, vl.value_title 
							 FROM property_product as pp 
							 JOIN property as p 
							 ON pp.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id IN (" . implode(',', $product_ids) . ")  
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$product_id = $property['product_id'];
				$properties[$product_id][] = $property;
			}
		}

		return $properties;
	}

	public function getProductsProperties($products, $product_ids)
	{
		$properties_list = Yii::app()->db
			->createCommand("SELECT pp.product_id, p.property_id, pl.property_title, pl.property_selector, v.value_id, vl.value_title 
							 FROM property_product as pp 
							 JOIN property as p 
							 ON pp.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id IN (" . implode(',', $product_ids) . ") AND p.property_cart = 1 
							 ORDER BY p.property_top DESC, pl.property_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($properties_list)) {
			foreach ($properties_list as $property) {
				$product_id = $property['product_id'];

				if (isset($products[$product_id])) {
					$products[$product_id]['properties'][] = $property;
				}
			}
		}

		return $products;
	}

	public function getProductsByKeyword($keyword, $sort)
	{
		/*
		$keyword_plus = '+' . preg_replace('#\s+#u', ' +', $keyword);
		$keyword_quoted = '"' . $keyword . '"';
		$keyword_sql = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';
		*/

		$keyword = addcslashes($keyword, '%_');
		$keyword_sql = preg_replace('#\s+#u', '%', $keyword);

		switch ($sort) {
			case 'price-asc':
				$order_by = 'p.product_price';
				break;
			case 'price-desc':
				$order_by = 'p.product_price DESC';
				break;
			default:
				$order_by = 'p.product_rating DESC, p.product_id DESC';
		}

		/*
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 AND MATCH (pl.product_title) AGAINST (:keyword IN BOOLEAN MODE) 
							 ORDER BY MATCH (pl.product_title) AGAINST (:keyword IN BOOLEAN MODE) DESC")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':keyword', $keyword_sql, PDO::PARAM_STR)
			->queryAll();
		*/
	
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 LEFT JOIN brand as b 
							 ON p.brand_id = b.brand_id AND b.active = 1 
							 LEFT JOIN brand_lang as bl 
							 ON b.brand_id = bl.brand_id AND bl.language_code = :code AND bl.brand_visible = 1 
							 WHERE p.active = 1 AND (p.product_sku = :sku OR pl.product_brand LIKE :keyword) 
							 ORDER BY {$order_by}")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':sku', $keyword, PDO::PARAM_STR)
			->bindValue(':keyword', '%' . $keyword_sql . '%', PDO::PARAM_STR)
			->queryAll();
		
		$products = $this->getProductsVariants($products);
			
		return $products;
	}

	public function getSearchProducts($product_ids)
	{
		$products = Yii::app()->db
			->createCommand("SELECT {$this->getCatalogFields()} 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1  
							 WHERE p.active = 1 AND p.product_id IN (" . implode(',', $product_ids) . ") 
							 ORDER BY FIELD (p.product_id," . implode(',', $product_ids) . ")")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
		
		$products = $this->getProductsVariants($products);
		$products = $this->getProductsBadges($products);
		$products = $this->getListProductsProperties($products);

		return $products;
    }

	private function getProductsVariants($products)
	{
		if ($this->product_extra_info && Yii::app()->params->settings['quick_buy'] && !empty($products)) {
			// get variants for products
			$product_ids = array_column($products, 'product_id');

            foreach ($products as $index => $product) {
                if ($product['product_price_type'] == 'variants') {
					$variants = $this->getProductVariants($product['product_id']);
					$products[$index] = array_merge($products[$index], $variants);
                }
            }
		}

		return $products;
	}

	private function getProductsSizes($product_ids)
	{
		$sizes = [];
		
		$varaint_fields = [
			'v.variant_id',
			'v.product_id',
			'v.variant_sku',
			'v.variant_price',
			'v.variant_price_old',
			'v.variant_instock',
			'v.variant_stock_qty',
			'pvl.value_title',
		];
		
		$sizes_list = Yii::app()->db
			->createCommand("SELECT " . implode(', ', $varaint_fields) . " 
							 FROM product_variant as v 
							 JOIN product_variant_value as vv 
							 ON v.variant_id = vv.variant_id 
							 JOIN property_value as pv 
							 ON vv.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code AND pvl.value_visible = 1 
							 JOIN property as p 
							 ON pv.property_id = p.property_id 
							 WHERE v.product_id IN (" . implode(',' , $product_ids) . ") AND p.property_size = 1 
							 ORDER BY v.position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		foreach ($sizes_list as $size) {
			$product_id = $size['product_id'];
			$variant_id = $size['variant_id'];

			$sizes[$product_id][$variant_id] = $size;
		}

		return $sizes;
	}

	private function getProductsBadges($products)
	{
		if (!empty($products)) {
			// get badges for products
			$product_ids = array_column($products, 'product_id');

			$badges = $this->getProductsBadgesList($product_ids);

			if (!empty($badges)) {
				foreach ($products as $index => $product) {
					if (isset($badges[$product['product_id']])) {
						$products[$index]['badges'] = $badges[$product['product_id']];
					}
				}
			}
		}

		return $products;
	}

	private function getListProductsProperties($products)
	{
		if ($this->product_extra_info && !empty($products)) {
			// get properties for products
			$product_ids = array_column($products, 'product_id');

			$properties = $this->getProductsPropertiesList($product_ids);

			if (!empty($properties)) {
				foreach ($products as $index => $product) {
					if (isset($properties[$product['product_id']])) {
						$products[$index]['props'] = $properties[$product['product_id']];
					}
				}
			}
		}

		return $products;
	}

	public function getProductById($product_id)
	{
		$product = Yii::app()->db
			->createCommand("SELECT * FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND p.product_id = :product_id LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryRow();

		return $product;
	}

	public function getProductByAlias($alias)
	{
		$product = Yii::app()->db
			->createCommand("SELECT * FROM product as p JOIN product_lang as pl ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 WHERE p.active = 1 AND p.product_alias = :alias LIMIT 1")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':alias', $alias, PDO::PARAM_STR)
			->queryRow();

		return $product;
	}

	public function getProductBadges($product_id)
	{
		$product_badges = Yii::app()->db
			->createCommand("SELECT pb.product_id, b.*, bl.badge_name, bl.badge_description 
							 FROM product_badge as pb 
							 JOIN badge as b 
							 ON pb.badge_id = b.badge_id 
							 JOIN badge_lang as bl 
							 ON b.badge_id = bl.badge_id AND bl.language_code = :code AND bl.badge_visible = 1 
							 WHERE pb.product_id = :id  
							 ORDER BY b.badge_icon_only, b.badge_position")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		return $product_badges;
	}

	public function getProductColors($product_id)
	{
		$product_colors = [];
		
		$refs = Yii::app()->db
			->createCommand("SELECT pc.ref_id 
							 FROM product_color as pc 
							 WHERE pc.product_id = :id")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryColumn();

		if (!empty($refs)) {
			$refs[] = $product_id;

			$product_colors = Yii::app()->db
				->createCommand("SELECT product.product_id, product.product_alias, pl.product_title, pv.value_color, pv.value_multicolor, pvl.value_title  
								FROM property_product as pp 
								JOIN property as p
								ON pp.property_id = p.property_id AND p.property_color = 1
								JOIN property_value as pv 
								ON pp.value_id = pv.value_id
								JOIN property_value_lang as pvl 
								ON pv.value_id = pvl.value_id AND pvl.language_code = :code 
								JOIN product as product 
								ON pp.product_id = product.product_id AND product.active = 1 
								JOIN product_lang as pl 
								ON product.product_id = pl.product_id AND pl.language_code = :code
								WHERE pp.product_id IN (" . implode(',', $refs) . ")
								ORDER BY pvl.value_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();
		}

		return $product_colors;
	}

	public function getProductStores($product_id)
	{
		$product_stores = Yii::app()->db
			->createCommand("SELECT ps.*, sl.store_name 
							 FROM product_store as ps 
							 JOIN store s 
							 ON ps.store_id = s.store_id AND s.store_type = 'warehouse' 
							 JOIN store_lang as sl
							 ON s.store_id = sl.store_id AND sl.language_code = :code AND sl.store_visible = 1 
							 WHERE ps.product_id = :id
							 ORDER BY sl.store_name")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		return $product_stores;
	}

	public function getProductStocks($product_id, $store_code = null)
	{
		if ($store_code !== null) {
			$order_by = "FIELD(s.store_code,'{$store_code}') DESC, s.store_position";
		} else {
			$order_by = "s.store_position";
		}
		
		$product_stocks = Yii::app()->db
			->createCommand("SELECT ps.quantity, s.* 
							 FROM product_store as ps 
							 JOIN store s 
							 ON ps.store_id = s.store_id  
							 WHERE ps.product_id = :id
							 ORDER BY {$order_by}")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		return $product_stocks;
	}

	public function getProductProperties($product_id)
	{
		$product_properties = array();

		$values = Yii::app()->db
			->createCommand("SELECT pp.property_id, v.value_id, v.value_icon, vl.value_title 
							 FROM property_product as pp 
							 JOIN property_value as v 
							 ON pp.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 WHERE pp.product_id = :id 
							 ORDER BY v.value_top DESC, v.value_position, vl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		if (!empty($values)) {
			$property_ids = array_column($values, 'property_id');

			$properties_list = Yii::app()->db
				->createCommand("SELECT p.*, pl.property_title, pl.property_selector 
								 FROM property as p 
								 JOIN property_lang as pl 
								 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1 
								 WHERE p.property_id IN (" . implode(',', $property_ids) . ")  
								 ORDER BY p.property_top DESC, pl.property_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($properties_list)) {
				foreach ($properties_list as $property) {
					$property_id = $property['property_id'];

					$product_properties[$property_id] = $property;
				}

				foreach ($values as $value) {
					$property_id = $value['property_id'];

					if (isset($product_properties[$property_id])) {
						$product_properties[$property_id]['value_id'] = $value['value_id'];
						$product_properties[$property_id]['value_title'] = $value['value_title'];
					}
				}
			}
		}

		return $product_properties;
	}

	public function getProductPhotos($product_id)
	{
		$product_photos = Yii::app()->db
			->createCommand("SELECT * FROM product_photo WHERE product_id = :id ORDER BY position")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		return $product_photos;
	}

	public function getProductVariants($product_id)
	{
		$product_variants = array(
			'variants' => array(),
			'properties' => array(),
			'values' => array(),
		);

		$variants_list = Yii::app()->db
			->createCommand("SELECT pv.variant_id, pv.variant_sku, pv.variant_price, pv.variant_price_old, pv.variant_price_type, pv.variant_instock 
							 FROM product_variant as pv 
							 WHERE pv.product_id = :id 
							 ORDER BY pv.position")
			->bindValue(':id', (int) $product_id, PDO::PARAM_STR)
			->queryAll();

		if (!empty($variants_list)) {
			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];
				
				$product_variants['variants'][$variant_id] = $variant;
				$product_variants['variants'][$variant_id]['sizes'] = array();
				$product_variants['variants'][$variant_id]['values'] = array();
				$product_variants['variants'][$variant_id]['photos'] = array();
				$product_variants['variants'][$variant_id]['stores'] = array();
			}

			$variant_ids = array_keys($product_variants['variants']);

			$variant_values = Yii::app()->db
				->createCommand("SELECT pvv.variant_id, p.property_size, v.value_id, v.property_id, vl.value_title 
								 FROM product_variant_value as pvv 
								 JOIN property_value as v 
								 ON pvv.value_id = v.value_id 
								 JOIN property_value_lang as vl 
								 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
								 JOIN property as p 
								 ON v.property_id = p.property_id 
								 WHERE pvv.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY v.value_top DESC, v.value_position, vl.value_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($variant_values)) {
				$values = array();
			
				foreach ($variant_values as $variant_value) {
					$variant_id = $variant_value['variant_id'];
					$property_id = $variant_value['property_id'];
					$value_id = $variant_value['value_id'];
					
					if (!isset($values[$property_id][$value_id])) {
						$values[$property_id][$value_id] = array(
							'value_id' => $value_id,
							'value_title' => $variant_value['value_title'],
						);
					}

					$product_variants['variants'][$variant_id]['values'][$property_id] = $value_id;

					if ($variant_value['property_size']) {
						$product_variants['variants'][$variant_id]['sizes'][] = $value_id;
					}
				}

				$property_ids = array_keys($values);

				$properties = Yii::app()->db
					->createCommand("SELECT p.property_id, pl.property_title, pl.property_selector  
									 FROM property as p 
									 JOIN property_lang as pl 
									 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
									 WHERE p.property_id IN (" . implode(',', $property_ids) . ")
									 ORDER BY p.property_top DESC, pl.property_title")
					->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
					->queryAll();

				foreach ($properties as $property) {
					$property_id = $property['property_id'];

					if (isset($values[$property_id])) {
						$product_variants['properties'][$property_id] = $property;
						$product_variants['properties'][$property_id]['values'] = $values[$property_id];
					}
				}
			}

			// get variant photos
			$variant_photos = Yii::app()->db
				->createCommand("SELECT variant_id, photo_path, photo_size FROM product_variant_photo WHERE variant_id IN (" . implode(',', $variant_ids) . ") ORDER BY position")
				->queryAll();

			foreach ($variant_photos as $variant_photo) {
				$variant_id = $variant_photo['variant_id'];

				if (isset($product_variants['variants'][$variant_id])) {
					$product_variants['variants'][$variant_id]['photos'][] = $variant_photo;
				}
			}
			
			// get variant stores
			$variant_stores = Yii::app()->db
				->createCommand("SELECT vs.*, sl.store_name 
								 FROM product_variant_store as vs 
								 JOIN store s 
								 ON vs.store_id = s.store_id AND s.store_type = 'warehouse' 
								 JOIN store_lang as sl
								 ON s.store_id = sl.store_id AND sl.language_code = :code AND sl.store_visible = 1 
								 WHERE vs.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY sl.store_name")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			foreach ($variant_stores as $variant_store) {
				$variant_id = $variant_store['variant_id'];

				if (isset($product_variants['variants'][$variant_id])) {
					$product_variants['variants'][$variant_id]['stores'][] = $variant_store;
				}
			}
		}

		return $product_variants;
	}

	public function getProductVariantsStocks($variant_id, $store_code = null)
	{
		if ($store_code !== null) {
			$order_by = "FIELD(s.store_code,'{$store_code}') DESC, s.store_position";
		} else {
			$order_by = "s.store_position";
		}
		
		$variant_stocks = Yii::app()->db
			->createCommand("SELECT pvs.quantity, s.* 
							 FROM product_variant_store as pvs 
							 JOIN store s 
							 ON pvs.store_id = s.store_id  
							 WHERE pvs.variant_id = :id
							 ORDER BY {$order_by}")
			->bindValue(':id', (int) $variant_id, PDO::PARAM_STR)
			->queryAll();

		return $variant_stocks;
	}

	public function getProductOptions($product_id)
	{
		$product_options = array(
			'options' => array(),
			'properties' => array(),
		);

		$options = Yii::app()->db
			->createCommand("SELECT po.*, p.property_id, pl.property_title, pl.property_selector, pv.value_id, pvl.value_title 
							 FROM product_option as po 
							 JOIN property_value as pv 
							 ON po.value_id = pv.value_id 
							 JOIN property_value_lang as pvl 
							 ON pv.value_id = pvl.value_id AND pvl.language_code = :code 
							 JOIN property as p 
							 ON pv.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code 
							 WHERE po.product_id = :product_id 
							 ORDER BY p.property_top DESC, pl.property_title, pv.value_top DESC, pv.value_position, pvl.value_title")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($options)) {
			foreach ($options as $option) {
				$product_options['options'][] = array(
					'option_id' => $option['option_id'],
					'option_price' => $option['option_price'],
					'value_id' => $option['value_id'],
				);

				$property_id = $option['property_id'];
				$value_id = $option['value_id'];

				if (!isset($product_options['properties'][$property_id])) {
					$product_options['properties'][$property_id] = array(
						'property_id' => $property_id,
						'property_title' => $option['property_title'],
						'property_title' => $option['property_selector'],
						'values' => array(
							$value_id => array(
								'value_id' => $value_id,
								'value_title' => $option['value_title'],
							),
						),
					);
				} else {
					$product_options['properties'][$property_id]['values'][$value_id] = array(
						'value_id' => $value_id,
						'value_title' => $option['value_title'],
					);
				}
			}
		}

		return $product_options;
	}

	public function getProductVariant($product_id, $variant_id)
	{
		$variant = Yii::app()->db
			->createCommand("SELECT * FROM product_variant WHERE product_id = :product_id AND variant_id = :variant_id LIMIT 1")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->bindValue(':variant_id', (int) $variant_id, PDO::PARAM_INT)
			->queryRow();

		return $variant;
	}

	public function getProductOptionsData($product_id, $option_ids)
	{
		$options = Yii::app()->db
			->createCommand("SELECT * FROM product_option WHERE product_id = :product_id AND option_id IN (" . implode(',', $option_ids) . ")")
			->bindValue(':product_id', (int) $product_id, PDO::PARAM_INT)
			->queryAll();

		return $options;
	}

	public function getVariantsByIds($variant_ids)
	{
		$variants = array();

		$variants_list = Yii::app()->db
			->createCommand("SELECT pv.*, pvp.photo_path, pvp.photo_size 
							 FROM product_variant as pv 
							 LEFT JOIN product_variant_photo as pvp 
							 ON pv.variant_id = pvp.variant_id 
							 LEFT JOIN product_variant_photo as pvp_filter 
							 ON pvp.variant_id = pvp_filter.variant_id AND pvp.position > pvp_filter.position 
							 WHERE pv.variant_id IN (" . implode(',', $variant_ids) . ") AND pvp_filter.photo_id IS NULL")
			->queryAll();

		if (!empty($variants_list)) {
			foreach ($variants_list as $variant) {
				$variant_id = $variant['variant_id'];

				$variants[$variant_id] = $variant;
				$variants[$variant_id]['values'] = array();
			}

			// get properties and their values
			$variant_ids = array_keys($variants);

			$variant_values = Yii::app()->db
				->createCommand("SELECT pvv.variant_id, p.property_id, pl.property_title, pl.property_selector, v.value_id, vl.value_title 
								 FROM product_variant_value as pvv 
								 JOIN property_value as v 
								 ON pvv.value_id = v.value_id 
								 JOIN property_value_lang as vl 
								 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
								 JOIN property as p 
								 ON v.property_id = p.property_id 
								 JOIN property_lang as pl 
								 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
								 WHERE pvv.variant_id IN (" . implode(',', $variant_ids) . ")
								 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, v.value_position, vl.value_title")
				->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
				->queryAll();

			if (!empty($variant_values)) {
				foreach ($variant_values as $variant_value) {
					$variant_id = $variant_value['variant_id'];

					if (isset($variants[$variant_id])) {
						$variants[$variant_id]['values'][] = $variant_value;
					}
				}
			}
		}
			
		return $variants;
	}

	public function getOptionsByIds($option_ids)
	{
		$options = array();

		$options_list = Yii::app()->db
			->createCommand("SELECT po.*, p.property_id, pl.property_title, pl.property_selector, v.value_id, vl.value_title 
							 FROM product_option as po 
							 JOIN property_value as v 
							 ON po.value_id = v.value_id 
							 JOIN property_value_lang as vl 
							 ON v.value_id = vl.value_id AND vl.language_code = :code AND vl.value_visible = 1 
							 JOIN property as p 
							 ON v.property_id = p.property_id 
							 JOIN property_lang as pl 
							 ON p.property_id = pl.property_id AND pl.language_code = :code AND pl.property_visible = 1
							 WHERE po.option_id IN (" . implode(',', $option_ids) . ")
							 ORDER BY p.property_top DESC, pl.property_title, v.value_top DESC, v.value_position, vl.value_title")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();

		if (!empty($options_list)) {
			foreach ($options_list as $option) {
				$option_id = $option['option_id'];
				$options[$option_id] = $option;
			}
		}
			
		return $options;
	}

	public static function getDiscountPrice($product_price, $category_id = 0, $brand_id = 0)
	{
		$discount_data = array(
			'price' => $product_price,
			'discount' => 0,
		);

		$categories = Yii::app()->params->categories;
		$brands = Yii::app()->params->brands;

		$category_discount = 0;
		$brand_discount = 0;

		if (isset($categories[$category_id]) && !empty($categories[$category_id]['category_discount'])) {
			$category_discount = $categories[$category_id]['category_discount'];
		}
		
		if (isset($brands[$brand_id]) && !empty($brands[$brand_id]['brand_discount'])) {
			$brand_discount = $brands[$brand_id]['brand_discount'];
		}

		$discount = ($brand_discount > $category_discount) ? $brand_discount : $category_discount;

		if ($discount) {
			$discount_price = $product_price * (100 - $discount) / 100;
			$discount_price = round($discount_price, 2);
			
			$discount_data = array(
				'price' => $discount_price,
				'discount' => $discount,
			);
		}

		/* $user_discount = User::getUserDiscount();
		$categories = Yii::app()->params->categories;

		if ($user_discount && isset($categories[$category_id]) && $categories[$category_id]['category_discount']) {
			$category_discount = (int) $categories[$category_id]['category_discount'];
			$product_discount = $user_discount > $category_discount ? $category_discount : $user_discount;
			$discount_price = $product_price * (100 - $product_discount) / 100;
			$discount_price = round($discount_price, 2);

			$discount_data = array(
				'price' => $discount_price,
				'discount' => $product_discount,
			);
		} */

		return $discount_data;
	}

	public function getProductsSitemap()
	{
		$products = Yii::app()->db
			->createCommand("SELECT p.product_id, p.product_alias, pl.product_title, pl.product_no_index 
							 FROM product as p 
							 JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE p.active = 1 
							 ORDER BY p.product_id")
			->bindValue(':code', Yii::app()->language, PDO::PARAM_STR)
			->queryAll();
			
		return $products;
	}
}