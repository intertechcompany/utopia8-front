<?php
use GuzzleHttp\Client;

/**
 * Api class
 */
class Api
{
	const RETAIL_PRICE_TYPE = '000000001';
	const STORE_KHARKIV = '000000001';
	const STORE_ODESA = '000000003';

	private $client;
	private $product_model;
	private $stores = [];
	
	public function __construct()
	{
		ini_set('memory_limit', '512M');
		set_time_limit(10 * 60);
		
		// GuzzleHttp
		require_once Yii::getPathOfAlias('application.vendor.GuzzleHttp') . DS . 'autoload.php';

		$this->client = new Client([
			'timeout' => 15.0,
		]);

		$this->product_model = Product::model();
	}
	
	public function createOrder($order, $order_items)
	{
		$order_data = $this->prepareOrderData($order, $order_items);
		Yii::log(print_r($order_data, true), 'error', 'RetailCRM');

        if (!Yii::app()->params->dev) {
            $result = $this->request('POST', '/orders/create', [
                'site' => '000000001',
                'order' => json_encode($order_data),
            ]);

            Yii::log(print_r($result, true), 'error', 'RetailCRM');
        }
	}

	private function prepareOrderData($order, $order_items)
	{
		$how_to_ship = 'other';
		$shipping_details = '';
		$payment = '';

		if ($order['delivery'] == 1) {
			if ($order['np_city'] == 'Одеса' || $order['np_city'] == 'Одесса') {
				$how_to_ship = 'pickupod';
			} elseif ($order['np_city'] == 'Харків' || $order['np_city'] == 'Харьков') {
				$how_to_ship = 'pickukh';
			}
		} elseif ($order['delivery'] == 2) {
			$how_to_ship = 'npoffice';
			$shipping_details = $order['np_city'] . ', ' . $order['np_department'];
		} elseif ($order['delivery'] == 3) {
			$how_to_ship = 'npaddress';
			$shipping_details = $order['np_city'] . ', ' . $order['address'];
		}

        if ($order['payment'] == 1) {
			$payment = 'bank-card';
        } elseif ($order['payment'] == 2) {
			$payment = 'lp';
		} elseif ($order['payment'] == 3) {
			$payment = 'cash';
		}

		$items = $this->getOrderItems($how_to_ship, $order_items);
		
		if (count($this->stores) > 1) {
			$store_code = self::STORE_KHARKIV;
		} else {
			reset($this->stores);
			$store_code = key($this->stores);
		}
		
		$order_data = [
			'site' => '000000001',
			'number' => $order['order_id'],
			'countryIso' => 'UA',
			'createdAt' => $order['created'],
			'lastName' => $order['last_name'],
			'firstName' => $order['first_name'],
			'phone' => str_replace(' ', '', $order['phone']),
			'email' => $order['email'],
			'customerComment' => $order['comment'],
			'contragent' => [
				'contragentType' => 'individual',
			],
			'customFields' => [
				'how_to_communicate' => $order['zip'],
				'how_to_ship' => $how_to_ship,
				'shipping_details' => $shipping_details,
				'card_text' => $order['wishes'],
			],
			'orderType' => 'eshop-individual',
			'orderMethod' => 'shopping-cart',
			'managerId' => 8,
			'status' => 'new',
			'shipmentStore' => $store_code,
			'items' => $items,
			'payments' => [
				[
					'amount' => $order['price'],
					'type' => $payment,
				]
			],
		];

		return $order_data;
	}

	private function getOrderItems($how_to_ship, $order_items)
	{
		$store_code = null;
		
		if ($how_to_ship == 'pickupod') {
			$store_code = self::STORE_ODESA;
		} elseif ($how_to_ship == 'pickukh') {
			$store_code = self::STORE_KHARKIV;
		}
		
		$items = [];

		foreach ($order_items as $order_item) {
			$key = '';
			$offer_id = 0;
			
			if ($order_item['variant_id'] > 0 ) {
				$key = 'v_' . $order_item['variant_id'];
				$offer_id = $order_item['variant_sku'];
				$stocks = $this->product_model->getProductVariantsStocks($order_item['variant_id'], $store_code);
			} else {
				$key = 'p_' . $order_item['product_id'];
				$offer_id = $order_item['offer_id'];
				$stocks = $this->product_model->getProductStocks($order_item['product_id'], $store_code);
			}

			if (count($stocks) == 1) {
				$store_id = $stocks[0]['store_code'];
				$this->stores[$store_id][$key] = $order_item['quantity'];
			} else {
				foreach ($stocks as $index => $stock) {
					$store_id = $stock['store_code'];
					
					if ($index == 0 && $store_code !== null && $order_item['quantity'] <= $stock['quantity']) {
						$this->stores[$store_id][$key] = $order_item['quantity'];
						break;
					}

					if ($stock['quantity'] > 0) {
						if (!isset($this->stores[$store_id][$key])) {
							$quantity = $order_item['quantity'] > $stock['quantity'] ? $stock['quantity'] : $order_item['quantity'];
							$this->stores[$store_id][$key] = $quantity;
						} else {
							$rest_quantity = $order_item['quantity'] - $this->stores[$store_id][$key];
							$quantity = $rest_quantity > $stock['quantity'] ? $stock['quantity'] : $rest_quantity;
							$this->stores[$store_id][$key] += $quantity;
						}
					}

					if ($this->stores[$store_id][$key] == $order_item['quantity']) {
						break;
					}
				}
			}
			
			$items[] = [
				'id' => $order_item['order_item_id'],
				'initialPrice' => $order_item['price'],
				'quantity' => $order_item['quantity'],
				'productName' => $order_item['title'],
				'offer' => [
					'id' => $offer_id,
				],
				'priceType' => [
					'code' => self::RETAIL_PRICE_TYPE,
				],
			];
		}

		return $items;
	}

	// TEST URL: https://slashdotdash.retailcrm.ru/api/v5/store/products?filter[sites][]=000000001&filter[manufacturer]=test&apiKey=Bj2DWQ4YB4tEVy112kxUdNREXTyjROds
	private function request($type = 'GET', $action, $data = [])
	{
		$base_url = 'https://slashdotdash.retailcrm.ru/api/v5';
		$api_key = Yii::app()->params->retail_crm['api_key'];
		
		try {
			if ($type == 'POST') {
				$url = $base_url . $action;
				
				$response = $this->client->post($url, [
					// 'headers' => [
					// 	'Content-Type'  => 'application/json',
					// ],
					'form_params' => array_merge($data, [
						'apiKey' => $api_key,
					]),
					// 'body' => json_encode($data),
				]);
			} else {
				$url = $base_url . $action . '?' . http_build_query(array_merge($data, [
					'apiKey' => $api_key,
				]));
				
				$response = $this->client->get($url, [
					'headers' => [
						'Content-Type'  => 'application/json',
					],
				]);
			}

			$data = json_decode($response->getBody(), true);

			if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
				Yii::log($response->getBody(), 'error', 'RetailCRM');
			} else {
				return $data;
			}
		} catch (GuzzleHttp\Exception\RequestException $e) {
			$exception = "Status code: " . $e->getCode() . ". Error message: " . $e->getMessage();

			if ($e->hasResponse()) {
				$exception .= $e->getResponse()->getBody();
			}

			Yii::log($exception, 'error', 'RetailCRM');
		}

		return false;	
	}
}