<?php
    $assetsUrl = Yii::app()->assetManager->getBaseUrl();
    $staticUrl = Yii::app()->assetManager->getBaseUrl() . '/static/' . Yii::app()->params->settings['rev'];
    $controller = Yii::app()->getController();
?>
<?php if (!in_array($controller->route, ['site/cart', 'site/checkout'])) { ?>
<section class="instagram">
    <div class="instagram__title"><?=Lang::t('layout.tip.instagram', ['{insta_url}' => CHtml::encode(Yii::app()->params->settings['instagram'])])?></div>
    <?php if (!empty($banners)) { ?>
    <div class="instagram__grid">
        <?php foreach ($banners as $banner) { ?>
        <div class="instagram__photo">
            <?php
                if (strpos($banner['banner_url'], '/') === 0) {
                    $banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
                }

                if (!empty($banner['banner_logo'])) {
                    $banner_image = json_decode($banner['banner_logo'], true);
                    $banner_image_url = $assetsUrl . '/banner/' . $banner_image['file'];
                } else {
                    $banner_image_url = '';
                }
            ?>
            <a href="<?=$banner['banner_url']?>" rel="nofollow"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
                <?php if (!empty($banner_image_url)) { ?>
                <picture>
                    <img class="lazyload" data-src="<?=$banner_image_url?>" alt="" width="<?=$banner_image['w']?>" height="<?=$banner_image['h']?>">
                </picture>
                <?php } ?>
            </a>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</section>
<!-- /.instagram -->
<?php } ?>

<footer class="footer">
    <div class="footer__column">
        <div class="footer__subtitle"><?=Lang::t('layout.tip.footerStore')?></div>
        <ul class="footer__list list-unstyled">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $category['category_alias']])?>"><?=CHtml::encode($category['category_name'])?></a></li>
            <?php } ?>
            <!-- <li><a href="<?=Yii::app()->createUrl('site/sale')?>">Sale</a></li> -->
        </ul>
    </div>
    <div class="footer__column">
        <div class="footer__subtitle"><?=Lang::t('layout.tip.footerInfo')?></div>
        <ul class="footer__list list-unstyled">
            <?php foreach ($pages as $page) { ?>
            <li><a href="<?=Yii::app()->createUrl('site/page', ['alias' => $page['page_alias']])?>"><?=CHtml::encode($page['page_title'])?></a></li>
            <?php } ?>
        </ul>
    </div>
    <div class="footer__column footer__column--extra">
        <div class="footer__subtitle"><?=Lang::t('layout.tip.footerSocial')?></div>
        <ul class="footer__list list-unstyled">
            <?php if (!empty(Yii::app()->params->settings['instagram'])) { ?>
            <li><a href="<?=CHtml::encode(Yii::app()->params->settings['instagram'])?>" target="_blank" rel="nofollow">Instagram</a></li>
            <?php } ?>
            <?php if (!empty(Yii::app()->params->settings['facebook'])) { ?>
            <li><a href="<?=CHtml::encode(Yii::app()->params->settings['facebook'])?>" target="_blank" rel="nofollow">Facebook</a></li>
            <?php } ?>
        </ul>

        <div class="footer__bottom">
            <div class="footer__subtitle"><?=Lang::t('layout.tip.footerPayments')?></div>
            <div class="footer__cards">
                <i class="icon-inline icon-visa"></i>
                <i class="icon-inline icon-mastercard"></i>
            </div>
        </div>
    </div>
    <div class="footer__column footer__column--extra">
        <div class="footer__subtitle"><?=Lang::t('layout.tip.footerSupport')?></div>
        <ul class="footer__list list-unstyled">
            <?php if (!empty(Yii::app()->params->settings['phone'])) { ?>
            <li><a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a></li>
            <?php } ?>
            <?php if (!empty(Yii::app()->params->settings['mail'])) { ?>
            <li><a href="mailto:<?=CHtml::encode(Yii::app()->params->settings['mail'])?>"><?=CHtml::encode(Yii::app()->params->settings['mail'])?></a></li>
            <?php } ?>
            <li><?=Lang::t('layout.tip.footerWorktime')?></li>
        </ul>

        <div class="footer__bottom footer__bottom--design">
            <?=Lang::t('layout.tip.footerDesign')?> <a href="https://otherland.studio/" target="_blank" rel="nofollow">Other Land</a><br>
            <?=Lang::t('layout.tip.footerDev')?> <a href="https://www.facebook.com/evgenii.syrotenko/" target="_blank" rel="nofollow">Syrotenko</a>
        </div>
    </div>
</footer>
<!-- /.footer -->