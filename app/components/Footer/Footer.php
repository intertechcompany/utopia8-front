<?php
class Footer extends CWidget
{ 
	public function run()
	{
		$pages = Page::model()->getBottomMenu();
		$categories = Yii::app()->params->categories_tree;
		$banners = Banner::model()->getInstagramBanners();

		$this->render('footer', [
			'pages' => $pages,
			'categories' => $categories,
			'banners' => $banners,
		]);
	}
}