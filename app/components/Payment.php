<?php
use WayForPay\SDK\Collection\ProductCollection;
use WayForPay\SDK\Credential\AccountSecretCredential;
use WayForPay\SDK\Domain\Client;
use WayForPay\SDK\Domain\Product;
use WayForPay\SDK\Wizard\PurchaseWizard;

class Payment extends CApplicationComponent
{
	public function __construct()
	{
        require_once Yii::getPathOfAlias('application.vendor.WayForPay') . DS . 'autoload.php';
    }
	
	public function getPaymentFormHtml($order_id, $amount, $order, $products)
	{
		$controller = Yii::app()->getController();
		
		$credential = new AccountSecretCredential(Yii::app()->params->wfp['account'], Yii::app()->params->wfp['secret']);

		$products_collection = [];

		foreach ($products as $product) {
			$product_title = $product['title'];

			if (!empty($product['variant_title']) || !empty($product['options_title'])) {
				$product_subtitle = '';

                if (!empty($product['variant_title'])) {
					$product_subtitle .= ' ' . $product['variant_title'];
				}
				
				if (!empty($product['options_title'])) {
					$product_subtitle .= ' ' . $product['options_title'];
                }

				$product_title .= ' (' . trim(htmlspecialchars_decode($product_subtitle, ENT_QUOTES)) . ')';
			}

			$products_collection[] = new Product($product_title, (float) $product['price'], (int) $product['quantity']);
		}

		$form = PurchaseWizard::get($credential)
			->setOrderReference($order_id)
			->setAmount($amount)
			->setCurrency('UAH')
			->setOrderDate(new \DateTime())
			->setMerchantDomainName(Yii::app()->request->getServerName())
			->setMerchantTransactionType('SALE')
			->setClient(new Client(
				$order['first_name'],
				$order['last_name'],
				$order['email'],
				$order['phone']
			))
			->setProducts(new ProductCollection($products_collection))
			->setReturnUrl($controller->createAbsoluteUrl('payment/result'))
			->setServiceUrl($controller->createAbsoluteUrl('payment/api'))
			->setLanguage(Yii::app()->language == 'uk' ? 'UA' : mb_strtoupper(Yii::app()->language, 'utf-8'))
			->getForm()
			->getAsString();
		
		return $form;
	}

	function callback($data)
	{
		Yii::log("API callback\n" . print_r($data, true), 'error', 'wfp');
		
		if (!isset($data['orderReference'])) {
			throw new PaymentException('Invalid order reference.');
		}
		
		if (isset($data['transactionStatus']) && $this->isValidSignature($data) === false) {
			throw new PaymentException('Invalid signature.');
		}

		$order_id = (int) str_replace('FB_', '', $data['orderReference']);

		$order_model = Order::model();
		$order = $order_model->getOrderById($order_id);

		if (empty($order)) {
			throw new PaymentException('Invalid order ID.');
		}

		if (in_array($order['status'], ['paid', 'completed', 'cancelled'])) {
			// do nothing
			return;
		}

		$payment_status = isset($data['transactionStatus']) ? $data['transactionStatus'] : '';

		if ($payment_status == 'Approved') {
			$status = 'paid';
		} elseif ($payment_status == 'InProcessing' || $payment_status == 'Pending') {
			$status = 'processing';
		} else {
			$status = 'payment_error';
		}
		
		$comment = json_encode($data);

		$order_model->updateStatus($order_id, $status, $comment);

		if ($status == 'paid') {			
			if (Yii::app()->params->settings['stock'] == 'payment') {
				$order_model->subtractOrderQty($order_id);
			}

			$order_products = $order_model->getOrderProducts($order_id);
			
			// send success order email
			$this->sendSuccessEmail($order, $order_products);
		}
	}

	public function response($data)
	{
		$time = time();
			
		$response = [
			"orderReference" => $data['orderReference'],
			"status" => 'accept',
			"time" => $time,
			"signature" => hash_hmac('md5', implode(';', [$data['orderReference'], 'accept', $time]), Yii::app()->params->wfp['secret']),
		];

		return json_encode($response);
	}

	public function result($data)
	{
		Yii::log("Result\n" . print_r($data, true), 'error', 'wfp');

		if (!isset($data['orderReference'])) {
			throw new PaymentException('Invalid order reference.');
		}
		
		if (isset($data['transactionStatus']) && $this->isValidSignature($data) === false) {
			throw new PaymentException('Invalid signature.');
		}

		$order_id = (int) str_replace('FB_', '', $data['orderReference']);

		$order_model = Order::model();
		$order = $order_model->getOrderById($order_id);

		if (empty($order)) {
			throw new PaymentException('Invalid order ID.');
		}

		$order_model->setThankYou(array(
			'order_id' => $order_id,
			'order_status' => $order['status'],
			'payment_status' => isset($data['transactionStatus']) ? $data['transactionStatus'] : '',
			'payment_message' => isset($data['reason']) ? $data['reason'] : '',
		));

		Yii::app()->getController()->redirect(Yii::app()->getController()->createUrl('site/thankyou'));
	}

	public function resultTest()
	{
		// Use test credential or yours
		$credential = new AccountSecretTestCredential();
		// $credential = new AccountSecretCredential('account', 'secret');

		$data = [
			$credential->getAccount(),
			'DH783023',
			'1547.36',
			'UAH',
			'541963',
			'41****8217',
			'Declined',
			'1101',
		];

		// echo hash_hmac('md5', implode(';', $data), $credential->getSecret());
		// return;

		try {
			$handler = new ServiceUrlHandler($credential);
			$response = $handler->parseRequestFromPostRaw();
			$transaction = $response->getTransaction();
			$order_id = $transaction->getOrderReference();

			echo $handler->getSuccessResponse($transaction);
		} catch (ApiException $e) {
			echo $e->getMessage();
		} catch (WayForPaySDKException $e) {
			echo "WayForPay SDK exception: " . $e->getMessage();
		}

		/* $data = \json_decode(file_get_contents('php://input'), TRUE);
		$_POST = $_REQUEST = $data;

		try {
			$handler = new ServiceUrlHandler($credential);
			$response = $handler->parseRequestFromGlobals();
			if ($response->getReason()->isOK()) {
				echo "Success";
			} else {
				echo "Error: " . $response->getReason()->getMessage();
			}
		} catch (WayForPaySDKException $e) {
			echo "WayForPay SDK exception: " . $e->getMessage();
		} */
	}

	private function isValidSignature($data)
	{
		$signature_fields = [
			Yii::app()->params->wfp['account'],
			$data['orderReference'],
			$data['amount'],
			$data['currency'],
			$data['authCode'],
			$data['cardPan'],
			$data['transactionStatus'],
			$data['reasonCode'],
		];

		$valid_signature = hash_hmac('md5', implode(';', $signature_fields), Yii::app()->params->wfp['secret']);

		return ($data['merchantSignature'] === $valid_signature) ? true : false;
	}

	private function sendSuccessEmail($order, $order_products)
	{
        $subject = Lang::t('email.subject.paidOrder', ['{order_id}' => $order['order_id']]);
        $body = Yii::app()->getController()->renderPartial('//email/order', [
            'order' => $order,
            'order_products' => $order_products,
        ], true);

		// send email to admins
		$mailer = new EsputnikMailer();
		$emails = explode(', ', Yii::app()->params->settings['notify_mail']);
		$mailer->send($emails, $subject, $body);

		// send email to client
		$mailer->send([$order['email']], $subject, $body);
	}
}

class PaymentException extends CException
{

}