<?php
/**
 * Currency class
 */
class Currency
{
	const BASE_CURRENCY = 'eur';
	
	public static function getValue($price)
	{
		$currency_code = Yii::app()->params->currency;
		$currency_rates = Yii::app()->params->settings['currency'];
		
		$price = $price * ($currency_rates[self::BASE_CURRENCY] / $currency_rates[$currency_code]);
		
		if (!empty($currency_rates['margin']) && $currency_code == 'uah') {
			$price = $price * (1 + $currency_rates['margin'] / 100);
		}
		
		$price = round($price, 2);

		return $price;
	}
	
	public static function format($price, $covert_rate = true, $currency_code = '')
	{
		$currency_code = (empty($currency_code) || !isset(Yii::app()->params->currencies[$currency_code])) ? Yii::app()->params->currency : $currency_code;
		$currency = Yii::app()->params->currencies[$currency_code];
		
		if ($covert_rate) {
			$price = self::getValue($price);
		}

		$formatted_currency = $currency['prefix'] . number_format($price, 2, '.', ' ') . $currency['suffix'];
		
		return $formatted_currency;
	}

	public static function formatExtended($params)
	{
		$currency_code = Yii::app()->params->currency;
		$currency = Yii::app()->params->currencies[$currency_code];

		$data = [];

		foreach ($params['data'] as $attribute => $value) {
			$data[] = 'data-' . $attribute . '="' . CHtml::encode($value) . '"';
		}

		$price = self::getValue($params['price']);
		$price = '<' . $params['tag'] . ' ' . implode(' ', $data) . '>' . number_format($price, 2, '.', ' ') . '</' . $params['tag'] . '>';

		$formatted_currency = $currency['prefix'] . $price . $currency['suffix'];
		
		return $formatted_currency;
	}
}