<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$controller = Yii::app()->getController();
?>
<aside id="cart" class="cart cart--sidebar cart--hidden">
	<div class="cart__overlay"></div>
	<div class="cart__container">
		<div class="cart-details wrap">
			<div class="cart-details__title"><?=Lang::t('cartSide.tip.cartTitle')?> <span><?=$total?></span></div>
			<button type="button" class="cart__close"></button>
			<div class="cart-products">
				<form action="<?=Yii::app()->createUrl('site/cart')?>" class="cart-products__list" method="post">
					<?php $controller->renderPartial('//site/cartList', [
                        'cart' => $cart,
                        'box_banner' => $box_banner,
                        'postcard_banner' => $postcard_banner,
                    ]); ?>
				</form>
                <!-- /.cart-products__list -->
                
                <?php $controller->renderPartial('//site/gifts', [
                    'box_banner' => $box_banner,
                    'postcard_banner' => $postcard_banner,
                ]); ?>

				<div class="cart-products__summary">
					<div class="cart__total">
						<span><?=Lang::t('cartSide.tip.total')?></span>
						<span><?=$price?> <?=Lang::t('layout.tip.uah')?></span>
					</div>

					<div class="cart__checkout"><a href="<?=Yii::app()->createUrl('site/checkout')?>" class="cart__checkout-btn<?php if (!empty($cart_item_disabled)) { ?> cart__checkout-btn--disabled<?php } ?>"><?=Lang::t('cartSide.btn.checkout')?></a></div>

					<?php /* <div class="cart__tip"><?=Lang::t('cartSide.tip.promocode')?></div> */ ?>
				</div>
            </div>
		</div>
	</div>
</aside>
<!-- /.cart -->