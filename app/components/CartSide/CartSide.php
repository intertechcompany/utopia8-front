<?php
class CartSide extends CWidget
{ 
	public function run()
	{
		$cart_model = Cart::model();

		$cart = $cart_model->getCartProducts();
		$total = $cart_model->getTotal();
		$price = $cart_model->getPrice();

		$categories = Yii::app()->params->categories;
		$box_banner = Banner::model()->getBoxBanner();
    	$postcard_banner = Banner::model()->getPostcardBanner();

		$this->render('cartSide', [
			'cart' => $cart,
			'total' => $total,
			'price' => $price,
			'categories' => $categories,
			'box_banner' => $box_banner,
			'postcard_banner' => $postcard_banner,
		]);
	}
}