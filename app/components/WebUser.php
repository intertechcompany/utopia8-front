<?php
class WebUser extends CWebUser
{
	public function init()
	{
		// Yii::trace('webuser init');
		parent::init();
	}

	public function restoreFromCookie()
	{
		// Yii::trace('restore');
		parent::restoreFromCookie();
	}

	public function renewCookie()
	{
		// Yii::trace('renew');
		parent::renewCookie();
	}

	protected function beforeLogin($id,$states,$fromCookie)
	{
		// Yii::trace('from cookie ' . var_export($fromCookie, true));
		return parent::beforeLogin($id,$states,$fromCookie);
	}

	protected function afterLogin($fromCookie)
	{
		$this->initClientData(true);

		$cart_model = Cart::model();
		$cart_model->recalculateCartPrices();
	}

	protected function afterLogout()
	{
		$cart_model = Cart::model();
		$cart_model->recalculateCartPrices();
	}

	public function validateToken()
	{
		$user_id = $this->id;
		$user_token = $this->hasState('token') ? $this->getState('token') : '';
		
		if (!User::model()->isValidToken($user_id, $user_token)) {
			$this->logout(false);
		}

		return $this;
	}

	public function initClientData($reinit = false)
	{
		if ($this->isGuest) {
			// skip if not logged
			return $this;
		}

		if (!$reinit && $this->hasState('user_data') && $this->hasState('saved')) {
			$current_user_data = $this->getState('user_data');

			// force reinit if user data was changed
			if ($current_user_data['saved'] != $this->getState('saved')) {
				$reinit = true;
			}
		} else {
			// no user data or force reinit
			$reinit = true;
		}

		if ($reinit) {
			$user_model = User::model();
			$user_data = array();

			// get user data
			$user = $user_model->getUser();

			$get_keys = array(
				'saved',
				'user_login',
				'user_email',
				'user_phone',
				'user_first_name',
				'user_last_name',
				'user_zip',
				'user_city',
				'user_address',
				'user_address_2',
				'discount',
			);

			foreach ($get_keys as $key) {
				if (isset($user[$key])) {
					$user_data[$key] = $user[$key];
				}
			}

			$this->setState('user_data', $user_data);
		}

		return $this;
	}
}