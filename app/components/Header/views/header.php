<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
	$staticUrl = $assetsUrl . '/static/' . Yii::app()->params->settings['rev'];
	$controller = Yii::app()->getController();

	$logo_class = '';

	if (!in_array($controller->route, ['site/product', 'site/cart', 'site/checkout'])) {
		$logo_class = ' header__logo--stage-1';
	}
?>
<header class="header<?php if ($controller->route == 'site/index') { ?> header--home<?php } elseif (in_array($controller->route, ['site/cart', 'site/checkout'])) { ?> header--checkout<?php } ?>">
	<div class="header__container wrap">
		<a href="<?=Yii::app()->createUrl('site/index')?>" class="header__logo<?=$logo_class?>"></a>
		<?php if (in_array($controller->route, ['site/cart', 'site/checkout'])) { ?>
		<div class="header__phone">
			<span><?=Lang::t('checkout.tip.header')?></span>
			<a href="tel:+<?=preg_replace('#[^\d]#', '', CHtml::encode(Yii::app()->params->settings['phone']))?>"><?=CHtml::encode(Yii::app()->params->settings['phone'])?></a>
		</div>
		<?php } else { ?>
		<a href="#" class="header__menu-toggle"><i></i></a>

		<?php if (!empty($categories)) {?>
		<ul class="header__menu list-unstyled">
			<?php foreach ($categories as $category) { ?>
			<li class="header__menu-item">
				<a href="<?=Yii::app()->createUrl('site/category', ['alias' => $category['category_alias']])?>" class="header__menu-link<?php if (!empty($category['sub'])) { ?> header__menu-link--collapsed<?php } ?>"><?=CHtml::encode($category['category_name'])?></a>
				<?php if (!empty($category['sub'])) { ?>
				<div class="menu">
					<ul class="menu__group menu__group--mobile list-unstyled">
						<li class="menu__category"><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $category['category_alias']])?>"><?=Lang::t('layout.link.allProducts')?></a></li>
					</ul>
					<?php foreach ($category['sub'] as $subcategory) { ?>
					<ul class="menu__group list-unstyled">
						<li class="menu__category<?php if (!empty($subcategory['sub'])) { ?> menu__category--collapsed<?php } ?>"><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $subcategory['category_alias']])?>"><?=CHtml::encode($subcategory['category_name'])?></a></li>
						<?php if (!empty($subcategory['sub'])) { ?>
						<li class="menu__subcategory menu__subcategory--mobile"><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $subcategory['category_alias']])?>"><?=Lang::t('layout.link.allProducts')?></a></li>
						<?php foreach ($subcategory['sub'] as $subcategory) { ?>
						<li class="menu__subcategory"><a href="<?=Yii::app()->createUrl('site/category', ['alias' => $subcategory['category_alias']])?>"><?=CHtml::encode($subcategory['category_name'])?></a></li>
						<?php } ?>
						<?php } ?>
					</ul>
					<?php } ?>
					<?php if (isset($banners[$category['category_id']])) { ?>
					<ul class="menu__group menu__group--special list-unstyled">
						<?php foreach ($banners[$category['category_id']] as $banner) { ?>
						<li class="menu__special">
							<?php
								if (strpos($banner['banner_url'], '/') === 0) {
									$banner['banner_url'] = preg_replace('#^/#ui', Yii::app()->request->getBaseUrl() . '/', $banner['banner_url']);
								}

								if (!empty($banner['banner_logo'])) {
									$banner_image = json_decode($banner['banner_logo'], true);
									$banner_image = $assetsUrl . '/banner/' . $banner_image['file'];
								} else {
									$banner_image = '';
								}
							?>
							<a href="<?=$banner['banner_url']?>"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
								<?php /* <picture>
									<source type="image/webp" srcset="<?=$staticUrl?>/images/category_1.webp, <?=$staticUrl?>/images/category_1@2x.webp 2x">
									<source srcset="<?=$staticUrl?>/images/category_1.jpg, <?=$staticUrl?>/images/category_1@2x.jpg 2x">
									<img src="<?=$staticUrl?>/images/category_1.jpg" alt="">
								</picture> */ ?>
								<?php if (!empty($banner_image)) { ?>
								<picture>
									<img src="<?=$banner_image?>" alt="" width="680" height="420">
								</picture>
								<?php } ?>
							</a>
							<a href="<?=$banner['banner_url']?>"<?php if ($banner['banner_url_blank']) { ?> target="_blank"<?php } ?>>
								<?=CHtml::encode($banner['banner_name'])?>
							</a>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
				<?php } ?>
			</li>
			<?php } ?>
		
			<!-- <li class="header__menu-item">
				<a href="<?=$controller->createUrl('site/sale')?>" class="header__menu-link">Sale</a>
			</li> -->
			<?php if (!empty($pages)) {?>
			<li class="header__menu-item header__menu-item--submenu">
				<a href="#" class="header__menu-link header__menu-link--collapsed"><?=Lang::t('layout.btn.info')?></a>
				<ul class="submenu list-unstyled">
					<?php foreach ($pages as $page) { ?>
					<li class="submenu__page"><a href="<?=Yii::app()->createUrl('site/page', ['alias' => $page['page_alias']])?>"><?=CHtml::encode($page['page_title'])?></a></li>
					<?php } ?>
				</ul>
			</li>
			<?php } ?>
			<li class="header__menu-item header__menu-item--submenu">
				<a href="#" class="header__menu-link header__menu-link--collapsed"><?=Yii::app()->params->langs[Yii::app()->language]['short_name']?></a>
				<ul class="submenu list-unstyled">
					<?php
						// $current_url_path = Yii::app()->getRequest()->getBaseUrl() . '/' . Yii::app()->getRequest()->getPathInfo();
						$current_url_path = Yii::app()->getRequest()->getPathInfo();
						$query_string = Yii::app()->getRequest()->getQueryString();

						if (!empty($query_string)) {
							$current_url_path .= '?' . $query_string;
						}
						
					?>
					<?php foreach (Yii::app()->params->langs as $lang_code => $lang) { ?>
					<?php
						if ($lang_code == Yii::app()->language) {
							continue;
						}

						if (empty($lang['url'])) {
							$lang_url = '/' . $current_url_path;
						} else {
							$lang_url = '/' . $lang['url'] . '/' . $current_url_path;
						}
					?>
					<li class="submenu__page"><a href="<?=$lang_url?>"><?=CHtml::encode($lang['name'])?></a></li>
					<?php } ?>
				</ul>
			</li>
		</ul>
		<?php } ?>

		<a href="#" class="header__search-toggle">
			<i class="icon icon-search"></i>
			<i class="icon icon-close"></i>
		</a>
		<form action="<?=$controller->createUrl('site/search')?>" class="header__search" method="get">
			<input type="text" name="keyword" placeholder="<?=Lang::t('layout.placeholder.search')?>">
			<a href="#" class="header__search-clear header__search-clear--hidden"><i class="icon-inline icon-close"></i></a>
			<button><i class="icon-inline icon-search"></i></button>
		</form>
		<a href="<?=$controller->createUrl('site/cart')?>" class="header__cart header__cart--sm<?php if ($cart_total) { ?> header__cart--products<?php } ?>">
			<i class="icon-inline icon-cart"></i>
			(<span><?=$cart_total?></span>)
		</a>
		<a href="<?=$controller->createUrl('site/cart')?>" class="header__cart header__cart--lg<?php if ($cart_total) { ?> header__cart--products<?php } ?>">
			<?=Lang::t('layout.btn.cart')?> <span><?=$cart_total?></span>
		</a>
		<?php } ?>
	</div>
</header>
<!-- /.header -->
