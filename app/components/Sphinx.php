<?php
/**
 * Sphinx class
 */
class Sphinx
{
	private $max_matches = 1000;
	
	public function __construct()
	{
		
	}

	public static function escapeMatchValue($str)
    {
        return str_replace(
            ['\\', '/', '"', '(', ')', '|', '-', '!', '@', '~', '&', '^', '$', '=', '>', '<', "\x00", "\n", "\r", "\x1a"],
            ['\\\\', '\\/', '\\"', '\\(', '\\)', '\\|', '\\-', '\\!', '\\@', '\\~', '\\&', '\\^', '\\$', '\\=', '\\>', '\\<',  "\\x00", "\\n", "\\r", "\\x1a"],
            $str
        );
	}

	public static function getFieldWeights()
	{	
		$field_weights = [
			'title_uk=9',
			'title_ru=9',
			'title_en=9',
			'articul=8',
			'brand_uk=6',
			'brand_ru=6',
			'brand_en=6',
			'tag_uk=5',
			'tag_ru=5',
			'tag_en=5',
			'description_uk=2',
			'description_ru=2',
			'description_en=2',
			'category_uk=2',
			'category_ru=2',
			'category_en=2',
			'badge_uk=1',
			'badge_ru=1',
			'badge_en=1',
		];
		
		return '(' . implode(', ', $field_weights) . ')';
	}

	public function testFacet()
	{
		$conn = new PDO('mysql:host=127.0.0.1;port=9306');
		$stmt = $conn->prepare("SELECT WEIGHT(), * 
							  FROM product 
							  WHERE MATCH(:match) AND active = 1
							  ORDER BY WEIGHT() DESC
							  LIMIT 0,5
							  OPTION field_weights = " . Sphinx::getFieldWeights() . "
							  FACET categories
							  FACET tags
							  FACET badges
							  FACET values;
							  SHOW META");
		$stmt->bindValue(':match', Sphinx::escapeMatchValue('TEST'), PDO::PARAM_STR);
		$stmt->execute();
		
		do {
			try {
				echo '<pre>', print_r($stmt->fetchAll(PDO::FETCH_ASSOC), true), '</pre>';
			} catch (PDOException $e) {
				echo $e->getMessage();
			}
		} while ($stmt->nextRowset());

		/* $data = Yii::app()->sphinx
			// ->createCommand("SELECT * FROM product WHERE MATCH('сумка') AND active = 1 ORDER BY WEIGHT() DESC") // LIMIT 995,10
			->createCommand("SELECT id FROM product WHERE active = 1 LIMIT 995,10 OPTION max_matches=3000") // LIMIT 995,10
			->queryAll();
		$data = Yii::app()->sphinx
			->createCommand("SHOW META")
			->queryAll();
		echo '<pre>', print_r($data, true), '</pre>'; */

		/* 
		$data = Yii::app()->sphinx
			->createCommand("SELECT WEIGHT(), * 
							 FROM product 
							 WHERE MATCH(:match) AND active = 1
							 ORDER BY WEIGHT() DESC
							 LIMIT 0,5
							 OPTION field_weights = " . Sphinx::getFieldWeights() . "
							 FACET categories
							 FACET tags
							 FACET badges
							 FACET values")
			->bindValue(':match', Sphinx::escapeMatchValue('фут'), PDO::PARAM_STR)
			->queryAll();

		echo '<pre>ROWS: — ', print_r($data, true), '</pre>';

		$data = Yii::app()->sphinx
			->createCommand("SHOW META")
			->queryAll();
		echo '<pre>', print_r($data, true), '</pre>'; */
	}

	public function isActive()
	{
		try {
			Yii::app()->sphinx->active = true;
		} catch (CDbException $e) {
			// no connection
			return false;
		}

		return true;
	}

	public function searchTotal($per_page, $keyword)
	{
		$tmp_data = Yii::app()->sphinx
			->createCommand("SELECT id 
							 FROM product 
							 WHERE MATCH(:match) AND active = 1
							 LIMIT 1
							 OPTION field_weights = " . self::getFieldWeights())
			->bindValue(':match', self::escapeMatchValue($keyword), PDO::PARAM_STR)
			->queryColumn();

		$data = [];
		$raw_data = Yii::app()->sphinx
			->createCommand("SHOW META")
			->queryAll();
			
		foreach ($raw_data as $index => $value) {
			if ($index > 2) {
				break;
			}

			$data[$value['Variable_name']] = $value['Value'];
		}

		if ($data['total_found'] > $data['total']) {
			$this->max_matches = $data['total_found']; 
		}

		return array(
			'total' => (int) $data['total_found'],
			'pages' => ceil($data['total_found'] / $per_page),
		);
	}

	public function search($offset, $limit, $keyword, $sort)
	{
		switch ($sort) {
			case 'price':
				$order_by = 'in_stock DESC, price ASC';
				break;
			case 'newest':
				$order_by = 'in_stock DESC, published DESC, id DESC';
				break;
			default:
				// $order_by = 'in_stock DESC, rating DESC, published DESC, id DESC';
				$order_by = 'in_stock DESC, WEIGHT() DESC';
		}
		
		$data = Yii::app()->sphinx
			// ->createCommand("SELECT *, WEIGHT() 
			->createCommand("SELECT id 
							 FROM product 
							 WHERE MATCH(:match) AND active = 1
							 ORDER BY {$order_by}
							 LIMIT {$offset},{$limit}
							 OPTION field_weights = " . self::getFieldWeights() . ", max_matches=" . $this->max_matches)
			->bindValue(':match', self::escapeMatchValue($keyword), PDO::PARAM_STR)
			->queryColumn();

		return $data;
	}
}