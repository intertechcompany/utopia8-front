<?php
/**
 * BeginRequest class
 */
class BeginRequest
{
	public static function onStartSite()
	{
		// before start request
		$_app_request = Yii::app()->getRequest();
		
		// base url
		$_base_url = $_app_request->getBaseUrl();

		// remove trailing slashes
		$_full_uri = $_app_request->getRequestUri();
		list($_path, ) = explode('?', $_full_uri);
		$_clean_path = rtrim($_path, '/');

		if ($_clean_path == $_base_url) {
			$_clean_path = $_base_url . '/';
		}
		
		if ($_path != $_clean_path) {
			if ($_app_request->getQueryString() != '') {
				$_app_request->redirect($_app_request->getHostInfo() . $_clean_path . '?' . $_app_request->getQueryString(), true, 301);
			} else {
				$_app_request->redirect($_app_request->getHostInfo() . $_clean_path, true, 301);
			}
		}

		if (preg_match('#/(ru|en)/(ru|en)/(.*)?#ui', $_path, $_path_parts)) {
			$_app_request->redirect('/' . $_path_parts[1] . '/' . $_path_parts[3], true, 301);
		}

		// get app config
		$app_settings = Yii::app()->db
			->createCommand("SELECT * FROM setting WHERE setting_id = 1")
			->queryRow();
		
		if (!empty($app_settings)) {
			$disabled_setting_fields = array(
				'setting_id',
				'created',
				'saved',
			);
			
			$settings_data = array();
			
			foreach ($app_settings as $setting_key => $setting_value) {
				if (in_array($setting_key, $disabled_setting_fields)) {
					continue;
				} else {
					$setting_key = str_replace('setting_', '', $setting_key);
				}

				if ($setting_key == 'currency' && !empty($setting_value)) {
					$setting_value = json_decode($setting_value, true);
				}
				
				$settings_data[$setting_key] = $setting_value;
			}
			
			Yii::app()->setParams(array('settings' => $settings_data));
		} else {
			throw new CException('Can\'t load site settings');
		}

		// get categories
		$category_model = Category::model();
		Yii::app()->params->categories = $category_model->getCategoriesList();
		Yii::app()->params->categories_tree = $category_model->getCategoriesTree();

		// get translations from cache
		if (Yii::app()->cache !== null) {
			$translations = Yii::app()->cache->get('_t');

			if ($translations !== false) {
				Yii::app()->params->translations = $translations;
			} else {
				Translation::model()->warmUpCache();
				
				Yii::app()->params->translations = Yii::app()->cache->get('_t');
			}
		}

		// validate user
		$app_user = Yii::app()->getUser();

		if (!$app_user->isGuest) {
			$app_user->validateToken()->initClientData();
		}

		// init cart
		Cart::model()->initCart();

		// init discount
		Discount::model()->init();

		// highlight translations
		if (Yii::app()->request->getQuery('highlight') == '1') {
            Yii::app()->params->highlight = true;
		}
    }
  
    public static function onStopSite()
    {  
		// after request complete
		if (!Yii::app()->request->isAjaxRequest) {
			echo sprintf('%0.5f',Yii::getLogger()->getExecutionTime());
		}
    }
}